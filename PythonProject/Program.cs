﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PythonProject
{
    static class Program
    {
        public static FrmMain MainFormm;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += Application_ThreadException;

          //  Application.Run(new Form2());
            Application.Run(new FrmMain());
            //Application.Run(new FrmLogin());
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            MessageBox.Show("Unexpected error has occurred", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            LogUtility.Log(e.Exception);
            
            
        }
    }
}
