﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythonProject
{
    public class Input2
    {
        public string PowerPlantCode { get; set; }
        public string UnitCode { get; set; }
        public string OurUnitCode { get; set; }
        public string FirstDate { get; set; }
        public string LastDate { get; set; }
        public int Step { get; set; }
        public bool IsUnit { get; set; }

        public int LogFunction { get; set; }
        public int MovingAverage { get; set; }
        public int StationaryTest { get; set; }
        public int Seasonality { get; set; }
        public int Autocorrelation { get; set; }
        public int Differencing { get; set; }
        public int SpectrumAnalysis { get; set; }

   
    }
}
