﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace PythonProject
{
    public class XmlUtility
    {
        public static string GetSettingString()
        {
            try
            {
                string fileName = "Config.xml";
                
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string s = xmlDocument.SelectNodes("root/SettingString")[0].InnerText;
                return s;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
                return null;
            }
        }

        public static void SaveSettingString(string settingString)
        {
            string fileName = "Config.xml";
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                xmlDocument.SelectNodes("root/SettingString")[0].InnerText = settingString;
                
                xmlDocument.Save(fileName);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error " + ex.Message);
            }
        }
        public static void SaveServerAddress(string pythonServer, string appServer)
        {
            string fileName = "Config.xml";

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(fileName);
            xmlDocument.SelectNodes("root/DB/Server")[0].InnerText = pythonServer;
            xmlDocument.SelectNodes("root/DB2/Server")[0].InnerText = appServer;
            xmlDocument.Save(fileName);

        }
        public static void SaveServerLogin(string pythonLogin, string appLogin)
        {
            string fileName = "Config.xml";

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(fileName);
            xmlDocument.SelectNodes("root/DB/Login")[0].InnerText = pythonLogin;
            xmlDocument.SelectNodes("root/DB2/Login")[0].InnerText = appLogin;
            xmlDocument.Save(fileName);

        }

        public static void SaveServerPassword(string pythonPassword, string appPassword)
        {
            string fileName = "Config.xml";

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(fileName);
            xmlDocument.SelectNodes("root/DB/Password")[0].InnerText = pythonPassword;
            xmlDocument.SelectNodes("root/DB2/Password")[0].InnerText = appPassword;
            xmlDocument.Save(fileName);

        }
        public static void SavePythonPath(string pythonPath)
        {
            string fileName = "Config.xml";

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(fileName);
            xmlDocument.SelectNodes("root/PythonPath")[0].InnerText = pythonPath;
            xmlDocument.Save(fileName);

        }

        public static Input ReadInput()
        {
            
                string fileName = "Input.xml";
                Input input = new Input();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                input.PowerPlantCode = xmlDocument.SelectNodes("root/PowerPlant")[0].InnerText;
                input.UnitCode = xmlDocument.SelectNodes("root/UnitCode")[0].InnerText;
                input.FirstDate = xmlDocument.SelectNodes("root/FirstDate")[0].InnerText;
                input.LastDate = xmlDocument.SelectNodes("root/LastDate")[0].InnerText;
                input.HasFuel = int.Parse(xmlDocument.SelectNodes("root/HasFuel")[0].InnerText);
                input.Revision = int.Parse(xmlDocument.SelectNodes("root/Revision")[0].InnerText);
                input.IsLastRevision = int.Parse(xmlDocument.SelectNodes("root/IsLastRevision")[0].InnerText);
                input.Attribute1 = xmlDocument.SelectNodes("root/Attribute1")[0].InnerText;
                input.Attribute2 = xmlDocument.SelectNodes("root/Attribute2")[0].InnerText;
                string isUnit=xmlDocument.SelectNodes("root/isUnit")[0].InnerText.Trim();
                input.IsUnit = isUnit == "1" ? true : false;                

                return input;                       


            // <PowerPlant>131</PowerPlant>
            //<UnitCode>S1</UnitCode>
            //<FirstDate>1395/01/01</FirstDate>
            //<LastDate>1395/07/30</LastDate>
            //<Table> [EMIS0895].[Run].[HourlyUnitCommitment__BV]</Table>
            //<Hour> HourID</Hour>
            //<HasFuel>1</HasFuel>
            //<Revision>1</Revision>
            //<IsLastRevision>0</IsLastRevision>
            //<Attribute1>EconomicValue</Attribute1>
            //<Table2>[EMIS0895].[Run].[HourlyUnitCommitment__BV]</Table2>
            //<Attribute2>RequiredValue</Attribute2>    
        }

        public static void WriteInput(Input input)
        {
            string fileName = @"Input.xml";
            
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                xmlDocument.SelectNodes("root/PowerPlant")[0].InnerText = input.PowerPlantCode;
                xmlDocument.SelectNodes("root/UnitCode")[0].InnerText = input.UnitCode;
                xmlDocument.SelectNodes("root/FirstDate")[0].InnerText = input.FirstDate;
                xmlDocument.SelectNodes("root/LastDate")[0].InnerText = input.LastDate;
                //xmlDocument.SelectNodes("root/HasFuel")[0].InnerText = input.HasFuel.ToString();
                //xmlDocument.SelectNodes("root/Revision")[0].InnerText = input.Revision.ToString();
                //xmlDocument.SelectNodes("root/IsLastRevision")[0].InnerText = input.IsLastRevision.ToString();
                xmlDocument.SelectNodes("root/Attribute1")[0].InnerText = input.Attribute1;
                xmlDocument.SelectNodes("root/Attribute2")[0].InnerText = input.Attribute2;

                xmlDocument.SelectNodes("root/isUnit")[0].InnerText = input.IsUnit ? "1" : "0";

                xmlDocument.Save(fileName);             
        }

        public static Input2 ReadInput2()
        {
            
                string fileName = "Input2.xml";
                Input2 input = new Input2();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                input.PowerPlantCode = xmlDocument.SelectNodes("root/PowerPlant")[0].InnerText;
                input.UnitCode = xmlDocument.SelectNodes("root/UnitCode")[0].InnerText;
                input.FirstDate = xmlDocument.SelectNodes("root/FirstDate")[0].InnerText;
                input.LastDate = xmlDocument.SelectNodes("root/LastDate")[0].InnerText;
                //input.HasFuel = int.Parse(xmlDocument.SelectNodes("root/HasFuel")[0].InnerText);
                //input.Revision = int.Parse(xmlDocument.SelectNodes("root/Revision")[0].InnerText);
                //input.IsLastRevision = int.Parse(xmlDocument.SelectNodes("root/IsLastRevision")[0].InnerText);
                //input.Attribute1 = xmlDocument.SelectNodes("root/Attribute1")[0].InnerText;
                //input.Attribute2 = xmlDocument.SelectNodes("root/Attribute2")[0].InnerText;
                input.Step = int.Parse(xmlDocument.SelectNodes("root/Step")[0].InnerText);
                input.OurUnitCode = xmlDocument.SelectNodes("root/OurUnit")[0].InnerText;

                input.LogFunction = int.Parse(xmlDocument.SelectNodes("root/log")[0].InnerText);
                input.MovingAverage = int.Parse(xmlDocument.SelectNodes("root/moving")[0].InnerText);
                input.StationaryTest = int.Parse(xmlDocument.SelectNodes("root/stationary")[0].InnerText);
                input.Differencing = int.Parse(xmlDocument.SelectNodes("root/difference")[0].InnerText);
                input.Seasonality = int.Parse(xmlDocument.SelectNodes("root/season")[0].InnerText);
                input.Autocorrelation = int.Parse(xmlDocument.SelectNodes("root/Autocor")[0].InnerText);
                input.SpectrumAnalysis = int.Parse(xmlDocument.SelectNodes("root/powSpec")[0].InnerText);

                string isUnit = xmlDocument.SelectNodes("root/isUnit")[0].InnerText.Trim();
                input.IsUnit = isUnit == "1" ? true : false; 
                return input;
            
            
        }

        public static void WriteInput2(Input2 input2)
        {
            string fileName = @"Input2.xml";
           
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                xmlDocument.SelectNodes("root/PowerPlant")[0].InnerText = input2.PowerPlantCode;
                xmlDocument.SelectNodes("root/UnitCode")[0].InnerText = input2.UnitCode;
                xmlDocument.SelectNodes("root/FirstDate")[0].InnerText = input2.FirstDate;
                xmlDocument.SelectNodes("root/LastDate")[0].InnerText = input2.LastDate;
                //xmlDocument.SelectNodes("root/HasFuel")[0].InnerText = input2.HasFuel.ToString();
                //xmlDocument.SelectNodes("root/Revision")[0].InnerText = input2.Revision.ToString();
                //xmlDocument.SelectNodes("root/IsLastRevision")[0].InnerText = input2.IsLastRevision.ToString();
                //xmlDocument.SelectNodes("root/Attribute1")[0].InnerText = input2.Attribute1;
                //xmlDocument.SelectNodes("root/Attribute2")[0].InnerText = input2.Attribute2;
                xmlDocument.SelectNodes("root/Step")[0].InnerText = input2.Step.ToString();
                xmlDocument.SelectNodes("root/OurUnit")[0].InnerText = input2.OurUnitCode;

                
                xmlDocument.SelectNodes("root/log")[0].InnerText = input2.LogFunction.ToString();
                xmlDocument.SelectNodes("root/moving")[0].InnerText = input2.MovingAverage.ToString();
                xmlDocument.SelectNodes("root/stationary")[0].InnerText = input2.StationaryTest.ToString();
                xmlDocument.SelectNodes("root/difference")[0].InnerText = input2.Differencing.ToString();
                xmlDocument.SelectNodes("root/season")[0].InnerText = input2.Seasonality.ToString();
                xmlDocument.SelectNodes("root/Autocor")[0].InnerText = input2.Autocorrelation.ToString();
                xmlDocument.SelectNodes("root/powSpec")[0].InnerText = input2.SpectrumAnalysis.ToString();

                xmlDocument.SelectNodes("root/isUnit")[0].InnerText = input2.IsUnit ? "1" : "0";

                xmlDocument.Save(fileName);
            


        }

        public static DbConnection GetDbConnection()
        {
            try
            {
                string fileName = "Config.xml";

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                DbConnection dbConnection = new DbConnection(
                    Protection.Decrypt(xmlDocument.SelectNodes("root/DB2/Server")[0].InnerText),
                    Protection.Decrypt(xmlDocument.SelectNodes("root/DB2/Login")[0].InnerText),
                    Protection.Decrypt(xmlDocument.SelectNodes("root/DB2/Password")[0].InnerText));

                return dbConnection;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
                return null;
            }
        }

        public static string GetPythonPath()
        {
            try
            {
                string fileName = "Config.xml";
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string path = xmlDocument.SelectNodes("root/PythonPath")[0].InnerText;
                return path;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
                return null;
            }
        }

        public static List<GeneralMethod> GetPreProcessingMethods()
        {
            List<GeneralMethod> methods = new List<GeneralMethod>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load("PreprocessingMethods.xml");
            XmlNodeList nodes = xmlDocument.SelectNodes("Methods/Method");
            foreach (XmlElement node in nodes)
            {
                methods.Add(new GeneralMethod(node.Attributes["Name"].InnerText,
                    node.Attributes["ScriptFile"].InnerText));
            }
            return methods;
        }

        public static List<string> GetPreProcessingAttributes()
        {
            List<string> attribs = new List<string>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load("PreprocessingAttributes.xml");
            XmlNodeList nodes = xmlDocument.SelectNodes("Attributes/Attribute");
            foreach (XmlElement node in nodes)
            {
                attribs.Add(node.InnerText);
            }
            return attribs;
        }


        public static List<GeneralMethod> GetModelingMethods()
        {
            List<GeneralMethod> methods = new List<GeneralMethod>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load("ModelingMethods.xml");
            XmlNodeList nodes = xmlDocument.SelectNodes("Methods/Method");
            foreach (XmlElement node in nodes)
            {
                methods.Add(new GeneralMethod(node.Attributes["Name"].InnerText,
                    node.Attributes["ScriptFile"].InnerText,
                    int.Parse(node.Attributes["Type"].InnerText),
                    node.Attributes["Description"].InnerText
                    ));
            }
            return methods;
        }
    }

   

  
}
