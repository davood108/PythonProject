﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythonProject
{
    public class Input
    {
        public string PowerPlantCode { get; set; }
        public string UnitCode { get; set; }
        public string FirstDate { get; set; }
        public string LastDate { get; set; }
        public int HasFuel { get; set; }
        public int IsLastRevision { get; set; }
        public int Revision { get; set; }
        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public bool IsUnit { get; set; }
    }
}
