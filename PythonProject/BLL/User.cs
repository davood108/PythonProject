﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PythonProject
{
    internal class User
    {
        public static string CurrentUserName { get; set; }

        public static bool Login(string userName, string password, out string error)
        {
            ir.igmc.rwst.RIntegratedWebService ws = new ir.igmc.rwst.RIntegratedWebService();
            var appcode = "SETL@22596541";

            var loginInfo = String.Format("{0}|{1}", userName.Trim(), password);
            var errorMsg = "";
            var roles = "";
            bool success = ws.Login(appcode, loginInfo, ref errorMsg, ref roles);
            error = errorMsg;
            return success;

        }
    }
}
