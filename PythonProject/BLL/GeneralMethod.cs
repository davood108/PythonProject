﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythonProject
{
    public class GeneralMethod
    {
        public string Name { get; set; }
        public string ScriptFile { get; set; }

        public int Type { get; set; }
        public string Description { get; set; }
        public GeneralMethod(string name, string scriptFileName)
        {
            Name = name;
            ScriptFile = scriptFileName;
        }
        /// <summary>
        /// For Modeling Methods
        /// </summary>
        public GeneralMethod(string name, string scriptFileName, int type, string description)
        {
            Name = name;
            ScriptFile = scriptFileName;
            Type = type;
            Description = description;
        }
        //For use this class as ComboBoxItem
        public override bool Equals(object obj)
        {
            GeneralMethod item = obj as GeneralMethod;
            if (item == null)
            {
                return false;
            }
            return item.Name == this.Name;
        }
        //For use this class as ComboBoxItem
        public override int GetHashCode()
        {
            if (this.Name == null)
            {
                return 0;
            }
            return this.Name.GetHashCode();
        }
        ////For use this class as ComboBoxItem
        public override string ToString()
        {
            return Name;
        }

    }
}
