﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace PythonProject
{
    internal class LogUtility
    {

        public static void Log(string txtInfo)
        {
            //StackTrace stackTrace = new StackTrace();
            // Get calling method name
            //string s=  stackTrace.GetFrame(1).GetMethod().Name;

            string logFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Log.txt";
            StreamWriter sw = new StreamWriter(logFile, true);
            sw.WriteLine(DateTime.Now + "    " + txtInfo);
            sw.Close();



        }

        public static void Log(Exception exception)
        {
            string logFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Log.txt";
            StreamWriter sw = new StreamWriter(logFile, true);

            string s = string.Format("{0}    Exception:{1}   .Inner Exception:{2}   .Stack:{3}", DateTime.Now, exception.Message, exception.InnerException != null ? exception.InnerException.Message : "", exception.StackTrace);
            sw.WriteLine(s);
            sw.Close();
        }
    }
}
