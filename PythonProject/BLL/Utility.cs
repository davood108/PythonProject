﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PythonProject
{
  public  class Utility
    {
    public  static string GetOutputPath(string FromDate, string ToDate) 
      {
          string appPath = AppDomain.CurrentDomain.BaseDirectory;
          //string path = string.Format("{0}\\Output\\{1}_{2}", appPath, FromDate.Replace('/', '.'), ToDate.Replace('/', '.'));
          string path = string.Format("{0}\\Output\\{1}_{2}", appPath, FromDate.Replace('/', '_'), ToDate.Replace('/', '_'));
          if (!Directory.Exists(path))
          {
              Directory.CreateDirectory(path);
          }
          return path;
      }
    }
}
