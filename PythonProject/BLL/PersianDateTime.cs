﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace PythonProject
{
    public class PersianDateTime:IComparable<PersianDateTime>
    {
        
        /// <summary>
        /// yyyy/MM/dd
        /// </summary>
        public string PersianDateString
        {
            get { return String.Format("{0:D4}/{1:D2}/{2:D2}", Year, Month, Day); }
        }

        /// <summary>
        /// yyyy/MM/dd hh:mm:ss
        /// </summary>
        public string PersianDateTimeString
        {
            get
            {
                return String.Format("{0:D4}/{1:D2}/{2:D2} {3:D2}:{4:D2}:{5:D2}", Year, Month, Day, Hour, Minute, Second);
            }
        }

        public int MonthDays //baraye esfand mah va sale kabise dorost mibashad
        {
            get
            {
                PersianCalendar p = new PersianCalendar();
                return p.GetDaysInMonth(Year, Month);
            }
        }

        public int Year { get; private set; }

        public int Month { get; private set; }

        public int Day { get; private set; }

        public int Hour { get; private set; }

        public int Minute { get; private set; }

        public int Second { get; private set; }

        public DateTime MiladiDateTime {
            get
            {
                PersianCalendar p = new PersianCalendar();
               return p.ToDateTime(Year, Month, Day, Hour, Minute, Second, 0);
            }
        }

        public PersianDateTime PreviousMonth
        {
            get
            {
                int y = Year;
                int m = Month;
                int d = Day;
                if (m == 1)
                {
                    m = 12;
                    y--;
                }
                else
                    m--;

                PersianDateTime temp = new PersianDateTime(y, m, 1);
                if (temp.MonthDays < d)
                {
                    d = temp.MonthDays;
                }
                return new PersianDateTime(y, m, d);
            }
        }

        public PersianDateTime NextMonth
        {
            get
            {
                int y = Year;
                int m = Month;
                int d = Day;
                if (m == 12)
                {
                    m = 1;
                    y++;
                }
                else
                    m++;

                PersianDateTime temp = new PersianDateTime(y, m, 1);
                if (temp.MonthDays < d)
                {
                    d = temp.MonthDays;
                }
                return new PersianDateTime(y, m, d);
            }
        }

        public PersianDateTime AddDays(int count)
        {
            DateTime dateTime = MiladiDateTime.AddDays(count);
            return new PersianDateTime(dateTime);

        }
        public PersianDateTime AddHours(int count)
        {
            DateTime dateTime = MiladiDateTime.AddHours(count);
            return new PersianDateTime(dateTime);

        }
        public PersianDateTime AddMinutes(int count)
        {
            DateTime dateTime = MiladiDateTime.AddMinutes(count);
            return new PersianDateTime(dateTime);

        }
        public PersianDateTime AddSeconds(int count)
        {
            DateTime dateTime = MiladiDateTime.AddSeconds(count);
            return new PersianDateTime(dateTime);
        }

        private readonly Regex _dateRegex = new Regex(@"^\d{4}\/((0[1-9])|(1[0-2]))\/(([0-2]\d)|3[01])$");
        private readonly Regex _timeRegex = new Regex(@"^([0-1]\d{1}|2[0-3]):[0-5]\d{1}$");
        //^\d{4}\/((0\d)|(1[012]))\/(([012]\d)|3[01])$

        public static PersianDateTime Now
        {
            get
            {
                DateTime dt = DateTime.Now;
                PersianCalendar p = new PersianCalendar();
                return new PersianDateTime(p.GetYear(dt), p.GetMonth(dt), p.GetDayOfMonth(dt))
                {
                    Hour = dt.Hour,
                    Minute = dt.Minute,
                    Second = dt.Second
                };
            }
        }

        /// <summary>
        /// Time=00:00:00
        /// </summary>
        public static PersianDateTime Now2
        {
            get
            {
                DateTime dt = DateTime.Now;
                PersianCalendar p = new PersianCalendar();
                return new PersianDateTime(p.GetYear(dt), p.GetMonth(dt), p.GetDayOfMonth(dt))
                {
                    Hour = 0,
                    Minute = 0,
                    Second = 0
                };
            }
        }

        /// <summary>
        /// Time=hh:MM:00 (Second is zero)
        /// </summary>
        public static PersianDateTime Now3
        {
            get
            {
                DateTime dt = DateTime.Now;
                PersianCalendar p = new PersianCalendar();
                return new PersianDateTime(p.GetYear(dt), p.GetMonth(dt), p.GetDayOfMonth(dt))
                {
                    Hour = dt.Hour,
                    Minute = dt.Minute,
                    Second = 0
                };
            }
        }


        /// <summary>
        /// Format: yyyy/mm/dd
        /// </summary>
        public PersianDateTime(string date)
        {
            Match match = _dateRegex.Match(date);
            if (match.Success)
            {
                string[] dateparts = date.Split('/');
                Year = int.Parse(dateparts[0]);
                Month = int.Parse(dateparts[1]);
                Day = int.Parse(dateparts[2]);
                PersianCalendar p = new PersianCalendar();
                p.ToDateTime(Year, Month, Day, 0, 0, 0, 0); //parse date
            }
            else
            {
                throw new Exception("Invalid Date Format. Valid Format:yyyy/MM/dd");
            }

        }

        /// <summary>
        /// Format: date: yyyy/MM/dd , time: hh:mm
        /// </summary>
        public PersianDateTime(string date,string time)
        {
            Match match = _dateRegex.Match(date);
            Match match2 = _timeRegex.Match(time);
            if (match.Success)
            {
                string[] dateparts = date.Split('/');
                Year = int.Parse(dateparts[0]);
                Month = int.Parse(dateparts[1]);
                Day = int.Parse(dateparts[2]);
                PersianCalendar p = new PersianCalendar();
                p.ToDateTime(Year, Month, Day, 0, 0, 0, 0); //parse date
            }
            else
            {
                throw new Exception("Invalid Date Format. Valid Format:yyyy/MM/dd");
            }

            if (match2.Success)
            {
                string[] timeParts = time.Split(':');
                Hour = int.Parse(timeParts[0]);
                Minute = int.Parse(timeParts[1]);
            }
            else
            {
                throw new Exception("Invalid Time Format. Valid Format:hh:mm");
            }

        }

        public PersianDateTime(int year, int month, int day)
        {
            //if (year > 9999 || year < 1 || month < 1 || month > 12 || day < 1 || day > 31)
            //{
            //    throw new Exception("Invalid Values");
            //}
          
            PersianCalendar p = new PersianCalendar();
            p.ToDateTime(year, month, day, 0, 0, 0, 0); //parse date
            Year = year;
            Month = month;
            Day = day;
        }

        public PersianDateTime(int year, int month, int day, int hour, int minute)
        {
            //if (year > 9999 || year < 1 || month < 1 || month > 12 || day < 1 || day > 31)
            //{
            //    throw new Exception("Invalid Values");
            //}

            PersianCalendar p = new PersianCalendar();
            p.ToDateTime(year, month, day, hour, minute, 0, 0); //parse date
            Year = year;
            Month = month;
            Day = day;
            Hour = hour;
            Minute = minute;
        }

        public PersianDateTime(DateTime miladiDateTime)
        {
            PersianCalendar p = new PersianCalendar();
            Year = p.GetYear(miladiDateTime);
            Month = p.GetMonth(miladiDateTime);
            Day = p.GetDayOfMonth(miladiDateTime);

            Hour = p.GetHour(miladiDateTime);
            Minute = p.GetMinute(miladiDateTime);
            Second = p.GetSecond(miladiDateTime);
        }



        #region Operator Overloading
        public static bool operator >=(PersianDateTime dateL, PersianDateTime dateR)
        {
            return (dateL > dateR) || (dateL == dateR);
        }

        public static bool operator <=(PersianDateTime dateL, PersianDateTime dateR)
        {
            return (dateL < dateR) || (dateL == dateR);
        }

        public static bool operator <(PersianDateTime dateL, PersianDateTime dateR)
        {
            return dateL.MiladiDateTime < dateR.MiladiDateTime;
            //if (dateL.Year < dateR.Year)
            //    return true;
            //if (dateL.Year == dateR.Year)
            //{
            //    if (dateL.Month < dateR.Month)
            //        return true;
            //    if (dateL.Month == dateR.Month)
            //    {
            //        if (dateL.Day < dateR.Day)
            //            return true;
            //        return false;
            //    }
            //}
            //return false;
        }

        public static bool operator >(PersianDateTime dateL, PersianDateTime dateR)
        {
            return dateL.MiladiDateTime > dateR.MiladiDateTime;
            //if (dateL.Year > dateR.Year)
            //    return true;
            //if (dateL.Year == dateR.Year)
            //{
            //    if (dateL.Month > dateR.Month)
            //        return true;
            //    if (dateL.Month == dateR.Month)
            //    {
            //        if (dateL.Day > dateR.Day)
            //            return true;
            //        return false;
            //    }
            //}
            //return false;
        }

        public static bool operator ==(PersianDateTime dateL, PersianDateTime dateR)
        {
            return dateL.MiladiDateTime == dateR.MiladiDateTime;
            //return (dateL.Year == dateR.Year && dateL.Month == dateR.Month && dateL.Day == dateR.Day);
        }

        public static bool operator !=(PersianDateTime dateL, PersianDateTime dateR)
        {
            return !(dateL == dateR);
        }
        #endregion Operator Overloading

        protected bool Equals(PersianDateTime other)
        {
            return Year == other.Year && Month == other.Month && Day == other.Day;
        }


        int IComparable<PersianDateTime>.CompareTo(PersianDateTime other)
        {
            return MiladiDateTime.CompareTo(other.MiladiDateTime);
        }
    }
   
}

