﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace PythonProject
{
    public static class DbUtility
    {
      private static SqlConnection _connection;

        static DbUtility()
        {
            DbConnection dbConnection = XmlUtility.GetDbConnection();
            SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder
            {
                DataSource = dbConnection.Server,
                UserID = dbConnection.Login,
                Password = dbConnection.Password
            };

            _connection = new SqlConnection(connectionBuilder.ConnectionString);
        }
        


        public static DataTable GetPowerPlants()
        {
            string query = "SELECT distinct [PowerPlantCode],[PowerPlantName],[PowerPlantLatinName],[RegionCode] ,[RegionName] FROM [EMIS].[dbo].[PowerPlantCompany__PV]";
            return GetTable(query);
        }
        public static DataTable GetUnitsCode(string powerPlantCode)
        {
            string query =
                String.Format(
                    "SELECT DISTINCT [UnitCode] FROM [EMIS].[dbo].[UnitContract__PV] WHERE [PowerPlantCode] = '{0}'",
                    powerPlantCode.Trim());
            return GetTable(query);
        }

        /// <summary>
        /// Returns datatable according to select query from DB
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <returns></returns>
        public static DataTable GetTable(string selectQuery)
        {
            return GetTable(selectQuery, 30);
        }
        
        
        /// <summary>
        /// Returns datatable according to select query with specific command timeout 
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        public static DataTable GetTable(string selectQuery, int commandTimeout)
        {
            SqlConnection conn = _connection;
            DataTable resulTable = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(selectQuery, conn);
                   

                cmd.CommandTimeout = commandTimeout;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                adapter.Fill(resulTable);
                return resulTable;
            }
            catch (SqlException e)
            {
                //LogUtility.LogSqlError("DbUtility.GetTable", selectQuery, e);
                throw e;
            }
            finally
            {
                conn.Close();
            }

        }
        /// <summary>
        /// Returns datatable of execution store procedure 
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="parametersWithValues">strore procedure parameters</param>
        /// <returns></returns>
        public static DataTable GetTable(string procedureName, params SqlParameter[] parametersWithValues)
        {
            SqlConnection conn = _connection;
            DataTable resulTable = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(procedureName, conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                for (int i = 0; i < parametersWithValues.Count(); i++)
                {
                    cmd.Parameters.Add(parametersWithValues[i]);
                }
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                adapter.Fill(resulTable);
                return resulTable;
            }
            catch (SqlException e)
            {
                //LogUtility.LogSqlError("DbUtility.GetTable", procedureName, e);
                throw e;
            }
            finally
            {
                conn.Close();
            }

        }
       
        
        /// <summary>
        /// execute  query on DB
        /// </summary>
        /// <param name="query"></param>
        public static void RunQuery(string query)
        {
            RunQuery(query, 60);
        }
      
        /// <summary>
        /// Execute query on DB with specific command timeout
        /// </summary>
        /// <param name="query"></param>
        /// <param name="commandTimeout"></param>
        public static void RunQuery(string query, int commandTimeout)
        {
            SqlConnection conn = _connection;
            try
            {
                SqlCommand cmd = new SqlCommand(query, conn)
                {
                    CommandType = CommandType.Text,
                    CommandTimeout = commandTimeout
                };
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                //  LogUtility.LogSqlError("DbUtility.RunQuery", query, e);
                throw e;
            }
            finally
            {
                conn.Close();
            }

        }
        /// <summary>
        /// execute store procedure 
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="parametersWithValues"></param>
        public static void RunSP(string procedureName, params SqlParameter[] parametersWithValues)
        {
            RunSP(procedureName,30, parametersWithValues);

        }
       
        
        /// <summary>
        /// Exceute store procedure 
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="parametersWithValues"></param>
        public static void RunSP(string procedureName, int commandTimeout, params SqlParameter[] parametersWithValues)
        {
            SqlConnection conn = _connection;
            try
            {
                SqlCommand cmd = new SqlCommand(procedureName, conn)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout=commandTimeout
                };

                for (int i = 0; i < parametersWithValues.Count(); i++)
                {
                    cmd.Parameters.Add(parametersWithValues[i]);
                }

                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                // LogUtility.LogSqlError("DbUtility.RunSP", procedureName, e);
                throw e;

            }
            finally
            {
                conn.Close();
            }

        }


       

        /// <summary>
        /// Run SQL command from DB
        /// </summary>
        /// <param name="command"></param>
        public static void RunCommand(SqlCommand command)
        {
            using (SqlConnection connection = _connection)
            {
                //SqlConnection conn = isDaily ? ConnectionDaily : Connection;
                connection.Open();
                try
                {
                    command.Connection = connection;
                    //command.Connection = conn;
                    //if (conn.State == ConnectionState.Closed)
                    //    conn.Open();

                    command.CommandTimeout = 60;
                    command.ExecuteNonQuery();
                }
                catch (SqlException e)
                {
                    // LogUtility.LogSqlError("DbUtility.RunCommand", command.CommandText, e);
                    throw e;

                }
                //finally
                //{
                //    conn.Close();
                //}
            }

        }

        /// <summary>
        /// check query for syntax error
        /// </summary>
        public static bool CheckQuery(string query, out string msg)
        {

            try
            {
                string noExecQuery = "SET NOEXEC ON;" + query;
                SqlCommand cmd = new SqlCommand(noExecQuery, _connection);
                _connection.Open();
                cmd.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
                msg = e.Message;
                return false;
            }
            finally
            {
                _connection.Close();
            }
            msg = "";
            return true;
        }

        /// <summary>
        /// Returns DataSet according to multiple select query with specific command timeout 
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(string selectQuery, int commandTimeout)
        {
            SqlConnection conn = _connection;
            DataSet resulDataSet = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand(selectQuery, conn);
               // cmd.en

                cmd.CommandTimeout = commandTimeout;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                adapter.Fill(resulDataSet);
                return resulDataSet;
            }
            catch (SqlException e)
            {
                //LogUtility.LogSqlError("DbUtility.GetTable", selectQuery, e);
                throw e;
            }
            finally
            {
                conn.Close();
            }

        }

    }


}
