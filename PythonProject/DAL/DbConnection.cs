﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythonProject
{
    public class DbConnection
    {
        public string Server { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public DbConnection(string server, string login, string password)
        {
            Server = server;
            Login = login;
            Password = password;
        }
    }
}
