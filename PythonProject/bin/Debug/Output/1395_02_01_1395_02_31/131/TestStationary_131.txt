Results of Dickey-Fuller Test:Test Statistic                  -2.399809
p-value                          0.141790
#Lags Used                      20.000000
Number of Observations Used    723.000000
Critical Value (5%)             -2.865546
Critical Value (1%)             -3.439427
Critical Value (10%)            -2.568903
dtype: float64