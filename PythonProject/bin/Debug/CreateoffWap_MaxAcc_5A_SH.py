import warnings
from sklearn.preprocessing import Imputer
import matplotlib.dates as dates
from datetime import datetime
import pyodbc
import pandas as pd
import varconn1
import matplotlib.pyplot as plt 
import numpy as np
import math
from varselclass import varsel
import xml.etree.ElementTree as etree
import xml.etree.cElementTree as ET
import os
def offWap_MaxAcc_5A_SH(powplnt,uncode, path, firstdate,lastdate,table1, table2,logF, movingF,stationaryF,diff, resuF, parF,powSpec,vclass):        
    print(uncode)
    path1=path+'/'+str(powplnt)+'_'+str(uncode)
    try:
            if not os.path.exists(path1):
                os.makedirs(path1)
    except OSError:
        print ('Error: Creating directory. ' +  path)
    qst="Select A.[UnitCode],A.[Date],A.[HourNo],A.[PowerPlantCode],A.[PowerPlantName], A.[Power0], A.[Price0], A.[Power1],A.[Price1],A.[Power2],A.[Price2],A.[Power3],A.[Price3], A.[Power4],A.[Price4],A.[Power5],A.[Price5],A.[Power6],A.[Price6],A.[Power7],A.[Price7], A.[Power8],A.[Price8],A.[Power9],A.[Price9],A.[Power10],A.[Price10], B.[EconomicValue], A.DeclaredCapacity FROM "+ table1+" as A INNER JOIN "+ table2+ " as B ON a.[PowerPlantCode]= B.[PowerPlantCode] and A.[UnitId]=B.[UnitId]  and A.[DayId]=B.[DayId] and A.[HourNo]=B.[HourID] WHERE A.PowerPlantCode=\'" +str(powplnt)+"\'AND A.UnitCode=\'"+str(uncode)+"\'and A.Date between \'"+ firstdate+"\'and \'"+lastdate+"\' ORDER BY A.[Date],A.[HourNo]"
    var=vclass.HourlyUnitCommitment__BV(qst)
    ##misiing DeclaredCapacity
    var22=pd.DataFrame(columns=var.columns) 
    pHub= np.mean(var.DeclaredCapacity)
    for index, row in var.iterrows():
        if row['DeclaredCapacity'] is None or math.isnan (row['DeclaredCapacity']):
            if (pHub>0):
                row['DeclaredCapacity']=pHub
                var22=var22.append(row)
            else:
                row['DeclaredCapacity']=np.mean(var.DeclaredCapacity)
                var22=var22.append(row)
        else:
            pHub=row['DeclaredCapacity']
            var22=var22.append(row)  
    iIndex=0
    dfPlot=pd.DataFrame(columns=["Date", "Hour", "WEP", "offWAP", "maxAcc"])
    for index, row in var22.iterrows():
        WEP=0
        den=0
        dfApp = pd.DataFrame(columns=["Date", "Hour", "WEP", "offWAP", "maxAcc"])
        flag=0
        if (row['DeclaredCapacity'] > 0):
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) : 
                WEP=WEP+row['Power0']*row['Price0']
                den=row['Power0']
                if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                    if float(row['DeclaredCapacity'])< row['Power1'] or (row['Power2'] is None or  math.isnan(row['Power2'])):
                        WEP=WEP+row['Price1'] *(float(row['DeclaredCapacity'])- row['Power0'])
                        flag=1
                    else: 
                        WEP=WEP+row['Price1'] *(row['Power1']- row['Power0'])
                        den=row['Power1']
                    if  flag!=1 and row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                        if float(row['DeclaredCapacity'])< row['Power2']or (row['Power3'] is None or  math.isnan(row['Power3'])):
                            WEP=WEP+row['Price2'] *(float(row['DeclaredCapacity'])- row['Power1'])
                            flag=1
                        else :
                            WEP=WEP+row['Price2'] *(row['Power2']- row['Power1'])
                            den=row['Power2']
                        if  flag!=1 and row['Power3'] is not None and not math.isnan (row['Power3']) : #
                            if float(row['DeclaredCapacity'])< row['Power3']or (row['Power4'] is None or  math.isnan(row['Power4'])):
                                WEP=WEP+row['Price3'] *(float(row['DeclaredCapacity'])- row['Power2'])
                                flag=1
                            else:
                                WEP=WEP+row['Price3'] *(row['Power3']- row['Power2'])
                                den=row['Power3']
                                #print ('3 '+ str(WEP))
                            if  flag!=1 and row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                                if  float(row['DeclaredCapacity'])< row['Power4']or (row['Power5'] is None or  math.isnan(row['Power5'])):
                                    WEP=WEP+row['Price4'] *(float(row['DeclaredCapacity'])- row['Power3'])
                                    #print ('2 '+ str(WEP))
                                    flag=1
                                else:
                                    WEP=WEP+row['Price4'] *(row['Power4']- row['Power3'])
                                    den=row['Power4']
    
                                if  flag!=1 and row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                    if  float(row['DeclaredCapacity'])< row['Power5']or (row['Power6'] is None or  math.isnan(row['Power6'])):
                                        WEP=WEP+row['Price5'] *(float(row['DeclaredCapacity'])- row['Power4'])
                                        flag=1
                                    else:
                                        WEP=WEP+row['Price5'] *(row['Power5']- row['Power4'])
                                        den=row['Power5']
                                    if  flag!=1 and row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                        if float(row['DeclaredCapacity'])< row['Power6']or (row['Power7'] is None or  math.isnan(row['Power7'])):
                                            WEP=WEP+row['Price6'] *(float(row['DeclaredCapacity'])- row['Power5'])
                                            flag=1
                                        else:#
                                            WEP=WEP+row['Price6'] *(row['Power6']- row['Power5'])
                                            den=row['Power6']
                                        if  flag!=1 and row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                            if float(row['DeclaredCapacity'])< row['Power7']or (row['Power8'] is None or  math.isnan(row['Power8'])):
                                                WEP=WEP+row['Price7'] *(float(row['DeclaredCapacity'])- row['Power6'])
                                                flag=1
                                            else:#
                                                WEP=WEP+row['Price7'] *(row['Power7']- row['Power6'])
                                                den=row['Power7']
                                            if  flag!=1 and row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                                if float(row['DeclaredCapacity'])< row['Power8'] or (row['Power9'] is None or  math.isnan(row['Power9'])):
                                                    WEP=WEP+row['Price8'] *(float(row['DeclaredCapacity'])- row['Power7'])
                                                    flag=1
                                                else:#
                                                    WEP=WEP+row['Price8'] *(row['Power8']- row['Power7'])
                                                    den=row['Power8']
                                                if  flag!=1 and row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                    if float(row['DeclaredCapacity'])< row['Power9']or (row['Power10'] is None or  math.isnan(row['Power10'])):
                                                        WEP=WEP+row['Price9'] *(float(row['DeclaredCapacity'])- row['Power8'])
                                                        flag=1
                                                    else:#
                                                        WEP=WEP+row['Price9'] *(row['Power9']- row['Power8'])
                                                        den=row['Power9']
                                                    
                                                    if  flag!=1 and row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                        WEP=WEP+row['Price10'] *(float(row['DeclaredCapacity'])- row['Power9'])
                                                        den=row['Power10']
            Eco=0
            if row['EconomicValue'] is not None and not math.isnan (row['EconomicValue']):
                bound=row['EconomicValue']
            # else:
            #     print( "Economic Value is null")
            if row["Power0"] is not None and not math.isnan (row['Power0']):
                bound=bound+row["Power0"]
            # else:
            #     print("power0 is null")
            if bound is not None and not math.isnan (bound):    
                if row["Power0"] is not None and not math.isnan (row['Power0']) and bound>=row["Power0"] :
                    Eco=row["Price0"]
                if row["Power1"] is not None and not math.isnan (row['Power1']) and bound>=row["Power1"] :
                    Eco=row["Price1"]
                if row["Power2"] is not None and not math.isnan (row['Power2']) and bound>=row["Power2"] :
                    Eco=row["Price2"]
                if row["Power3"] is not None and not math.isnan (row['Power3']) and bound>=row["Power3"] :
                    Eco=row["Price3"]
                if row["Power4"] is not None and not math.isnan (row['Power4']) and bound>=row["Power4"] :
                    Eco=row["Price4"]
                if row["Power5"] is not None and not math.isnan (row['Power5']) and bound>=row["Power5"] :
                    Eco=row["Price5"]
                if row["Power6"] is not None and not math.isnan (row['Power6']) and bound>=row["Power6"] :
                    Eco=row["Price6"]
                if row["Power7"] is not None and not math.isnan (row['Power7']) and bound>=row["Power7"] :
                    Eco=row["Price7"]
                if row["Power8"] is not None and not math.isnan (row['Power8']) and bound>=row["Power8"] :
                    Eco=row["Price8"]
                if row["Power9"] is not None and not math.isnan (row['Power9']) and bound>=row["Power9"] :
                    Eco=row["Price9"]
                if row["Power10"] is not None and not math.isnan (row['Power10']) and bound>=row["Power10"] :
                    Eco=row["Price10"]
            if (row['DeclaredCapacity'] is not None and  not math.isnan(row['DeclaredCapacity'])):  
                den= float(row['DeclaredCapacity'])
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) :
                    den=den-row['Power0']    
            if (WEP!=0):
                dfApp.loc[iIndex]=[row['Date'], row['HourNo'], float((Eco*den)/WEP), float(WEP/den), Eco] 
                iIndex=iIndex+1
                dfPlot=dfPlot.append(dfApp)
        else:
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) : #row['Power0'] is not None: #
                WEP=WEP+row['Power0']*row['Price0']
                den=row['Power0']
                if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                    WEP=WEP+row['Price1'] *(row['Power1']- row['Power0'])
                    den=row['Power1']
                    if  row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                        WEP=WEP+row['Price2'] *(row['Power2']- row['Power1'])
                        den=row['Power2']
                        if  row['Power3'] is not None and not math.isnan (row['Power3']) : #
                            WEP=WEP+row['Price3'] *(row['Power3']- row['Power2'])
                            den=row['Power3']
                            if  row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                                WEP=WEP+row['Price4'] *(row['Power4']- row['Power3'])
                                den=row['Power4']
                                if  row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                    WEP=WEP+row['Price5'] *(row['Power5']- row['Power4'])
                                    den=row['Power5']
                                    if  row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                        WEP=WEP+row['Price6'] *(row['Power6']- row['Power5'])
                                        den=row['Power6']
                                        if  row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                            WEP=WEP+row['Price7'] *(row['Power7']- row['Power6'])
                                            den=row['Power7']
                                            if  row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                                WEP=WEP+row['Price8'] *(row['Power8']- row['Power7'])
                                                den=row['Power8']
                                                if  row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                    WEP=WEP+row['Price9'] *(row['Power9']- row['Power8'])
                                                    den=row['Power9']
                                                    if  row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                        WEP=WEP+row['Price10'] *(row['Power10']- row['Power9'])
                                                        den=row['Power10']
            
            Eco=0
            if row['EconomicValue'] is not None and not math.isnan (row['EconomicValue']):
                bound=row['EconomicValue']
            # else:
            #     print( "Economic Value is null")
            if row["Power0"] is not None and not math.isnan (row['Power0']):
                bound=bound+row["Power0"]
            # else:
            #     print("power0 is null")
            if bound is not None and not math.isnan (bound):    
                if row["Power0"] is not None and not math.isnan (row['Power0']) and bound>=row["Power0"] :
                    Eco=row["Price0"]
                if row["Power1"] is not None and not math.isnan (row['Power1']) and bound>=row["Power1"] :
                    Eco=row["Price1"]
                if row["Power2"] is not None and not math.isnan (row['Power2']) and bound>=row["Power2"] :
                    Eco=row["Price2"]
                if row["Power3"] is not None and not math.isnan (row['Power3']) and bound>=row["Power3"] :
                    Eco=row["Price3"]
                if row["Power4"] is not None and not math.isnan (row['Power4']) and bound>=row["Power4"] :
                    Eco=row["Price4"]
                if row["Power5"] is not None and not math.isnan (row['Power5']) and bound>=row["Power5"] :
                    Eco=row["Price5"]
                if row["Power6"] is not None and not math.isnan (row['Power6']) and bound>=row["Power6"] :
                    Eco=row["Price6"]
                if row["Power7"] is not None and not math.isnan (row['Power7']) and bound>=row["Power7"] :
                    Eco=row["Price7"]
                if row["Power8"] is not None and not math.isnan (row['Power8']) and bound>=row["Power8"] :
                    Eco=row["Price8"]
                if row["Power9"] is not None and not math.isnan (row['Power9']) and bound>=row["Power9"] :
                    Eco=row["Price9"]
                if row["Power10"] is not None and not math.isnan (row['Power10']) and bound>=row["Power10"] :
                    Eco=row["Price10"]                                    
            if (WEP!=0):
                dfApp.loc[iIndex]=[row['Date'], row['HourNo'], float((Eco*den)/WEP), float(WEP/den), Eco] 
                #print ("%f, %f", (WEP, den))
                iIndex=iIndex+1
                dfPlot=dfPlot.append(dfApp)
            
    ## remove dublicate        
    pdate='1300/01/01'
    phur=1
    idx=0
    dftemp=pd.DataFrame(columns=["Date", "Hour", "WEP", "offWAP", "maxAcc"])
    for index, row in dfPlot.iterrows():
        temp=pd.DataFrame(columns=["Date", "Hour", "WEP", "offWAP", "maxAcc"])
        if (pdate== row['Date'] and phur!=row['Hour']) or (pdate!=row['Date'] and phur==row['Hour']) or (pdate!=row['Date'] and phur!=row['Hour']) :
            temp.loc[idx]=[row['Date'],row['Hour'],row['WEP'], row["offWAP"], row["maxAcc"]]
            idx=idx+1
            dftemp=dftemp.append(temp)
            pdate=row['Date']
            phur=row['Hour']
    # print(dftemp.shape)
    writer = pd.ExcelWriter(path1+'/outputWEP_'+powplnt +'_'+uncode+'.xlsx')
    dftemp.to_excel(writer,'Sheet1')
    writer.save()
    data = ET.Element('root')  
    for index,row in dftemp.iterrows():
        records = ET.SubElement(data, 'records')
        iPw=ET.SubElement(records, 'PowerPlant') 
        iU=ET.SubElement(records, 'UnitCode')  
        iDate = ET.SubElement(records, 'Date') 
        ihour = ET.SubElement(records, 'Hour')
        iWEP =  ET.SubElement(records, 'WEP')
        iPw.text =powplnt
        iU.text  =uncode
        iDate.text = row["Date"]  
        ihour.text = str(row["Hour"])
        iWEP.text= "{:.5f}".format(dftemp.WEP[index]) #str(row["WEP"])
    mydata = ET.tostring(data)  
    myfile = open("items2.xml", "wb")  
    myfile.write(mydata)  
    myfile.close()
    
    f=plt.figure()
    plt.plot(dftemp.WEP)#, linestyle='',marker='.')
    plt.ylabel('OFFWAP/maxAcc')
    plt.title('OffWAP/maxAcc for '+ uncode + ' of ' + 'PowerPlant with code= '+ powplnt + ' Date:'+ dftemp.iloc[0].Date+' to ' + dftemp.iloc[dftemp.shape[0]-1].Date)
    #f.show()
    plt.savefig(path1+'/OriginalData_'+powplnt +'_'+uncode+'.pdf', format='pdf', dpi=1200)
    xx=sum(x != 0 for x in dfPlot.WEP)
    if xx==0:
        text_file = open(path1+'/outputWEP_'+powplnt +'_'+uncode+'.txt', 'w')
        text_file.write('Error:  all values in the time series are Zero')
        text_file.close()
        import subprocess as sp
        programName = "notepad.exe"
        fileName = path1+'/outputWEP_'+powplnt +'_'+uncode+'.txt'
        sp.Popen([programName, fileName])
        
    if xx!=0:
        from Outlier1 import plotOutLier 
        plotOutLier(dftemp.WEP,powplnt +'_'+uncode,path1)
        ts=dfPlot.WEP
        if (logF==1):
            tss=np.where(ts == 0, 0.00001,ts)
            ts=pd.Series(tss)
            ts_log = np.log(ts)
        else:
            ts_log=ts
        if (movingF!=0):
            moving_avg = pd.rolling_mean(ts_log,24)
            ts_log_moving_avg_diff = ts_log - moving_avg
            ts_log_moving_avg_diff.dropna(inplace=True)
            ts_log=ts_log_moving_avg_diff
        if (diff!=0):
            ts_log_diff = ts_log - ts_log.shift()
            ts_log_diff.dropna(inplace=True)
        else:
            ts_log_diff=ts_log
        if (stationaryF ==1):
            name_file=path1+'/Data_'+str(powplnt)+'_'+str(uncode)+'.txt'
            text_file = open(name_file, "w")
            text_file.write("%s" % ts_log_diff)
            text_file.close()
            from test_stationarity import test_stationarity
            test_stationarity(ts_log_diff, 'Data', 24, powplnt +'_'+uncode,path1)
        if(parF==1):
            from statsmodels.tsa.stattools import acf, pacf
            ts_log_diff.dropna(inplace=True)
            lag_acf = acf(ts_log_diff, nlags=24)
            lag_pacf = pacf(ts_log_diff, nlags=24, method='ols')
            m=plt.figure()
            #Plot ACF: 
            plt.subplot(121) 
            plt.plot(lag_acf)
            plt.axhline(y=0,linestyle='--',color='gray')
            plt.axhline(y=-1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.axhline(y=1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.title('Autocorrelation Function')
            #Plot PACF:
            plt.subplot(122)
            plt.plot(lag_pacf)
            plt.axhline(y=0,linestyle='--',color='gray')
            plt.axhline(y=-1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.axhline(y=1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.title('Partial Autocorrelation Function')
            plt.tight_layout()
           # m.show()
            plt.savefig(path1+'/Autocorrelation_'+powplnt+'_'+uncode+'.pdf', format='pdf', dpi=1300)
        if (resuF==1):
            from seasonality import test_seasonality
            test_seasonality(ts_log_diff, 24, powplnt +'_'+uncode,path1)
        if(powSpec==1):
            x=ts_log_diff- ts_log_diff.mean() #data.WEP - data.WEP.mean()
            ps = np.abs(np.fft.fft(x))**2
            time_step = 1/24
            freqs = np.fft.fftfreq(ts_log_diff.size, time_step)
            idx   = np.argsort(freqs)
            gp1=plt.figure()
            plt.plot(freqs[idx], ps[idx])
            plt.title('Power Spectrum analysis')
            #gp1.show()
            plt.savefig(path1+'/PowerSpectrum_'+powplnt+'_'+uncode+'.pdf', format='pdf', dpi=1300)
