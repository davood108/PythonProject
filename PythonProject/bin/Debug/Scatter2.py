try:
    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)
    import numpy as np
    warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
    from varselclass import varsel
    vclass=varsel()
    import pyodbc
    import pandas as pd
    import varconn1
    import matplotlib.pyplot as plt 
    import numpy 
    import seaborn as sns
    import xml.etree.ElementTree as etree
    import xml.etree.cElementTree as ET
    import os
    while True:
        try:
            tree = etree.parse('Input.xml')
            break
        except OSError:
            print("Oops!  That was no valid Attribute in Table, Try again...")
    root = tree.getroot()
    powplntList= root.find("PowerPlant").text
    powplntList= powplntList.split(',')
    uncodeList=root.find("UnitCode").text 
    uncodeList=uncodeList.split(',')
    firstdate=root.find("FirstDate").text 
    lastdate=root.find("LastDate").text
    Hur=root.find("Hour").text 
    table= root.find("Table").text 
    inp= root.find("Attribute1").text
    inp = inp[:-1]
    inp = inp[1:]
    table2= root.find("Table2").text 
    inp2= root.find("Attribute2").text
    inp2 = inp2[:-1]
    inp2 = inp2[1:]
    Rev='1' #root.find("Revision").text 
    lastRev= '0' #root.find("IsLastRevision").text 
    HasF='1' #root.find("HasFuel").text 
    isUnit=int(root.find("isUnit").text)
    path="./Output/"+firstdate.replace('/','_')+'_'+lastdate.replace('/','_')
    if isUnit==1:
        for i in range(len(uncodeList)):
            powplnt = powplntList[i]
            uncode = uncodeList[i]
            print("PlantCode: "+powplnt)#******
            print(uncode)#********
            path1=path+'/'+str(powplnt)+'_'+str(uncode)
            try:
                    if not os.path.exists(path1):
                        os.makedirs(path1)
            except OSError:
                    print ('Error: Creating directory. ' +  path1)
            var1=varconn1.varconn(powplnt,uncode,firstdate, lastdate,Hur, table, inp, Rev, lastRev,HasF)
            if (var1.shape[0] == 0):
                exit()
            x=var1 
            row1=var1.count()####
            var2=varconn1.varconn(powplnt,uncode,firstdate, lastdate,Hur, table2, inp2, Rev, lastRev,HasF)
            if (var2.shape[0] == 0):
                exit()
            y=var2 
            row2=var2.count()
            s=min(row1,row2)
            x1=x.iloc[0:s-1]
            x2=y.iloc[0:s-1]
            f=plt.figure()
            plt.scatter(x1,x2)
            plt.title("Scatter Plot of "+ inp + ' and '+ inp2)
            plt.xlabel(inp)
            plt.ylabel(inp2)
            plt.savefig(path1+'/Scatter_'+powplnt +'_'+uncode+'.pdf', format='pdf', dpi=2000)
            plt.clf()    
    else:
        for i in range(len(powplntList)):
            powplnt = powplntList[i]
            print("PlantCode: "+powplnt)#******
            qst="SELECT distinct [UnitCode] As unc FROM "+ table + " WHERE PowerPlantCode=\'" +str(powplnt)+ "\' ORDER BY UnitCode"
            unnc=vclass.HourlyUnitCommitment__BV(qst)
            Mystring=','.join(map(str, unnc.unc)) 
            un=Mystring.split(',')
            idx=0
            for uncode in un:
                print(uncode)#***********
                path1=path+'/'+str(powplnt)+'_'+str(uncode)
                try:
                        if not os.path.exists(path1):
                            os.makedirs(path1)
                except OSError:
                        print ('Error: Creating directory. ' +  path1)
                var1=varconn1.varconn(powplnt,uncode,firstdate, lastdate,Hur, table, inp, Rev, lastRev,HasF)
                if (var1.shape[0] == 0):
                    exit()
                x=var1 
                row1=var1.count()####
                var2=varconn1.varconn(powplnt,uncode,firstdate, lastdate,Hur, table2, inp2, Rev, lastRev,HasF)
                if (var2.shape[0] == 0):
                    exit()
                y=var2 
                row2=var2.count()
                s=min(row1,row2)
                x1=x.iloc[0:s-1]
                x2=y.iloc[0:s-1]
                f=plt.figure()
                plt.scatter(x1,x2)
                plt.title("Scatter Plot of "+ inp + ' and '+ inp2)
                plt.xlabel(inp)
                plt.ylabel(inp2)
                plt.savefig(path1+'/Scatter_'+powplnt +'_'+uncode+'.pdf', format='pdf', dpi=2000)
                plt.clf()
    print("******Finish******")
    input()
except Exception as inst:
    print("Unexpected error:")
    print (type(inst))    # the exception instance     
    print (inst )          # __str__ allows args to be printed directly
    raise
    