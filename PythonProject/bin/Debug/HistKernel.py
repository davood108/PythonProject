try:
    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)
    import numpy as np
    warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
    from varselclass import varsel
    vclass=varsel()
    import pyodbc
    import pandas as pd
    import varconn1
    import matplotlib.pyplot as plt 
    plt.rcParams.update({'figure.max_open_warning': 0})
    import numpy  as np
    warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning) 
    import seaborn as sns
    import xml.etree.ElementTree as etree
    import xml.etree.cElementTree as ET
    import os
    tree = etree.parse('Input.xml')
    root = tree.getroot()
    powplntList= root.find("PowerPlant").text
    powplntList= powplntList.split(',')
    uncodeList=root.find("UnitCode").text 
    uncodeList=uncodeList.split(',')
    firstdate=root.find("FirstDate").text 
    lastdate=root.find("LastDate").text
    Hur=root.find("Hour").text 
    table= root.find("Table").text 
    inp= root.find("Attribute1").text
    inp = inp[:-1]
    inp = inp[1:]
    Rev='1' #root.find("Revision").text 
    lastRev= '0' #root.find("IsLastRevision").text 
    HasF='1' #root.find("HasFuel").text 
    isUnit=int(root.find("isUnit").text)
    path="./Output/"+firstdate.replace('/','_')+'_'+lastdate.replace('/','_')
    if isUnit==1:
        for i in range(len(uncodeList)):
            powplnt = powplntList[i]
            uncode = uncodeList[i]
            print("PlantCode: "+powplnt)#******
            print(uncode)
            path1=path+'/'+str(powplnt)+'_'+str(uncode)
            try:
                    if not os.path.exists(path1):
                        os.makedirs(path1)
            except OSError:
                    print ('Error: Creating directory. ' +  path)
            plt.figure()
            var=varconn1.varconn(powplnt,uncode,firstdate, lastdate,Hur, table, inp,Rev, lastRev,HasF)
            sns.distplot(var);
            plt.xlabel(inp)
            plt.ylabel('Frequency')
            plt.title("\n")
            plt.title('Plantcode %s Unitcode %s Histogram and kernel density estimation of %s' % (powplnt,uncode, inp))
            #plt.show()
            plt.savefig(path1+'/Histkernel_'+powplnt +'_'+uncode+'.pdf', format='pdf', dpi=1200)
            plt.clf()
    else:
        for i in range(len(powplntList)):
            powplnt = powplntList[i]
            print("PlantCode: "+powplnt)#******
            qst="SELECT distinct [UnitCode] As unc FROM "+ table + " WHERE PowerPlantCode=\'" +str(powplnt)+ "\' ORDER BY UnitCode"
            unnc=vclass.HourlyUnitCommitment__BV(qst)
            Mystring=','.join(map(str, unnc.unc)) 
            un=Mystring.split(',')
            idx=0
            for uncode in un:
                print(uncode)
                path1=path+'/'+str(powplnt)+'_'+str(uncode)
                try:
                        if not os.path.exists(path1):
                            os.makedirs(path1)
                except OSError:
                        print ('Error: Creating directory. ' +  path)
                plt.figure()
                var=varconn1.varconn(powplnt,uncode,firstdate, lastdate,Hur, table, inp,Rev, lastRev,HasF)
                sns.distplot(var);
                plt.xlabel(inp)
                plt.ylabel('Frequency')
                plt.title("\n")
                plt.title('Plantcode %s Unitcode %s Histogram and kernel density estimation of %s' % (powplnt,uncode, inp))
                #plt.show()
                plt.savefig(path1+'/Histkernel_'+powplnt +'_'+uncode+'.pdf', format='pdf', dpi=1200)
                plt.clf()
    print("******Finish******") 
    input()
except Exception as inst:
    print("Unexpected error:")
    print (type(inst))    # the exception instance     
    print (inst )          # __str__ allows args to be printed directly
    raise

