import warnings
from sklearn.preprocessing import Imputer
import matplotlib.dates as dates
from datetime import datetime
import pyodbc
import pandas as pd
import varconn1
import matplotlib.pyplot as plt 
import numpy as np
import math
from varselclass import varsel
import xml.etree.ElementTree as etree
import xml.etree.cElementTree as ET
import os
def Power_10A_SH(powplnt,uncode, path, firstdate,lastdate,table1, table2,logF, movingF,stationaryF,diff, resuF, parF,powSpec,vclass):
    print(uncode)
    path1=path+'/'+str(powplnt)+'_'+str(uncode)
    try:
            if not os.path.exists(path1):
                os.makedirs(path1)
    except OSError:
            print ('Error: Creating directory. ' +  path)
    qst="Select A.[UnitCode],A.[Date],A.[HourNo],A.[PowerPlantCode],A.[PowerPlantName], A.[Power0], A.[Price0], A.[Power1],A.[Price1],A.[Power2],A.[Price2],A.[Power3],A.[Price3], A.[Power4],A.[Price4],A.[Power5],A.[Price5],A.[Power6],A.[Price6],A.[Power7],A.[Price7], A.[Power8],A.[Price8],A.[Power9],A.[Price9],A.[Power10],A.[Price10], A.DeclaredCapacity FROM "+ table1+" as A WHERE A.PowerPlantCode=\'" +str(powplnt)+"\'AND A.UnitCode=\'"+str(uncode)+"\'and A.Date between \'"+ firstdate+"\'and \'"+lastdate+"\' ORDER BY A.[Date],A.[HourNo]"
    var=vclass.HourlyUnitCommitment__BV(qst)
    ##misiing DeclaredCapacity
    var22=pd.DataFrame(columns=var.columns) 
    pHub= np.mean(var.DeclaredCapacity)
    for index, row in var.iterrows():
        if row['DeclaredCapacity'] is None or math.isnan (row['DeclaredCapacity']):
            if (pHub>0):
                row['DeclaredCapacity']=pHub
                var22=var22.append(row)
            else:
                row['DeclaredCapacity']=np.mean(var.DeclaredCapacity)
                var22=var22.append(row)
        else:
            pHub=row['DeclaredCapacity']
            var22=var22.append(row) 
    iIndex=0
    dfPlot=pd.DataFrame(columns=["Date", "Hour", "WEP", "Max", "Min", "Pday"])
    for index, row in var22.iterrows():
        nb=0
        dfApp = pd.DataFrame(columns=["Date", "Hour", "WEP", "Max", "Min", "Pday"])
        if (row['DeclaredCapacity'] > 0):
            den=row['DeclaredCapacity']
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) :
                    den=den-row['Power0'] 
        else:
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) : 
                den=row['Power0']
                if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                    den=row['Power1']
                    if  row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                        den=row['Power2']
                        if  row['Power3'] is not None and not math.isnan (row['Power3']) : #
                            den=row['Power3']
                            if  row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                                den=row['Power4']
                                if  row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                    den=row['Power5']
                                    if  row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                        den=row['Power6']
                                        if  row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                            den=row['Power7']
                                            if  row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                                den=row['Power8']
                                                if  row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                    den=row['Power9']
                                                    if  row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                        den=row['Power10']
        MinQ=1000
        MaxQ=0
        if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) : 
            if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                Q=(row['Power1']-row['Power0'])
                if Q<MinQ:
                    MinQ=Q
                if Q >MaxQ:
                    MaxQ=Q
                if  row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                    Q=(row['Power2']-row['Power1'])
                    if Q<MinQ:
                        MinQ=Q
                    if Q >MaxQ:
                        MaxQ=Q
                    if  row['Power3'] is not None and not math.isnan (row['Power3']) : #
                        Q=(row['Power3']-row['Power2'])
                        if Q<MinQ:
                            MinQ=Q
                        if Q >MaxQ:
                            MaxQ=Q
                        if  row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                            Q=(row['Power4']-row['Power3'])
                            if Q<MinQ:
                                MinQ=Q
                            if Q >MaxQ:
                                MaxQ=Q
                            if  row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                Q=(row['Power5']-row['Power4'])
                                if Q<MinQ:
                                    MinQ=Q
                                if Q >MaxQ:
                                    MaxQ=Q
                                if  row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                    Q=(row['Power6']-row['Power5'])
                                    if Q<MinQ:
                                        MinQ=Q
                                    if Q >MaxQ:
                                        MaxQ=Q
                                    if  row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                        Q=(row['Power7']-row['Power6'])
                                        if Q<MinQ:
                                            MinQ=Q
                                        if Q >MaxQ:
                                            MaxQ=Q
                                        if  row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                            Q=(row['Power8']-row['Power7'])
                                            if Q<MinQ:
                                                MinQ=Q
                                            if Q >MaxQ:
                                                MaxQ=Q
                                            if  row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                Q=(row['Power9']-row['Power8'])
                                                if Q<MinQ:
                                                    MinQ=Q
                                                if Q >MaxQ:
                                                    MaxQ=Q
                                                if  row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                    Q=(row['Power10']-row['Power9'])
                                                    if Q<MinQ:
                                                        MinQ=Q
                                                    if Q >MaxQ:
                                                        MaxQ=Q
        if (MaxQ>0):
            dfApp.loc[iIndex]=[row['Date'], row['HourNo'], float((MaxQ-MinQ)/MaxQ), MaxQ, MinQ, den] 
            iIndex=iIndex+1
            dfPlot=dfPlot.append(dfApp)
    ## remove dublicate        
    pdate='1300/01/01'
    phur=1
    idx=0
    dftemp=pd.DataFrame(columns=["Date", "Hour", "WEP", "Max", "Min", "Pday"])
    for index, row in dfPlot.iterrows():
        temp=pd.DataFrame(columns=["Date", "Hour", "WEP", "Max", "Min", "Pday"])
        if (pdate== row['Date'] and phur!=row['Hour']) or (pdate!=row['Date'] and phur==row['Hour']) or (pdate!=row['Date'] and phur!=row['Hour']) :
            temp.loc[idx]=[row['Date'],row['Hour'],row['WEP'],row["Max"],row["Min"], row["Pday"]]
            idx=idx+1
            dftemp=dftemp.append(temp)
            pdate=row['Date']
            phur=row['Hour']
    # print(dftemp.shape)
    writer = pd.ExcelWriter(path1+'/outputWEP_'+powplnt +'_'+uncode+'.xlsx')
    dftemp.to_excel(writer,'Sheet1')
    writer.save()
    data = ET.Element('root')  
    for index,row in dftemp.iterrows():
        records = ET.SubElement(data, 'records')
        iPw=ET.SubElement(records, 'PowerPlant') 
        iU=ET.SubElement(records, 'UnitCode')  
        iDate = ET.SubElement(records, 'Date') 
        ihour = ET.SubElement(records, 'Hour')
        iWEP =  ET.SubElement(records, 'WEP')
        iPw.text =powplnt
        iU.text  =uncode
        iDate.text = row["Date"]  
        ihour.text = str(row["Hour"])
        iWEP.text= "{:.5f}".format(dftemp.WEP[index]) #str(row["WEP"])
    mydata = ET.tostring(data)  
    myfile = open("items2.xml", "wb")  
    myfile.write(mydata)  
    myfile.close()
    f=plt.figure()
    plt.plot(dftemp.WEP)#, linestyle='',marker='.')
    plt.ylabel('(Max-Min)/Max')
    plt.title('(Max-Min)/Max  of for '+ uncode + ' of ' + 'PowerPlant with code= '+ powplnt + ' Date:'+ dftemp.iloc[0].Date+' to ' + dftemp.iloc[dftemp.shape[0]-1].Date)
    #f.show()
    plt.savefig(path1+'/OriginalData_'+powplnt +'_'+uncode+'.pdf', format='pdf', dpi=1200)
    
    xx=sum(x != 0 for x in dfPlot.WEP)
    if xx==0:
        text_file = open("Output.txt", "w")
        text_file.write('Error:  all values in the time series are Zero')
        text_file.close()
        import subprocess as sp
        programName = "notepad.exe"
        fileName = "Output.txt"
        sp.Popen([programName, fileName])
    if xx!=0:
        from Outlier1 import plotOutLier 
        plotOutLier(dftemp.WEP,powplnt +'_'+uncode,path1)
        ts=dfPlot.WEP
        if (logF==1):
            tss=np.where(ts == 0, 0.00001,ts)
            ts=pd.Series(tss)
            ts_log = np.log(ts)
        else:
            ts_log=ts
        if (movingF!=0):
            moving_avg = pd.rolling_mean(ts_log,24)
            ts_log_moving_avg_diff = ts_log - moving_avg
            ts_log_moving_avg_diff.dropna(inplace=True)
            ts_log=ts_log_moving_avg_diff
        if (diff!=0):
            ts_log_diff = ts_log - ts_log.shift()
            ts_log_diff.dropna(inplace=True)
        else:
            ts_log_diff=ts_log
        if (stationaryF ==1):
            name_file='Data_'+str(powplnt)+'_'+str(uncode)+'.txt'
            text_file = open(name_file, "w")
            text_file.write("%s" % ts_log_diff)
            text_file.close()
            from test_stationarity import test_stationarity
            test_stationarity(ts_log_diff, 'Data', 24, powplnt +'_'+uncode,path1)
        if(parF==1):
            from statsmodels.tsa.stattools import acf, pacf
            ts_log_diff.dropna(inplace=True)
            lag_acf = acf(ts_log_diff, nlags=24)
            lag_pacf = pacf(ts_log_diff, nlags=24, method='ols')
            m=plt.figure()
            #Plot ACF: 
            plt.subplot(121) 
            plt.plot(lag_acf)
            plt.axhline(y=0,linestyle='--',color='gray')
            plt.axhline(y=-1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.axhline(y=1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.title('Autocorrelation Function')
            #Plot PACF:
            plt.subplot(122)
            plt.plot(lag_pacf)
            plt.axhline(y=0,linestyle='--',color='gray')
            plt.axhline(y=-1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.axhline(y=1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.title('Partial Autocorrelation Function')
            plt.tight_layout()
            #m.show()
            plt.savefig(path1+'/Autocorrelation_'+powplnt+'_'+uncode+'.pdf', format='pdf', dpi=1300)
        if (resuF==1):
            from seasonality import test_seasonality
            test_seasonality(ts_log_diff, 24, powplnt +'_'+uncode,path1)
        if(powSpec==1):
            x=ts_log_diff- ts_log_diff.mean() #data.WEP - data.WEP.mean()
            ps = np.abs(np.fft.fft(x))**2
            time_step = 1/24
            freqs = np.fft.fftfreq(ts_log_diff.size, time_step)
            idx   = np.argsort(freqs)
            gp1=plt.figure()
            plt.plot(freqs[idx], ps[idx])
            plt.title('Power Spectrum analysis')
            #gp1.show()
            plt.savefig(path1+'/PowerSpectrum_'+powplnt+'_'+uncode+'.pdf', format='pdf', dpi=1300)
