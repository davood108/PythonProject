try: 
    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)   
    from sklearn.preprocessing import Imputer
    from varselclass import varsel
    import matplotlib.dates as dates
    from datetime import datetime
    import pyodbc
    import pandas as pd
    import varconn1
    import matplotlib.pyplot as plt 
    plt.rcParams.update({'figure.max_open_warning': 0})
    import numpy as np
    warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
    import math
    from varselclass import varsel
    vclass=varsel()
    import xml.etree.ElementTree as etree
    import xml.etree.cElementTree as ET
    tree = etree.parse('Input2.xml')
    root = tree.getroot()
    powplntList= root.find("PowerPlant").text
    powplntList= powplntList.split(',')
    uncodeList=root.find("UnitCode").text 
    uncodeList=uncodeList.split(',')
    firstdate=root.find("FirstDate").text 
    lastdate=root.find("LastDate").text 
    table1=root.find("Table").text 
    table2=root.find("Table2").text 
    logF=int(root.find("log").text)
    movingF= int(root.find("moving").text)
    stationaryF= int(root.find("stationary").text)
    diff=int(root.find("difference").text)
    resuF=int(root.find("season").text)
    parF=int(root.find("Autocor").text)
    powSpec=int(root.find("powSpec").text)
    isUnit=int(root.find("isUnit").text)
    path="./Output/"+firstdate.replace('/','_')+'_'+lastdate.replace('/','_')
    if isUnit==1:
        for i in range(len(uncodeList)):
            powplnt = powplntList[i]
            uncode = uncodeList[i]
            print("PlantCode: "+powplnt)#******
            from CreateWepCap2A_edited_SH import WepCap2A_SH
            WepCap2A_SH(powplnt,uncode, path, firstdate,lastdate,table1, table2,logF, movingF,stationaryF,diff, resuF, parF,powSpec,vclass)
    else:
        for i in range(len(powplntList)):
            powplnt = powplntList[i]
            print("PlantCode: "+powplnt)#******
            qst="SELECT distinct [UnitCode] As unc FROM "+ table1 + " WHERE PowerPlantCode=\'" +str(powplnt)+ "\' ORDER BY UnitCode"
            unnc=vclass.HourlyUnitCommitment__BV(qst)
            Mystring=','.join(map(str, unnc.unc)) 
            un=Mystring.split(',')
            idx=0
            for uncode in un:
                from CreateWepCap2A_edited_SH import WepCap2A_SH
                WepCap2A_SH(powplnt, uncode, path, firstdate, lastdate, table1, table2,logF, movingF,stationaryF,diff, resuF, parF,powSpec,vclass)
    print("******Finish******") #******   
    input()
except Exception as inst:
    print("Unexpected error:")
    print (type(inst))    # the exception instance     
    print (inst )          # __str__ allows args to be printed directly
    input() #raise

