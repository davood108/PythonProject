try:
    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)
    from sklearn.preprocessing import Imputer
    from varselclass import varsel
    import matplotlib.dates as dates
    from datetime import datetime
    import seaborn as sns
    import pyodbc
    import pandas as pd
    import varconn1
    import matplotlib.pyplot as plt 
    plt.rcParams.update({'figure.max_open_warning': 0})
    import numpy as np
    warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
    import math
    from varselclass import varsel
    vclass=varsel()
    import xml.etree.ElementTree as etree
    import xml.etree.cElementTree as ET
    tree = etree.parse('Input2.xml')
    root = tree.getroot()
    OurPowList = root.find("PowerPlant").text
    OurPowList=OurPowList.split(',')
    OurUnitList=root.find("UnitCode").text
    OurUnitList= OurUnitList.split(',')
    #print(OurUnitList)
    firstdate=root.find("FirstDate").text 
    lastdate=root.find("LastDate").text 
    table1=root.find("Table").text
    isUnit=int(root.find("isUnit").text)
    path="./Output/"+firstdate.replace('/','_')+'_'+lastdate.replace('/','_')
    df_Final=pd.DataFrame(columns=["Date","Hour","WEP", "WEPT","PDayT"])
    flag=0
    if isUnit==1:
        for i in range(len(OurUnitList)):
            powplnt = OurPowList[i]
            uncode = OurUnitList[i]
            print("PlantCode: "+powplnt)#******
            #print ("flag= "+ str(flag))
            dftemp=pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","Unit","Power"])
            from CreateCross_11_SH import Cross_11_SH
            df_Final= Cross_11_SH(powplnt,uncode, path, firstdate,lastdate,table1,vclass, flag, dftemp,df_Final,plt)
            if df_Final.shape[0]!=0 :
                flag=1
    else:
        for i in range(len(OurPowList)):
            powplnt = OurPowList[i]
            print("PlantCode: "+powplnt)#******
            qst="SELECT distinct [UnitCode] As unc FROM "+ table1 + " WHERE PowerPlantCode=\'" +str(powplnt)+ "\' ORDER BY UnitCode"
            unnc=vclass.HourlyUnitCommitment__BV(qst)
            Mystring=','.join(map(str, unnc.unc)) 
            un=Mystring.split(',')
            idx=0
            dftemp=pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","Unit","Power"])
            #print ("flag= "+ str(flag))
            for uncode in un:
                from CreateCross_11_SH import Cross_11_SH
                df_Final=Cross_11_SH(powplnt,uncode, path, firstdate,lastdate,table1,vclass, flag, dftemp,df_Final, plt)
                if df_Final.shape[0]!=0 :
                    flag=1
                
    print("******Finish******") #******  
    input()
except Exception as inst:
    print("Unexpected error:")
    print (type(inst))    # the exception instance     
    input()        # __str__ allows args to be printed directly
    raise
        
