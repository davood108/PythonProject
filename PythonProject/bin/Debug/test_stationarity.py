from statsmodels.tsa.stattools import adfuller
import pandas as pd
import matplotlib.pyplot as plt 
def test_stationarity(timeseries, desc, win, powplntUncode, path):
    
    #Determing rolling statistics
    rolmean = pd.rolling_mean(timeseries, window=win)
    rolstd = pd.rolling_std(timeseries, window=win)

    #Plot rolling statistics:
    g = plt.figure()
    orig = plt.plot(timeseries, color='blue',label='Original')
    mean = plt.plot(rolmean, color='red', label='Rolling Mean')
    std = plt.plot(rolstd, color='black', label = 'Rolling Std')
    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation : '+ desc)
    #g.show()
    plt.savefig(path+'/TestStationary_'+powplntUncode+'.pdf', format='pdf', dpi=1300)
    plt.clf()
    
    #Perform Dickey-Fuller test:
    
    dftest = adfuller(timeseries, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
    for key,value in dftest[4].items():
        dfoutput['Critical Value (%s)'%key] = value
    name_file=path+'/TestStationary_'+str(powplntUncode)+'.txt'
    #print(name_file)
    text_file = open(name_file, "w")
    text_file.write("%s" % 'Results of Dickey-Fuller Test:')
    text_file.write("%s" % dfoutput)
    text_file.close()
    # import subprocess as sp
    # programName = "notepad.exe"
    # sp.Popen([programName, name_file])
    #print(dfoutput)