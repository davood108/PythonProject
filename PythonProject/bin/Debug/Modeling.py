import matplotlib.pyplot as plt 
from statsmodels.tsa.arima_model import ARIMA
import pandas as pd
import numpy as np
def Modeling_ARIMA(ts, p,d,q):
    #AR model
    ts_log=np.log(ts)
    ts_log.index = pd.to_datetime(ts_log.index)
    model = ARIMA(ts_log, order=(p, d, 0))  
    results_AR = model.fit(disp=-1)  
    m1=plt.figure()
    ts_log_diff = ts_log - ts_log.shift()
    plt.plot(ts_log_diff)
    plt.plot(results_AR.fittedvalues, color='red')
    plt.title('AR')#RSS: %.4f'% sum((results_AR.fittedvalues-ts_log_diff)**2))
    m1.show()
    #MA model
    model = ARIMA(ts_log, order=(0, d, q))  
    results_MA = model.fit(disp=-1) 
    m2=plt.figure() 
    plt.plot(ts_log_diff)
    plt.plot(results_MA.fittedvalues, color='red')
    plt.title('MA')#RSS: %.4f'% sum((results_MA.fittedvalues-ts_log_diff)**2))
    m2.show()
    # combined 
    model = ARIMA(ts_log, order=(p, d, q))  
    results_ARIMA = model.fit(disp=-1)  
    m3=plt.figure()
    plt.plot(ts_log_diff)
    plt.plot(results_ARIMA.fittedvalues, color='red')
    plt.title('ARIMA')#RSS: %.4f'% sum((results_ARIMA.fittedvalues-ts_log_diff)**2))
    m3.show()
    #results_ARIMA=results_MA
    predictions_ARIMA_diff = pd.Series(results_ARIMA.fittedvalues, copy=True)
    predictions_ARIMA_diff_cumsum = predictions_ARIMA_diff.cumsum()
    predictions_ARIMA_log = pd.Series(ts_log.ix[0], index=ts_log.index)
    predictions_ARIMA_log = predictions_ARIMA_log.add(predictions_ARIMA_diff_cumsum,fill_value=0)
    predictions_ARIMA = np.exp(predictions_ARIMA_log)
    
    m4=plt.figure()
    plt.plot(ts)
    plt.plot(predictions_ARIMA)
    plt.title('Prediction ')#RMSE: %.4f'% np.sqrt(sum((predictions_ARIMA-ts)**2)/len(ts)))
    m4.show()