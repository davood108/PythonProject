import warnings
from sklearn.preprocessing import Imputer
import matplotlib.dates as dates
from datetime import datetime
import pyodbc
import pandas as pd
import varconn1
import matplotlib.pyplot as plt 
import numpy as np
import math
from varselclass import varsel
import xml.etree.ElementTree as etree
import xml.etree.cElementTree as ET
import os

def MaxReq_5A_SH(powplnt, uncode, path, firstdate, lastdate, table1, table2,logF, movingF,stationaryF,diff, resuF, parF,powSpec,vclass):
    print(uncode)
    path1=path+'/'+str(powplnt)+'_'+str(uncode)
    try:
            if not os.path.exists(path1):
                os.makedirs(path1)
    except OSError:
            print ('Error: Creating directory. ' +  path) 
    # qst1="Select A.[UnitCode],A.[Date],A.[HourNo],A.[PowerPlantCode],A.[PowerPlantName], A.[Power0], A.[Price0], A.[Power1],A.[Price1],A.[Power2],A.[Price2],A.[Power3],A.[Price3], A.[Power4],A.[Price4],A.[Power5],A.[Price5],A.[Power6],A.[Price6],A.[Power7],A.[Price7], A.[Power8],A.[Price8],A.[Power9],A.[Price9],A.[Power10],A.[Price10] FROM "+ table1+" as A WHERE Date between \'"+ firstdate+"\'and \'"+lastdate+"\' ORDER BY [Date],[HourNo]"
    # var=vclass.HourlyUnitCommitment__BV(qst1)
    # print(var.shape)
    # from PriceCap import Det_PriceCap
    # CapData=Det_PriceCap(var,uncode )
    # writer = pd.ExcelWriter('outputPriceCap.xlsx')
    # CapData.to_excel(writer,'Sheet1')
    # writer.save()
    qst2="Select A.[UnitCode],A.[Date],A.[HourNo],A.[PowerPlantCode], A.[Power0], A.[Price0], A.[Power1],A.[Price1],A.[Power2],A.[Price2],A.[Power3],A.[Price3], A.[Power4],A.[Price4],A.[Power5],A.[Price5],A.[Power6],A.[Price6],A.[Power7],A.[Price7], A.[Power8],A.[Price8],A.[Power9],A.[Price9],A.[Power10],A.[Price10], B.[RequiredValue] FROM "+ table1+" as A INNER JOIN "+ table2+ " as B ON a.[PowerPlantCode]= B.[PowerPlantCode] and A.[UnitId]=B.[UnitId]  and A.[DayId]=B.[DayId] and A.[HourNo]=B.[HourID] WHERE A.PowerPlantCode=\'" +str(powplnt)+"\'AND A.UnitCode=\'"+uncode+"\'and A.Date between \'"+ firstdate+"\'and \'"+lastdate+"\' ORDER BY A.[Date],A.[HourNo]"
    
    var2=vclass.HourlyUnitCommitment__BV(qst2)
    iIndex=0
    dfPlot=pd.DataFrame(columns=["Date", "Hour", "Max", "Eco","WEP"])
    for index, row in var2.iterrows():
        dfApp = pd.DataFrame(columns=["Date", "Hour", "Max", "Eco","WEP"])
        maxC=0
        if row["Price10"] is not None and not math.isnan (row['Price10']):
            maxC=row["Price10"]
        elif row["Price9"] is not None and not math.isnan (row['Price9']):
            maxC=row["Price9"]
        elif row["Price9"] is not None and not math.isnan (row['Price8']):
            maxC=row["Price8"]
        elif row["Price9"] is not None and not math.isnan (row['Price7']):
            maxC=row["Price7"]
        elif row["Price9"] is not None and not math.isnan (row['Price6']):
            maxC=row["Price6"]
        elif row["Price9"] is not None and not math.isnan (row['Price5']):
            maxC=row["Price5"]
        elif row["Price9"] is not None and not math.isnan (row['Price4']):
            maxC=row["Price4"]
        elif row["Price9"] is not None and not math.isnan (row['Price3']):
            maxC=row["Price3"]
        elif row["Price2"] is not None and not math.isnan (row['Price2']):
            maxC=row["Price2"]
        elif row["Price1"] is not None and not math.isnan (row['Price1']):
            maxC=row["Price1"]
        elif row["Price0"] is not None and not math.isnan (row['Price0']):
            maxC=row["Price0"]
            
        Eco=0
        if row['RequiredValue'] is not None and not math.isnan (row['RequiredValue']):
            bound=row['RequiredValue']
        # else:
        #     print( "RequiredValue Value is null")
        if row["Power0"] is not None and not math.isnan (row['Power0']):
            bound=bound+row["Power0"]
        # else:
        #     print("power0 is null")
        if bound is not None and not math.isnan (bound):    
            if row["Power0"] is not None and not math.isnan (row['Power0']) and bound>=row["Power0"] :
                Eco=row["Power0"]
            if row["Power1"] is not None and not math.isnan (row['Power1']) and bound>=row["Power1"] :
                Eco=row["Power1"]
            if row["Power2"] is not None and not math.isnan (row['Power2']) and bound>=row["Power2"] :
                Eco=row["Power2"]
            if row["Power3"] is not None and not math.isnan (row['Power3']) and bound>=row["Power3"] :
                Eco=row["Power3"]
            if row["Power4"] is not None and not math.isnan (row['Power4']) and bound>=row["Power4"] :
                Eco=row["Power4"]
            if row["Power5"] is not None and not math.isnan (row['Power5']) and bound>=row["Power5"] :
                Eco=row["Power5"]
            if row["Power6"] is not None and not math.isnan (row['Power6']) and bound>=row["Power6"] :
                Eco=row["Power6"]
            if row["Power7"] is not None and not math.isnan (row['Power7']) and bound>=row["Power7"] :
                Eco=row["Power7"]
            if row["Power8"] is not None and not math.isnan (row['Power8']) and bound>=row["Power8"] :
                Eco=row["Power8"]
            if row["Power9"] is not None and not math.isnan (row['Power9']) and bound>=row["Power9"] :
                Eco=row["Power9"]
            if row["Power10"] is not None and not math.isnan (row['Power10']) and bound>=row["Power10"] :
                Eco=row["Power10"]
        if (maxC!=0):
        #     print(row['Date']+ " "+ str(row['HourNo']) +" is Zero")    
        # else:
            dfApp.loc[iIndex]=[row['Date'], row['HourNo'], maxC, Eco, float(Eco/maxC)] 
        iIndex=iIndex+1
        dfPlot=dfPlot.append(dfApp)
            
    ## remove dublicate        
    pdate='1300/01/01'
    phur=1
    idx=0
    dftemp=pd.DataFrame(columns=["Date", "Hour", "Max", "Eco","WEP"])
    for index, row in dfPlot.iterrows():
        temp=pd.DataFrame(columns=["Date", "Hour", "Max", "Eco","WEP"])
        if (pdate== row['Date'] and phur!=row['Hour']) or (pdate!=row['Date'] and phur==row['Hour']) or (pdate!=row['Date'] and phur!=row['Hour']) :
            temp.loc[idx]=[row['Date'],row['Hour'],row["Max"], row["Eco"],row['WEP']]
            idx=idx+1
            dftemp=dftemp.append(temp)
            pdate=row['Date']
            phur=row['Hour']
    #print(dftemp.shape)
    writer = pd.ExcelWriter(path1+'/outputWEP_'+powplnt +'_'+uncode+'.xlsx')
    dftemp.to_excel(writer,'Sheet1')
    writer.save()
    data = ET.Element('root')  
    for index,row in dftemp.iterrows():
        records = ET.SubElement(data, 'records')
        iPw=ET.SubElement(records, 'PowerPlant') 
        iU=ET.SubElement(records, 'UnitCode')  
        iDate = ET.SubElement(records, 'Date') 
        ihour = ET.SubElement(records, 'Hour')
        iWEP =  ET.SubElement(records, 'WEP')
        iPw.text =powplnt
        iU.text  =uncode
        iDate.text = row["Date"]  
        ihour.text = str(row["Hour"])
        iWEP.text= "{:.5f}".format(dftemp.WEP[index]) #str(row["WEP"])
    mydata = ET.tostring(data)  
    myfile = open("items2.xml", "wb")  
    myfile.write(mydata)  
    myfile.close()
    f=plt.figure()
    #x=list(range(1, dfPlot.shape[0]+1))
    plt.plot(dftemp.WEP)#, linestyle='',marker='.')
    plt.ylabel('MaxOff/MaxACC')
    plt.title('MAXOff/MaxACC for '+ uncode + ' of ' + 'PowerPlant with code= '+ powplnt + ' Date:'+ dftemp.iloc[0].Date+' to ' + dftemp.iloc[dftemp.shape[0]-1].Date)
    #f.show()
    plt.savefig(path1+'/OriginalData_'+powplnt +'_'+uncode+'.pdf', format='pdf', dpi=1200)
    
    xx=sum(x != 0 for x in dfPlot.WEP)
    if xx==0:
        text_file = open(path1+'/Output.txt', "w")
        text_file.write('Error:  all values in the time series are Zero')
        text_file.close()
        # import subprocess as sp
        # programName = "notepad.exe"
        # fileName = "Output.txt"
        # sp.Popen([programName, fileName])
        
    if xx!=0:
        from Outlier1 import plotOutLier 
        plotOutLier(dftemp.WEP,powplnt +'_'+uncode,path1)
        ts=dfPlot.WEP
        if (logF==1):
            tss=np.where(ts == 0, 0.00001,ts)
            ts=pd.Series(tss)
            ts_log = np.log(ts)
        else:
            ts_log=ts
        if (movingF!=0):
            moving_avg = pd.rolling_mean(ts_log,24)
            ts_log_moving_avg_diff = ts_log - moving_avg
            ts_log_moving_avg_diff.dropna(inplace=True)
            ts_log=ts_log_moving_avg_diff
        if (diff!=0):
            ts_log_diff = ts_log - ts_log.shift()
            ts_log_diff.dropna(inplace=True)
        else:
            ts_log_diff=ts_log
        if (stationaryF ==1):
            name_file='Data_'+str(powplnt)+'_'+str(uncode)+'.txt'
            text_file = open(name_file, "w")
            text_file.write("%s" % ts_log_diff)
            text_file.close()
            from test_stationarity import test_stationarity
            test_stationarity(ts_log_diff, 'Data', 24, powplnt +'_'+uncode,path1)
        if(parF==1):
            from statsmodels.tsa.stattools import acf, pacf
            ts_log_diff.dropna(inplace=True)
            lag_acf = acf(ts_log_diff, nlags=24)
            lag_pacf = pacf(ts_log_diff, nlags=24, method='ols')
            m=plt.figure()
            #Plot ACF: 
            plt.subplot(121) 
            plt.plot(lag_acf)
            plt.axhline(y=0,linestyle='--',color='gray')
            plt.axhline(y=-1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.axhline(y=1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.title('Autocorrelation Function')
            #Plot PACF:
            plt.subplot(122)
            plt.plot(lag_pacf)
            plt.axhline(y=0,linestyle='--',color='gray')
            plt.axhline(y=-1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.axhline(y=1.96/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
            plt.title('Partial Autocorrelation Function')
            plt.tight_layout()
            #m.show()
            plt.savefig(path1+'/Autocorrelation_'+powplnt+'_'+uncode+'.pdf', format='pdf', dpi=1300)
        if (resuF==1):
            from seasonality import test_seasonality
            test_seasonality(ts_log_diff, 24, powplnt +'_'+uncode,path1)
        if(powSpec==1):
            x=ts_log_diff- ts_log_diff.mean() #data.WEP - data.WEP.mean()
            ps = np.abs(np.fft.fft(x))**2
            time_step = 1/24
            freqs = np.fft.fftfreq(ts_log_diff.size, time_step)
            idx   = np.argsort(freqs)
            gp1=plt.figure()
            plt.plot(freqs[idx], ps[idx])
            plt.title('Power Spectrum analysis')
            # gp1.show()
            plt.savefig(path1+'/PowerSpectrum_'+powplnt+'_'+uncode+'.pdf', format='pdf', dpi=1300)
