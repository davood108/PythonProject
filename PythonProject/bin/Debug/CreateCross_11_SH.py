import os
import warnings
from sklearn.preprocessing import Imputer
from varselclass import varsel
import matplotlib.dates as dates
from datetime import datetime
import pyodbc
import pandas as pd
import varconn1
import matplotlib.pyplot as plt 
import numpy as np
import xml.etree.ElementTree as etree
import xml.etree.cElementTree as ET
import math

def Cross_11_SH(OurPow,OurUnit, path, firstdate,lastdate,table1,vclass, TotalFlag,dftemp,df_Final, plt):
    print(OurUnit)
    path1=path+'/'+str(OurPow)+'_'+str(OurUnit)
    try:
            if not os.path.exists(path1):
                os.makedirs(path1)
    except OSError:
            print ('Error: Creating directory. ' +  path) 
    #### cal Our Unit
    dfOur=pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","Unit","Power"])  
    qst="Select A.[UnitCode],A.[Date],A.[HourNo],A.[PowerPlantCode], A.[Power0], A.[Price0], A.[Power1],A.[Price1],A.[Power2],A.[Price2],A.[Power3],A.[Price3], A.[Power4],A.[Price4],A.[Power5],A.[Price5],A.[Power6],A.[Price6],A.[Power7],A.[Price7], A.[Power8],A.[Price8],A.[Power9],A.[Price9],A.[Power10],A.[Price10], A.BilatValue, A.DeclaredCapacity, A.BourseValue, A.NetValue FROM "+ table1+" as A WHERE A.PowerPlantCode=\'" +OurPow+"\'AND A.UnitCode=\'"+OurUnit+"\'and A.Date between \'"+ firstdate+"\'and \'"+lastdate+"\' ORDER BY A.[Date],A.[HourNo]"
    var2=vclass.HourlyUnitCommitment__BV(qst)
    var22=pd.DataFrame(columns=var2.columns) 
    pHub= np.mean(var2.DeclaredCapacity)
    for index, row in var2.iterrows():
        if row['DeclaredCapacity'] is None or math.isnan (row['DeclaredCapacity']):
            if (pHub>0):
                row['DeclaredCapacity']=pHub
                var22=var22.append(row)
            else:
                row['DeclaredCapacity']=np.mean(var2.DeclaredCapacity)
                var22=var22.append(row)
        else:
            pHub=row['DeclaredCapacity']
            var22=var22.append(row) 
    #print("Set DeclaredCapacity")  
    iIndex=0
    ###New Unit
    dfPlot=pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","Unit","Power"])  
    for index, row in var22.iterrows():
        WEP=0 
        den=0
        flag=0
        dfApp = pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","Unit","Power"])
        if (row['DeclaredCapacity'] > 0):
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) : 
                WEP=WEP+row['Power0']*row['Price0']
                den=row['Power0']
                if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                    if float(row['DeclaredCapacity'])< row['Power1'] or (row['Power2'] is None or  math.isnan(row['Power2'])):
                        WEP=WEP+row['Price1'] *(float(row['DeclaredCapacity'])- row['Power0'])
                        flag=1
                    else:
                        WEP=WEP+row['Price1'] *(row['Power1']- row['Power0'])
                        den=row['Power1']
                    if  flag!=1 and row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                        if float(row['DeclaredCapacity'])< row['Power2'] or (row['Power3'] is None or  math.isnan(row['Power3'])):
                            WEP=WEP+row['Price2'] *(float(row['DeclaredCapacity'])- row['Power1'])
                            flag=1
                        else :
                            WEP=WEP+row['Price2'] *(row['Power2']- row['Power1'])
                            den=row['Power2']
                        if  flag!=1 and row['Power3'] is not None and not math.isnan (row['Power3']) : #
                            if float(row['DeclaredCapacity'])< row['Power3'] or (row['Power4'] is None or math.isnan(row['Power4'])):
                                WEP=WEP+row['Price3'] *(float(row['DeclaredCapacity'])- row['Power2'])
                                flag=1
                            else:
                                WEP=WEP+row['Price3'] *(row['Power3']- row['Power2'])
                                den=row['Power3']
                            if  flag!=1 and row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                                if float(row['DeclaredCapacity'])< row['Power4'] or (row['Power5'] is None or  math.isnan(row['Power5'])):
                                    WEP=WEP+row['Price4'] *(float(row['DeclaredCapacity'])- row['Power3'])
                                    flag=1
                                else:
                                    WEP=WEP+row['Price4'] *(row['Power4']- row['Power3'])
                                    den=row['Power4']
    
                                if  flag!=1 and row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                    if float(row['DeclaredCapacity'])< row['Power5'] or (row['Power6'] is None or  math.isnan(row['Power6'])):
                                        WEP=WEP+row['Price5'] *(float(row['DeclaredCapacity'])- row['Power4'])
                                        flag=1
                                    else:
                                        WEP=WEP+row['Price5'] *(row['Power5']- row['Power4'])
                                        den=row['Power5']
                                    if  flag!=1 and row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                        if float(row['DeclaredCapacity'])< row['Power6'] or (row['Power7'] is None or  math.isnan(row['Power7'])):
                                            WEP=WEP+row['Price6'] *(float(row['DeclaredCapacity'])- row['Power5'])
                                            flag=1
                                        else:#
                                            WEP=WEP+row['Price6'] *(row['Power6']- row['Power5'])
                                            den=row['Power6']
                                        if  flag!=1 and row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                            if float(row['DeclaredCapacity'])< row['Power7'] or (row['Power8'] is None or  math.isnan(row['Power8'])):
                                                WEP=WEP+row['Price7'] *(float(row['DeclaredCapacity'])- row['Power6'])
                                                flag=1
                                            else:#
                                                WEP=WEP+row['Price7'] *(row['Power7']- row['Power6'])
                                                den=row['Power7']
                                            if  flag!=1 and row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                                if float(row['DeclaredCapacity'])< row['Power8'] or (row['Power9'] is None or  math.isnan(row['Power9'])):
                                                    WEP=WEP+row['Price8'] *(float(row['DeclaredCapacity'])- row['Power7'])
                                                    flag=1
                                                else:#
                                                    WEP=WEP+row['Price8'] *(row['Power8']- row['Power7'])
                                                    den=row['Power8']
                                                if  flag!=1 and row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                    if float(row['DeclaredCapacity'])< row['Power9'] or (row['Power10'] is None or  math.isnan(row['Power10'])):
                                                        WEP=WEP+row['Price9'] *(float(row['DeclaredCapacity'])- row['Power8'])
                                                        flag=1
                                                    else:#
                                                        WEP=WEP+row['Price9'] *(row['Power9']- row['Power8'])
                                                        den=row['Power9']
                                                    
                                                    if  flag!=1 and row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                        WEP=WEP+row['Price10'] *(float(row['DeclaredCapacity'])- row['Power9'])
                                                        den=row['Power10']
            if (row['DeclaredCapacity'] is not None and  not math.isnan(row['DeclaredCapacity'])):  
                den= float(row['DeclaredCapacity'])
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) :
                en=den-row['Power0']    
            if (den!=0 and not math.isnan(float(WEP/den))):
                dfApp.loc[iIndex]=[row['Date'], row['HourNo'], float(WEP/den), den, OurUnit,OurPow] 
                iIndex=iIndex+1
                dfOur=dfOur.append(dfApp)
            else : 
                if math.isnan(WEP):
                    print (" EEEEEEERRRRRRRoR")   
                print("ELSE "+ row['Date']+ " "+ str(row['HourNo'])+" "+ str(den))
                                                        
        else:
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) :
                WEP=WEP+row['Power0']*row['Price0']
                den=row['Power0']
                if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                    WEP=WEP+row['Price1'] *(row['Power1']- row['Power0'])
                    den=row['Power1']
                    if  row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                        WEP=WEP+row['Price2'] *(row['Power2']- row['Power1'])
                        den=row['Power2']
                        if  row['Power3'] is not None and not math.isnan (row['Power3']) : #
                            WEP=WEP+row['Price3'] *(row['Power3']- row['Power2'])
                            den=row['Power3']
                            if  row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                                WEP=WEP+row['Price4'] *(row['Power4']- row['Power3'])
                                den=row['Power4']
                                if  row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                    WEP=WEP+row['Price5'] *(row['Power5']- row['Power4'])
                                    den=row['Power5']
                                    if  row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                        WEP=WEP+row['Price6'] *(row['Power6']- row['Power5'])
                                        den=row['Power6']
                                        if  row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                            WEP=WEP+row['Price7'] *(row['Power7']- row['Power6'])
                                            den=row['Power7']
                                            if  row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                                WEP=WEP+row['Price8'] *(row['Power8']- row['Power7'])
                                                den=row['Power8']
                                                if  row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                    WEP=WEP+row['Price9'] *(row['Power9']- row['Power8'])
                                                    den=row['Power9']
                                                    if  row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                        WEP=WEP+row['Price10'] *(row['Power10']- row['Power9'])
                                                        den=row['Power10']
            if (den!=0 and not math.isnan(float(WEP/den))):
                dfApp.loc[iIndex]=[row['Date'], row['HourNo'], WEP/den, den, OurUnit,OurPow] 
                iIndex=iIndex+1
                dfOur=dfOur.append(dfApp)
    Our=pd.DataFrame(columns=dfOur.columns) 
    idx=0
    pdate='1300/01/01'
    phur=1
    for index, row in dfOur.iterrows():
        temp=pd.DataFrame(columns=dfOur.columns)
        if (pdate== row['Date'] and phur!=row['Hour']) or (pdate!=row['Date'] and phur==row['Hour']) or (pdate!=row['Date'] and phur!=row['Hour']) :
            temp.loc[idx]=[row["Date"], row["Hour"],row["WEP"], row["PDay"], row["Unit"], row["Power"]]
            idx=idx+1
            Our=Our.append(temp)
            pdate=row['Date']
            phur=row['Hour'] 
    writer = pd.ExcelWriter(path1+'/outputWEP_Our.xlsx')
    dfOur.to_excel(writer,'Sheet1')
    writer.save()
    #print(dfOur.shape)
    #----------------------------
    #print('before flag=' +str(TotalFlag))  
    if (TotalFlag==0):
        TotalFlag=1
        qst="SELECT distinct [UnitCode], [PowerPlantCode] FROM "+ table1
        uncode=vclass.HourlyUnitCommitment__BV(qst)
        idx=0
        for index, row in uncode.iterrows():#for c in un:
            unT=row['UnitCode']
            powT=row['PowerPlantCode']
            qst="Select A.[UnitCode],A.[Date],A.[HourNo],A.[PowerPlantCode], A.[Power0], A.[Price0], A.[Power1],A.[Price1],A.[Power2],A.[Price2],A.[Power3],A.[Price3], A.[Power4],A.[Price4],A.[Power5],A.[Price5],A.[Power6],A.[Price6],A.[Power7],A.[Price7], A.[Power8],A.[Price8],A.[Power9],A.[Price9],A.[Power10],A.[Price10], A.BilatValue, A.DeclaredCapacity, A.BourseValue, A.NetValue FROM "+ table1+" as A WHERE A.PowerPlantCode=\'" +str(row['PowerPlantCode'])+"\'AND A.UnitCode=\'"+str(row['UnitCode'])+"\'and A.Date between \'"+ firstdate+"\'and \'"+lastdate+"\' ORDER BY A.[Date],A.[HourNo]"
            var2=vclass.HourlyUnitCommitment__BV(qst)
            var22=pd.DataFrame(columns=var2.columns) 
            pHub= np.mean(var2.DeclaredCapacity)
            for index, row in var2.iterrows():
                if row['DeclaredCapacity'] is None or math.isnan (row['DeclaredCapacity']):
                    if (pHub>0):
                        row['DeclaredCapacity']=pHub
                        var22=var22.append(row)
                    else:
                        row['DeclaredCapacity']=np.mean(var2.DeclaredCapacity)
                        var22=var22.append(row)
                else:
                    pHub=row['DeclaredCapacity']
                    var22=var22.append(row) 
            iIndex=0
            ###New Unit
            dfPlot=pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","Unit","Power"])  
            for index, row in var22.iterrows():
                WEP=0 
                den=0
                flag=0
                dfApp = pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","Unit","Power"])
                if (row['DeclaredCapacity'] > 0):
                    if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) : 
                        WEP=WEP+row['Power0']*row['Price0']
                        den=row['Power0']
                        if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                            if float(row['DeclaredCapacity'])< row['Power1'] or (row['Power2'] is None or  math.isnan(row['Power2'])):
                                WEP=WEP+row['Price1'] *(float(row['DeclaredCapacity'])- row['Power0'])
                                flag=1
                            else:
                                WEP=WEP+row['Price1'] *(row['Power1']- row['Power0'])
                                den=row['Power1']
                            if  flag!=1 and row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                                if float(row['DeclaredCapacity'])< row['Power2'] or (row['Power3'] is None or  math.isnan(row['Power3'])):
                                    WEP=WEP+row['Price2'] *(float(row['DeclaredCapacity'])- row['Power1'])
                                    flag=1
                                else :
                                    WEP=WEP+row['Price2'] *(row['Power2']- row['Power1'])
                                    den=row['Power2']
                                if  flag!=1 and row['Power3'] is not None and not math.isnan (row['Power3']) : #
                                    if float(row['DeclaredCapacity'])< row['Power3'] or (row['Power4'] is None or math.isnan(row['Power4'])):
                                        WEP=WEP+row['Price3'] *(float(row['DeclaredCapacity'])- row['Power2'])
                                        flag=1
                                    else:
                                        WEP=WEP+row['Price3'] *(row['Power3']- row['Power2'])
                                        den=row['Power3']
                                    if  flag!=1 and row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                                        if float(row['DeclaredCapacity'])< row['Power4'] or (row['Power5'] is None or  math.isnan(row['Power5'])):
                                            WEP=WEP+row['Price4'] *(float(row['DeclaredCapacity'])- row['Power3'])
                                            flag=1
                                        else:
                                            WEP=WEP+row['Price4'] *(row['Power4']- row['Power3'])
                                            den=row['Power4']
            
                                        if  flag!=1 and row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                            if float(row['DeclaredCapacity'])< row['Power5'] or (row['Power6'] is None or  math.isnan(row['Power6'])):
                                                WEP=WEP+row['Price5'] *(float(row['DeclaredCapacity'])- row['Power4'])
                                                flag=1
                                            else:
                                                WEP=WEP+row['Price5'] *(row['Power5']- row['Power4'])
                                                den=row['Power5']
                                            if  flag!=1 and row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                                if float(row['DeclaredCapacity'])< row['Power6'] or (row['Power7'] is None or  math.isnan(row['Power7'])):
                                                    WEP=WEP+row['Price6'] *(float(row['DeclaredCapacity'])- row['Power5'])
                                                    flag=1
                                                else:#
                                                    WEP=WEP+row['Price6'] *(row['Power6']- row['Power5'])
                                                    den=row['Power6']
                                                if  flag!=1 and row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                                    if float(row['DeclaredCapacity'])< row['Power7'] or (row['Power8'] is None or  math.isnan(row['Power8'])):
                                                        WEP=WEP+row['Price7'] *(float(row['DeclaredCapacity'])- row['Power6'])
                                                        flag=1
                                                    else:#
                                                        WEP=WEP+row['Price7'] *(row['Power7']- row['Power6'])
                                                        den=row['Power7']
                                                    if  flag!=1 and row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                                        if float(row['DeclaredCapacity'])< row['Power8'] or (row['Power9'] is None or  math.isnan(row['Power9'])):
                                                            WEP=WEP+row['Price8'] *(float(row['DeclaredCapacity'])- row['Power7'])
                                                            flag=1
                                                        else:#
                                                            WEP=WEP+row['Price8'] *(row['Power8']- row['Power7'])
                                                            den=row['Power8']
                                                        if  flag!=1 and row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                            if float(row['DeclaredCapacity'])< row['Power9'] or (row['Power10'] is None or  math.isnan(row['Power10'])):
                                                                WEP=WEP+row['Price9'] *(float(row['DeclaredCapacity'])- row['Power8'])
                                                                flag=1
                                                            else:#
                                                                WEP=WEP+row['Price9'] *(row['Power9']- row['Power8'])
                                                                den=row['Power9']
                                                            
                                                            if  flag!=1 and row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                                WEP=WEP+row['Price10'] *(float(row['DeclaredCapacity'])- row['Power9'])
                                                                den=row['Power10']
                    if (row['DeclaredCapacity'] is not None and  not math.isnan(row['DeclaredCapacity'])):  
                        den= float(row['DeclaredCapacity'])
                    if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) :
                        en=den-row['Power0']    
                    if (den!=0 and not math.isnan(float(WEP/den))):
                        dfApp.loc[iIndex]=[row['Date'], row['HourNo'], float(WEP/den), den, unT, powT] 
                        iIndex=iIndex+1
                        dfPlot=dfPlot.append(dfApp)
                    else : 
                        if math.isnan(WEP):
                            print (" EEEEEEERRRRRRRoR")   
                        print("ELSE "+ row['Date']+ " "+ str(row['HourNo'])+" "+ str(den))
                                                                
                else:
                    if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) :
                        WEP=WEP+row['Power0']*row['Price0']
                        den=row['Power0']
                        if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                            WEP=WEP+row['Price1'] *(row['Power1']- row['Power0'])
                            den=row['Power1']
                            if  row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                                WEP=WEP+row['Price2'] *(row['Power2']- row['Power1'])
                                den=row['Power2']
                                if  row['Power3'] is not None and not math.isnan (row['Power3']) : #
                                    WEP=WEP+row['Price3'] *(row['Power3']- row['Power2'])
                                    den=row['Power3']
                                    if  row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                                        WEP=WEP+row['Price4'] *(row['Power4']- row['Power3'])
                                        den=row['Power4']
                                        if  row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                            WEP=WEP+row['Price5'] *(row['Power5']- row['Power4'])
                                            den=row['Power5']
                                            if  row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                                WEP=WEP+row['Price6'] *(row['Power6']- row['Power5'])
                                                den=row['Power6']
                                                if  row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                                    WEP=WEP+row['Price7'] *(row['Power7']- row['Power6'])
                                                    den=row['Power7']
                                                    if  row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                                        WEP=WEP+row['Price8'] *(row['Power8']- row['Power7'])
                                                        den=row['Power8']
                                                        if  row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                            WEP=WEP+row['Price9'] *(row['Power9']- row['Power8'])
                                                            den=row['Power9']
                                                            if  row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                                WEP=WEP+row['Price10'] *(row['Power10']- row['Power9'])
                                                                den=row['Power10']
                    if (den!=0 and not math.isnan(float(WEP/den))):
                        dfApp.loc[iIndex]=[row['Date'], row['HourNo'], WEP/den, den, unT,powT] 
                        iIndex=iIndex+1
                        dfPlot=dfPlot.append(dfApp)
            writer = pd.ExcelWriter(path1+'/outputWEP'+powT+'_'+unT+'.xlsx')
            dfPlot.to_excel(writer,'Sheet1')
            writer.save()
            pdate='1300/01/01'
            phur=1
            for index, row in dfPlot.iterrows():
                temp=pd.DataFrame(columns=dfPlot.columns)
                if (pdate== row['Date'] and phur!=row['Hour']) or (pdate!=row['Date'] and phur==row['Hour']) or (pdate!=row['Date'] and phur!=row['Hour']) :
                    temp.loc[idx]=[row["Date"], row["Hour"],row["WEP"], row["PDay"], row["Unit"],row["Power"]]
                    idx=idx+1
                    dftemp=dftemp.append(temp)
                    pdate=row['Date']
                    phur=row['Hour'] 
        base1=uncode['UnitCode'][0]
        base2=uncode['PowerPlantCode'][0]
        df_0=dftemp[(dftemp['Unit'] ==base1 ) & (dftemp['Power'] ==base2) ]
        df_Final=pd.DataFrame(columns=["Date","Hour","WEP", "WEPT","PDayT"])
        i_P=0
        for index, row in df_0.iterrows():
            df_Hour=dftemp[(dftemp['Hour'] == row["Hour"]) &  (dftemp['Date'] == row['Date'])]
            WAP_P=0
            for index, row_h in df_Hour.iterrows():
                WAP_P=WAP_P+row_h['WEP'] *row_h['PDay']
            PDay_P=np.sum(df_Hour.PDay)
            temp=pd.DataFrame(columns=["Date","Hour","WEP","WEPT","PDayT"])
            temp.loc[i_P]=[row['Date'],row['Hour'], WAP_P/PDay_P, WAP_P, PDay_P]
            i_P=i_P+1
            df_Final=df_Final.append(temp)
    writer = pd.ExcelWriter(path1+'/outputToTal.xlsx')
    df_Final.to_excel(writer,'Sheet1')
    writer.save()
    print('Total PowerPlants were calculated')    
    #print('After Flag='+ str(TotalFlag))    
    if (TotalFlag!=0):    
        
        f=plt.figure()
        plt.plot(df_Final.WEP)
        plt.ylabel('OFFWAP')
        plt.title('OffWAP for Total PowerPlants')
        #f.show()
        plt.savefig(path1+'/OriginalData_ToTal'+'.pdf', format='pdf', dpi=2000)
        g=plt.figure()
        plt.plot(dfOur.WEP)#, linestyle='',marker='.')
        plt.ylabel('OFFWAP ')
        plt.title('OffWAP for PowerPlant with code= '+ OurPow + ' , Unit code= '+ OurUnit+' Date:'+firstdate+' to ' + lastdate)
        #g.show()
        plt.savefig(path1+'/OriginalData_'+OurPow +'_'+OurUnit+'.pdf', format='pdf', dpi=2000)
        npts = dfOur.shape[0]
        x = np.arange(0, dfOur.shape[0])
        y1 = df_Final.WEP
        y2 = dfOur.WEP
        lags = np.arange(-npts + 1, npts)
        temp1=(y1 - y1.mean())/y1.std()
        temp2=(y2 - y2.mean())/y2.std()
        
        import matplotlib.pyplot as plt
        wg=plt.figure()
        #plt.xcorr(y1,y2,maxlags=24)
        coeff = plt.xcorr(y1,y2,maxlags=None, usevlines=True, normed=True,lw=2)
        #wg.show()
        plt.savefig(path1+'/Coef_Cor_'+OurPow +'_'+OurUnit+'.pdf', format='pdf', dpi=2000)
        
        #y1 - y1.mean(),y2 - y2.mean()
        x=np.arange((-1*dfOur.shape[0])+1, dfOur.shape[0],1)
        ccov = np.correlate(y1-y1.mean(),y2-y2.mean() , mode='full')
        ccor = ccov / (npts * y1.std() * y2.std())  #
        ww=plt.figure()
        plt.plot(x,ccor)
        #ww.show()
        plt.savefig(path1+'/Cross_Cor_'+OurPow +'_'+OurUnit+'.pdf', format='pdf', dpi=2000)
    return df_Final
           
