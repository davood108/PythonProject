import warnings
from sklearn.preprocessing import Imputer
import matplotlib.dates as dates
from datetime import datetime
import pyodbc
import pandas as pd
import varconn1
import matplotlib.pyplot as plt 
import numpy as np
import math
from varselclass import varsel
import xml.etree.ElementTree as etree
import xml.etree.cElementTree as ET
import os
def Plant_13A_SH(powplnt, uncode, path, firstdate, lastdate, table1, table2,vclass):
    print(uncode)
    path1=path+'/'+str(powplnt)+'_'+str(uncode)
    try:
            if not os.path.exists(path1):
                os.makedirs(path1)
    except OSError:
            print ('Error: Creating directory. ' +  path)
    idx=0
    dftemp=pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","NBi"])
    qst="Select A.[UnitCode],A.[Date],A.[HourNo],A.[PowerPlantCode], A.[Power0], A.[Price0], A.[Power1],A.[Price1],A.[Power2],A.[Price2],A.[Power3],A.[Price3], A.[Power4],A.[Price4],A.[Power5],A.[Price5],A.[Power6],A.[Price6],A.[Power7],A.[Price7], A.[Power8],A.[Price8],A.[Power9],A.[Price9],A.[Power10],A.[Price10], A.BilatValue, A.DeclaredCapacity, A.BourseValue, A.NetValue FROM "+ table1+" as A WHERE A.PowerPlantCode=\'" +str(powplnt)+"\'AND A.UnitCode=\'"+uncode+"\'and A.Date between \'"+ firstdate+"\'and \'"+lastdate+"\' ORDER BY A.[Date],A.[HourNo]"
    var2=vclass.HourlyUnitCommitment__BV(qst)
    #misiing DeclaredCapacity
    var22=pd.DataFrame(columns=var2.columns) 
    pHub= np.mean(var2.DeclaredCapacity)
    for index, row in var2.iterrows():
        if row['DeclaredCapacity'] is None or math.isnan (row['DeclaredCapacity']):
            if (pHub>0):
                row['DeclaredCapacity']=pHub
                var22=var22.append(row)
            else:
                row['DeclaredCapacity']=np.mean(var2.DeclaredCapacity)
                var22=var22.append(row)
        else:
            pHub=row['DeclaredCapacity']
            var22=var22.append(row) 
    iIndex=0
    ###New Unit
    dfPlot=pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","NBi"])  
    for index, row in var22.iterrows():
        WEP=0
        den=0
        dfApp = pd.DataFrame(columns=["Date", "Hour", "WEP", "PDay","NBi"])
        flag=0
        if (row['DeclaredCapacity'] > 0):
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) : 
                WEP=WEP+row['Power0']*row['Price0']
                nb=0
                if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                    if float(row['DeclaredCapacity'])< row['Power1'] or (row['Power2'] is None or  math.isnan(row['Power2'])):
                        WEP=WEP+row['Price1'] *(float(row['DeclaredCapacity'])- row['Power0'])
                        flag=1
                    else:
                        WEP=WEP+row['Price1'] *(row['Power1']- row['Power0'])
                    nb=1
                    if  flag!=1 and row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                        if float(row['DeclaredCapacity'])< row['Power2'] or (row['Power3'] is None or  math.isnan(row['Power3'])):
                            WEP=WEP+row['Price2'] *(float(row['DeclaredCapacity'])- row['Power1'])
                            flag=1
                        else :
                            WEP=WEP+row['Price2'] *(row['Power2']- row['Power1'])
                        nb=2
                        if  flag!=1 and row['Power3'] is not None and not math.isnan (row['Power3']) : #
                            if float(row['DeclaredCapacity'])< row['Power3'] or (row['Power4'] is None or math.isnan(row['Power4'])):
                                WEP=WEP+row['Price3'] *(float(row['DeclaredCapacity'])- row['Power2'])
                                flag=1
                            else:
                                WEP=WEP+row['Price3'] *(row['Power3']- row['Power2'])
                            nb=3
                            if  flag!=1 and row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                                if float(row['DeclaredCapacity'])< row['Power4'] or (row['Power5'] is None or  math.isnan(row['Power5'])):
                                    WEP=WEP+row['Price4'] *(float(row['DeclaredCapacity'])- row['Power3'])
                                    flag=1
                                else:
                                    WEP=WEP+row['Price4'] *(row['Power4']- row['Power3'])
                                nb=4
                                if  flag!=1 and row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                    if float(row['DeclaredCapacity'])< row['Power5'] or (row['Power6'] is None or  math.isnan(row['Power6'])):
                                        WEP=WEP+row['Price5'] *(float(row['DeclaredCapacity'])- row['Power4'])
                                        flag=1
                                    else:
                                        WEP=WEP+row['Price5'] *(row['Power5']- row['Power4'])
                                    nb=5
                                    if  flag!=1 and row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                        if float(row['DeclaredCapacity'])< row['Power6'] or (row['Power7'] is None or  math.isnan(row['Power7'])):
                                            WEP=WEP+row['Price6'] *(float(row['DeclaredCapacity'])- row['Power5'])
                                            flag=1
                                        else:#
                                            WEP=WEP+row['Price6'] *(row['Power6']- row['Power5'])
                                        nb=6
                                        if  flag!=1 and row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                            if float(row['DeclaredCapacity'])< row['Power7'] or (row['Power8'] is None or  math.isnan(row['Power8'])):
                                                WEP=WEP+row['Price7'] *(float(row['DeclaredCapacity'])- row['Power6'])
                                                flag=1
                                            else:#
                                                WEP=WEP+row['Price7'] *(row['Power7']- row['Power6'])
                                            nb=7
                                            if  flag!=1 and row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                                if float(row['DeclaredCapacity'])< row['Power8'] or (row['Power9'] is None or  math.isnan(row['Power9'])):
                                                    WEP=WEP+row['Price8'] *(float(row['DeclaredCapacity'])- row['Power7'])
                                                    flag=1
                                                else:#
                                                    WEP=WEP+row['Price8'] *(row['Power8']- row['Power7'])
                                                nb=8
                                                if  flag!=1 and row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                    if float(row['DeclaredCapacity'])< row['Power9'] or (row['Power10'] is None or  math.isnan(row['Power10'])):
                                                        WEP=WEP+row['Price9'] *(float(row['DeclaredCapacity'])- row['Power8'])
                                                        flag=1
                                                    else:#
                                                        WEP=WEP+row['Price9'] *(row['Power9']- row['Power8'])
                                                    nb=9
                                                    if  flag!=1 and row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                        WEP=WEP+row['Price10'] *(float(row['DeclaredCapacity'])- row['Power9'])
                                                        nb=10
            if (row['DeclaredCapacity'] is not None and  not math.isnan(row['DeclaredCapacity'])): 
                den= float(row['DeclaredCapacity'])
                # print(den)
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) :
                den=den-row['Power0']
            if (den!=0 and not math.isnan(float(WEP/den)) ):
                dfApp.loc[iIndex]=[row['Date'], row['HourNo'], WEP/den, den, nb]
                iIndex=iIndex+1
                dfPlot=dfPlot.append(dfApp)
        else:
            if not math.isnan(row['Power0']) and not math.isnan (row['Power0']) : #row['Power0'] is not None: #
                WEP=WEP+row['Power0']*row['Price0']
                den=row['Power0']
                nb=0
                if  row['Power1'] is not None and not math.isnan (row['Power1']): #
                    WEP=WEP+row['Price1'] *(row['Power1']- row['Power0'])
                    den=row['Power1']
                    nb=1
                    if  row['Power2'] is not None and  not math.isnan(row['Power2']) : #
                        WEP=WEP+row['Price2'] *(row['Power2']- row['Power1'])
                        den=row['Power2']
                        nb=2
                        if  row['Power3'] is not None and not math.isnan (row['Power3']) : #
                            WEP=WEP+row['Price3'] *(row['Power3']- row['Power2'])
                            den=row['Power3']
                            nb=3
                            if  row['Power4'] is not None and  not math.isnan (row['Power4']) : #
                                WEP=WEP+row['Price4'] *(row['Power4']- row['Power3'])
                                den=row['Power4']
                                nb=4
                                if  row['Power5'] is not None and not math.isnan (row['Power5']) : #
                                    WEP=WEP+row['Price5'] *(row['Power5']- row['Power4'])
                                    den=row['Power5']
                                    nb=5
                                    if  row['Power6'] is not None and  not math.isnan (row['Power6'])  : 
                                        WEP=WEP+row['Price6'] *(row['Power6']- row['Power5'])
                                        den=row['Power6']
                                        nb=6
                                        if  row['Power7'] is not None and  not math.isnan (row['Power7']): #
                                            WEP=WEP+row['Price7'] *(row['Power7']- row['Power6'])
                                            den=row['Power7']
                                            nb=7
                                            if  row['Power8'] is not None and  not math.isnan (row['Power8']) : #
                                                WEP=WEP+row['Price8'] *(row['Power8']- row['Power7'])
                                                den=row['Power8']
                                                nb=8
                                                if  row['Power9'] is not None and not math.isnan (row['Power9']): #
                                                    WEP=WEP+row['Price9'] *(row['Power9']- row['Power8'])
                                                    den=row['Power9']
                                                    nb=9
                                                    if  row['Power10'] is not None and  not math.isnan (row['Power10']): #
                                                        WEP=WEP+row['Price10'] *(row['Power10']- row['Power9'])
                                                        den=row['Power10']
                                                        nb=10
            if (den!=0 and not math.isnan(float(WEP/den)) ):
                dfApp.loc[iIndex]=[row['Date'], row['HourNo'], WEP/den, den, nb] 
                iIndex=iIndex+1
                dfPlot=dfPlot.append(dfApp)
    pdate='1300/01/01'
    phur=1
    writer = pd.ExcelWriter(path1+'/outputWEP_'+powplnt+'_'+uncode+'.xlsx')
    dfPlot.to_excel(writer,'Sheet1')
    writer.save()
    #Insert New Unit  into Dftemp
    for index, row in dfPlot.iterrows():
        temp=pd.DataFrame(columns=dfPlot.columns)
        if (pdate== row['Date'] and phur!=row['Hour']) or (pdate!=row['Date'] and phur==row['Hour']) or (pdate!=row['Date'] and phur!=row['Hour']) :
            temp.loc[idx]=[row["Date"], row["Hour"],row["WEP"], row["PDay"], row["NBi"]]
            idx=idx+1
            dftemp=dftemp.append(temp)
            pdate=row['Date']
            phur=row['Hour'] 
    ## remove dublicate        
    writer = pd.ExcelWriter(path1+'/outputWEP_'+powplnt+'_'+uncode+'.xlsx')
    dftemp.to_excel(writer,'Sheet1')
    writer.save()
    
    data = ET.Element('root')  
    for index,row in dftemp.iterrows():
        records = ET.SubElement(data, 'records')
        iPw=ET.SubElement(records, 'PowerPlant') 
        #iU=ET.SubElement(records, 'UnitCode')  
        iDate = ET.SubElement(records, 'Date') 
        ihour = ET.SubElement(records, 'Hour')
        iWEP =  ET.SubElement(records, 'WEP')
        iNb =  ET.SubElement(records, 'NBi')
        iPw.text =powplnt
        #iU.text  =uncode
        iDate.text = row["Date"]  
        ihour.text = str(row["Hour"])
        iWEP.text= "{:.5f}".format(dftemp.WEP[index]) #str(row["WEP"])
    mydata = ET.tostring(data)  
    myfile = open("items2.xml", "wb")  
    myfile.write(mydata)  
    myfile.close()
    
    npts = dftemp.shape[0]
    x = np.arange(0, dftemp.shape[0])
    y1 = dftemp.WEP
    y2 = dftemp.NBi
    lags = np.arange(-npts + 1, npts)
    temp1=(y1 - y1.mean())/y1.std()
    temp2=(y2 - y2.mean())/y2.std()
    import matplotlib.pyplot as plt
    wg=plt.figure()
    coeff = plt.xcorr(y1,y2,maxlags=None, usevlines=True, normed=True,lw=2)
    #wg.show()
    plt.savefig(path1+'/correlation_'+powplnt+'_'+uncode+'.pdf', format='pdf', dpi=1300)
    # text_file = open('outputWEP_'+powplnt+'_'+uncode+'.txt', "w")
    # for item in coeff:
    #     text_file.write("%s\n" % item)
    # text_file.close()
    # import subprocess as sp
    # programName = "notepad.exe"
    # fileName = 'outputWEP_'+powplnt+'_'+uncode+'.txt'
    # sp.Popen([programName, fileName])
    # y1 - y1.mean(),y2 - y2.mean()
    # x=np.arange((-1*dftemp.shape[0])+1, dftemp.shape[0],1)
    # ccov = np.correlate(y1-y1.mean(),y2-y2.mean() , mode='full')
    # if y1.std() and y2.std():
    #     ccor = ccov / (npts * y1.std() * y2.std())  #
    #     ww=plt.figure()
    #     plt.plot(x,ccor)
    #     ww.show()