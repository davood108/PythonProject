try:
    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)
    import numpy as np
    warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
    import pandas as pd
    import xml.etree.ElementTree as etree
    import xml.etree.cElementTree as ET
    while True:
        try:
            tree = etree.parse('items2.xml')
            break
        except OSError:
            print("Oops!  That was no valid Attribute in Table, Try again...")
    root = tree.getroot()
    findex=0
    dfPlot=pd.DataFrame(columns=["Date", "hour", "WEP"])
    for elem in root.findall('records'):
        powplnt = elem.find("PowerPlant").text
        #print(powplnt) 
        uncode=elem.find("UnitCode").text 
        #print(uncode)
        date=elem.find("Date").text 
        hour=elem.find("Hour").text
        WEP=float(elem.find("WEP").text)
        dfApp = pd.DataFrame(columns=["Date", "hour", "WEP"])
        dfApp.loc[findex]=[date, hour, WEP] 
        findex=findex+1
        dfPlot=dfPlot.append(dfApp)
    ts=dfPlot.WEP
    from Modeling import Modeling_ARIMA
    while True:
        try:
            tree = etree.parse('Model_p.xml')
            break
        except OSError:
            print("Oops!  That was no valid File Modeling, Try again...")
    root = tree.getroot()
    p = int(root.find("p_ARIMA").text)
    d=int(root.find("d_ARIMA").text) 
    q=int(root.find("q_ARIMA").text) 
    Modeling_ARIMA(ts,p,d,q)
except Exception as inst:
    print("Unexpected error:")
    print (type(inst))    # the exception instance     
    print (inst )          # __str__ allows args to be printed directly
    raise
