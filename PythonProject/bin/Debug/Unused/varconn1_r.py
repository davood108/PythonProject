#[RUN].[HourlyUnitCommitment__BV]
def varconn(powplnt,uncode,firstdate, lastdate,Hur, table,inp, Rev, lastRev,HasF):
    from varselclass import varsel
    import pandas as pd
    import varconn1
    import pyodbc
    vclass=varsel()
    if inp=="CostHot":
        qst="SELECT *FROM [dbo].[Cost__LV]"
        var22=vclass.vcosthot(qst)
        var2=var22.CostHot
        var2=pd.Series(var2)
        return var2
    else:
        qst="SELECT * FROM "+table+" WHERE PowerPlantCode=\'" +str(powplnt)+"\'AND UnitCode=\'"+str(uncode)+"\' AND Date between \'"+ firstdate+"\'and \'"+lastdate+"\' and Revision=\'"+Rev+"\' and [IsLastRevision]=\'"+lastRev+"\' and HasFuelLimit=\'"+HasF+"\' ORDER BY [Date],"+ Hur
        #print(qst)
        var=vclass.HourlyUnitCommitment__BV(qst)
        var22=vclass.HourlyUnitCommitment__BV(qst)
        var2=var22[inp]
        var2=pd.Series(var2)
        return var2
        