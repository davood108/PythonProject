from numpy import *
import math
import matplotlib.pyplot as plt

t = linspace(-1*math.pi, 1*math.pi, 400)
a = sin(t)
b = cos(t)
c = a + b


plt.plot(t, a, 'r') # plotting t, a separately 
plt.title('sin')
plt.show()

plt.plot(t, b, 'b') # plotting t, b separately 
plt.title('cos')
plt.show()



plt.plot(t, c, 'g') # plotting t, c separately 
plt.title('+')
plt.show()
