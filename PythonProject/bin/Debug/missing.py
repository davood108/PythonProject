try:
    import warnings
    warnings.simplefilter(action='ignore', category=FutureWarning)
    import numpy as np
    warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
    from sklearn.preprocessing import Imputer
    from varselclass import varsel
    import pyodbc
    import pandas as pd
    import varconn1
    import matplotlib.pyplot as plt 
    import numpy
    import xml.etree.ElementTree as etree
    import xml.etree.cElementTree as ET
    while True:
        try:
            tree = etree.parse('Input.xml')
            break
        except OSError:
            print("Oops!  That was no valid Attribute in Table, Try again...")
    root = tree.getroot()
    powplnt = root.find("PowerPlant").text
    uncode=root.find("UnitCode").text 
    firstdate=root.find("FirstDate").text 
    lastdate=root.find("LastDate").text
    Hur=root.find("Hour").text 
    table= root.find("Table").text 
    inp= root.find("Attribute1").text
    inp = inp[:-1]
    inp = inp[1:]
    Rev= root.find("Revision").text 
    lastRev= root.find("IsLastRevision").text 
    HasF=root.find("HasFuel").text 
    var=varconn1.varconn(powplnt,uncode,firstdate, lastdate,Hur, table, inp, Rev, lastRev,HasF)
    if (var.shape[0] == 0):
        print("Oops! There is no Records, Try again...")
        exit()
    a=var.isnull().sum()
    nans = lambda var: var[var.isnull().any(axis=0)]
    if a!=0:
        Imp=Imputer(missing_values='NaN', strategy='mean', axis=0, verbose=0, copy=True)
        imp.fit(train)
        varcopy= imp.transform(var)
    import subprocess as sp
    programName = "notepad.exe"
    fileName = path1+'/OutputMissing.txt'
    sp.Popen([programName, fileName])    
except Exception as inst:
    print("Unexpected error:")
    print (type(inst))    # the exception instance     
    print (inst )          # __str__ allows args to be printed directly
    raise
        
    
   

