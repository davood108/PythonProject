# 
###seasonality
from datetime import datetime
import matplotlib.pyplot as plt 
import pandas as pd
from statsmodels.tsa.seasonal import seasonal_decompose
def test_seasonality(ts_log, win,powplntUncode, path ):
    ts_log.index = pd.to_datetime(ts_log.index)
   # print(ts_log)
    decomposition = seasonal_decompose(ts_log.values, freq=win)
    trend = decomposition.trend
    seasonal = decomposition.seasonal
    residual = decomposition.resid
    k=plt.figure()
    plt.subplot(411)
    plt.plot(ts_log, label='Original')
    plt.legend(loc='best')
    plt.subplot(412)
    plt.plot(trend, label='Trend')
    plt.legend(loc='best')
    plt.subplot(413)
    plt.plot(seasonal,label='Seasonality')
    plt.legend(loc='best')
    plt.subplot(414)
    plt.plot(residual, label='Residuals')
    plt.legend(loc='best')
    plt.tight_layout()
    #k.show()
    plt.savefig(path+'/Trend_'+powplntUncode+'.pdf', format='pdf', dpi=1300)
    plt.clf()
