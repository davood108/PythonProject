import numpy as np
import inspect
import matplotlib.pyplot as plt
import seaborn as sns
import sys

# def main():
#     for num in [10, 50]:
#         # Generate some data
#         x = np.random.normal(0, 0.5, num-3)
# 
#         # Add three outliers...
#         x = np.r_[x, -3, -10, 12]
#         print(x)
#         print('This is Test')
#         plotOutLier(x)
# 
#     plt.show()

def mad_based_outlier(points, thresh=3.5):
    if len(points.shape) == 1:
        points = points[:,None]
    median = np.median(points, axis=0)
    diff = np.sum((points - median)**2, axis=-1)
    diff = np.sqrt(diff)
    #print(diff)
    med_abs_deviation = np.median(diff)
    if med_abs_deviation==0:
        #print("MAD based NOT valid")
        med_abs_deviation=med_abs_deviation+0.0001
        #print(med_abs_deviation)
    else:
        modified_z_score = 0.6745 * diff / med_abs_deviation
    
    modified_z_score = 0.6745 * diff / med_abs_deviation

    return modified_z_score > thresh

def percentile_based_outlier(data, threshold=95):
    diff = (100 - threshold) / 2.0
    minval, maxval = np.percentile(data, [diff, 100 - diff])
    return (data < minval) | (data > maxval)

def plotOutLier2(x):
    outliers= mad_based_outlier(x, thresh=3.5)
    # print(" Number of Outlier ", end='')
    # print(outliers[outliers ==True])
    # print(len(outliers))

def plotOutLier(x, yz, path):
    fig, axes = plt.subplots(nrows=2)  #2
    for ax, func in zip(axes, [ percentile_based_outlier, mad_based_outlier]): #
        sns.distplot(x, ax=ax, rug=True, hist=False)
        outliers = x[func(x)]
        ax.plot(outliers, np.zeros_like(outliers), 'ro', clip_on=False)

    kwargs = dict(y=0.95, x=0.05, ha='left', va='top')
    axes[0].set_title('Percentile-based Outliers', **kwargs)
    axes[1].set_title('MAD-based Outliers', **kwargs) 
    fig.suptitle('Comparing Outlier Tests with n={}'.format(len(x)), size=14)
    #fig.show()
    plt.savefig(path+'/Outlier_'+str(yz)+'.pdf', format='pdf', dpi=1200)
    plt.clf()
    return outliers
 
def outlierVar(tmp):
    """tmp is a list of numbers"""
    outs = []
    mean = sum(tmp)/(1.0*len(tmp))
    var = sum((tmp[i] - mean)**2 for i in range(0, len(tmp)))/(1.0*len(tmp))
    std = var**0.5
    outs = [tmp[i] for i in range(0, len(tmp)) if abs(tmp[i]-mean) > 1.96*std]
    return outs  
    
    
def is_outlier_doubleMAD(points):
    """
    FOR ASSYMMETRIC DISTRIBUTION
    Returns : outlier ###filtered array excluding the outliers

    Parameters : the actual data Points array

    Calculates median to divide data into 2 halves.(skew conditions handled)
    Then those two halves are treated as separate data with calculation same as for symmetric distribution.(first answer) 
    Only difference being , the thresholds are now the median distance of the right and left median with the actual data median
    """

    if len(points.shape) == 1:
        points = points[:,None]
    median = np.median(points, axis=0)
    medianIndex = int(points.size/2)

    leftData = np.copy(points[0:medianIndex])
    rightData = np.copy(points[medianIndex:points.size])

    median1 = np.median(leftData, axis=0)
    diff1 = np.sum((leftData - median1)**2, axis=-1)
    diff1 = np.sqrt(diff1)

    median2 = np.median(rightData, axis=0)
    diff2 = np.sum((rightData - median2)**2, axis=-1)
    diff2 = np.sqrt(diff2)

    med_abs_deviation1 = max(np.median(diff1),0.000001)
    med_abs_deviation2 = max(np.median(diff2),0.000001)

    threshold1 = ((median-median1)/med_abs_deviation1)*3
    threshold2 = ((median2-median)/med_abs_deviation2)*3

    #if any threshold is 0 -> no outliers
    if threshold1==0:
        threshold1 = sys.maxsize#int
    if threshold2==0:
        threshold2 = sys.maxsize#int
    #multiplied by a factor so that only the outermost points are removed
    modified_z_score1 = 0.6745 * diff1 / med_abs_deviation1
    modified_z_score2 = 0.6745 * diff2 / med_abs_deviation2

    filtered1 = []
    i = 0
    for data in modified_z_score1:
        if data < threshold1:
            filtered1.append(leftData[i])
        i += 1
    i = 0
    filtered2 = []
    for data in modified_z_score2:
        if data < threshold2:
            filtered2.append(rightData[i])
        i += 1

    filtered = filtered1 + filtered2
    return filtered  
    

# main()
