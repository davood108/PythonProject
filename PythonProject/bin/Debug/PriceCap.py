import pandas as pd
import math
def Det_PriceCap(points, un):
    df_0=points[points['UnitCode'] == un]
    df_Final=pd.DataFrame(columns=["Date","Hour", "Cap"])  #
    i_P=0
    for index, row in df_0.iterrows():
        df_Hour=points[(points['Date'] == row['Date'])] 
        PrCap=0
        for index, row_h in df_Hour.iterrows():
            maxC=0
            if row_h["Price10"] is not None and not math.isnan (row_h['Price10']):
                maxC=row_h["Price10"]
            elif row_h["Price9"] is not None and not math.isnan (row_h['Price9']):
                maxC=row_h["Price9"]
            elif row_h["Price9"] is not None and not math.isnan (row_h['Price8']):
                maxC=row_h["Price8"]
            elif row_h["Price9"] is not None and not math.isnan (row_h['Price7']):
                maxC=row_h["Price7"]
            elif row_h["Price9"] is not None and not math.isnan (row_h['Price6']):
                maxC=row_h["Price6"]
            elif row_h["Price9"] is not None and not math.isnan (row_h['Price5']):
                maxC=row_h["Price5"]
            elif row_h["Price9"] is not None and not math.isnan (row_h['Price4']):
                maxC=row_h["Price4"]
            elif row_h["Price9"] is not None and not math.isnan (row_h['Price3']):
                maxC=row_h["Price3"]
            elif row_h["Price2"] is not None and not math.isnan (row_h['Price2']):
                maxC=row_h["Price2"]
            elif row_h["Price1"] is not None and not math.isnan (row_h['Price1']):
                maxC=row_h["Price1"]
            elif row_h["Price0"] is not None and not math.isnan (row_h['Price0']):
                maxC=row_h["Price0"]
            if maxC > PrCap:
                PrCap=maxC
        #PrCap=444000    
        temp=pd.DataFrame(columns=["Date","Hour", "Cap"])
        #print(float(PrCap))
        temp.loc[i_P]=[row['Date'], row['HourNo'], float(PrCap)]
        i_P=i_P+1
        df_Final=df_Final.append(temp)
    return df_Final
    