﻿using System.Windows.Forms;

namespace PythonProject
{
    partial class FrmModeling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUnitCode = new System.Windows.Forms.Label();
            this.dateTimeSelectorStart = new Atf.UI.DateTimeSelector();
            this.dateTimeSelectorEnd = new Atf.UI.DateTimeSelector();
            this.lblCorrelation = new System.Windows.Forms.Label();
            this.lblStep = new System.Windows.Forms.Label();
            this.lblPPName = new System.Windows.Forms.Label();
            this.btnSelectPowerPlant = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxDiffer = new PythonProject.MyControls.MyComboBox();
            this.comboBoxLog = new PythonProject.MyControls.MyComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comBoxMoving = new PythonProject.MyControls.MyComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxSpectrum = new PythonProject.MyControls.MyComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxAutoco = new PythonProject.MyControls.MyComboBox();
            this.comboBoxStationary = new PythonProject.MyControls.MyComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxSeason = new PythonProject.MyControls.MyComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioBtnPowerPlant = new System.Windows.Forms.RadioButton();
            this.radioBtnUnit = new System.Windows.Forms.RadioButton();
            this.comboBoxUnits = new PythonProject.MyControls.MyComboBox();
            this.txtPowerPlantCode = new PythonProject.MyControls.MyTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioBtnCorrelation = new System.Windows.Forms.RadioButton();
            this.radioBtnTimeSeries = new System.Windows.Forms.RadioButton();
            this.lblDescription = new System.Windows.Forms.Label();
            this.comboBoxCorrelation = new PythonProject.MyControls.MyComboBox();
            this.comboBoxStep = new PythonProject.MyControls.MyComboBox();
            this.comboBoxMethods = new PythonProject.MyControls.MyComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "PowerPlant Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 264);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "ToDate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 229);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "FromDate";
            // 
            // lblUnitCode
            // 
            this.lblUnitCode.AutoSize = true;
            this.lblUnitCode.Location = new System.Drawing.Point(13, 92);
            this.lblUnitCode.Name = "lblUnitCode";
            this.lblUnitCode.Size = new System.Drawing.Size(54, 13);
            this.lblUnitCode.TabIndex = 15;
            this.lblUnitCode.Text = "Unit Code";
            // 
            // dateTimeSelectorStart
            // 
            this.dateTimeSelectorStart.Location = new System.Drawing.Point(156, 221);
            this.dateTimeSelectorStart.Name = "dateTimeSelectorStart";
            this.dateTimeSelectorStart.Size = new System.Drawing.Size(169, 21);
            this.dateTimeSelectorStart.TabIndex = 4;
            this.toolTip1.SetToolTip(this.dateTimeSelectorStart, "The first day of month");
            this.dateTimeSelectorStart.UsePersianFormat = true;
            // 
            // dateTimeSelectorEnd
            // 
            this.dateTimeSelectorEnd.Location = new System.Drawing.Point(156, 256);
            this.dateTimeSelectorEnd.Name = "dateTimeSelectorEnd";
            this.dateTimeSelectorEnd.Size = new System.Drawing.Size(169, 21);
            this.dateTimeSelectorEnd.TabIndex = 5;
            this.toolTip1.SetToolTip(this.dateTimeSelectorEnd, "The last day of month");
            this.dateTimeSelectorEnd.UsePersianFormat = true;
            // 
            // lblCorrelation
            // 
            this.lblCorrelation.AutoSize = true;
            this.lblCorrelation.Location = new System.Drawing.Point(34, 334);
            this.lblCorrelation.Name = "lblCorrelation";
            this.lblCorrelation.Size = new System.Drawing.Size(107, 13);
            this.lblCorrelation.TabIndex = 24;
            this.lblCorrelation.Text = "Correlation Unit Code";
            // 
            // lblStep
            // 
            this.lblStep.AutoSize = true;
            this.lblStep.Location = new System.Drawing.Point(34, 299);
            this.lblStep.Name = "lblStep";
            this.lblStep.Size = new System.Drawing.Size(29, 13);
            this.lblStep.TabIndex = 23;
            this.lblStep.Text = "Step";
            // 
            // lblPPName
            // 
            this.lblPPName.AutoSize = true;
            this.lblPPName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblPPName.Location = new System.Drawing.Point(351, 61);
            this.lblPPName.Name = "lblPPName";
            this.lblPPName.Size = new System.Drawing.Size(11, 13);
            this.lblPPName.TabIndex = 20;
            this.lblPPName.Text = "*";
            // 
            // btnSelectPowerPlant
            // 
            this.btnSelectPowerPlant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnSelectPowerPlant.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnSelectPowerPlant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectPowerPlant.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectPowerPlant.Location = new System.Drawing.Point(311, 53);
            this.btnSelectPowerPlant.Name = "btnSelectPowerPlant";
            this.btnSelectPowerPlant.Size = new System.Drawing.Size(34, 31);
            this.btnSelectPowerPlant.TabIndex = 2;
            this.btnSelectPowerPlant.Text = "...";
            this.toolTip1.SetToolTip(this.btnSelectPowerPlant, "Search and Select Powerplants");
            this.btnSelectPowerPlant.UseVisualStyleBackColor = false;
            this.btnSelectPowerPlant.Click += new System.EventHandler(this.btnSelectPowerPlant_Click);
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnRun.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRun.Image = global::PythonProject.Properties.Resources.RunIcon1;
            this.btnRun.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRun.Location = new System.Drawing.Point(156, 388);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(170, 37);
            this.btnRun.TabIndex = 9;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = false;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(34, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Method";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Log function";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(392, 229);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(457, 180);
            this.panel1.TabIndex = 8;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.comboBoxDiffer);
            this.groupBox2.Controls.Add(this.comboBoxLog);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.comBoxMoving);
            this.groupBox2.Location = new System.Drawing.Point(232, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(214, 120);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Stationarize The Series";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Differencing";
            // 
            // comboBoxDiffer
            // 
            this.comboBoxDiffer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDiffer.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxDiffer.FormattingEnabled = true;
            this.comboBoxDiffer.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBoxDiffer.Location = new System.Drawing.Point(150, 96);
            this.comboBoxDiffer.Name = "comboBoxDiffer";
            this.comboBoxDiffer.Size = new System.Drawing.Size(55, 21);
            this.comboBoxDiffer.TabIndex = 5;
            this.comboBoxDiffer.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // comboBoxLog
            // 
            this.comboBoxLog.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLog.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxLog.FormattingEnabled = true;
            this.comboBoxLog.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBoxLog.Location = new System.Drawing.Point(150, 23);
            this.comboBoxLog.Name = "comboBoxLog";
            this.comboBoxLog.Size = new System.Drawing.Size(55, 21);
            this.comboBoxLog.TabIndex = 0;
            this.comboBoxLog.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Moving Average";
            // 
            // comBoxMoving
            // 
            this.comBoxMoving.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxMoving.EnterColor = System.Drawing.Color.LightGreen;
            this.comBoxMoving.FormattingEnabled = true;
            this.comBoxMoving.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comBoxMoving.Location = new System.Drawing.Point(150, 57);
            this.comBoxMoving.Name = "comBoxMoving";
            this.comBoxMoving.Size = new System.Drawing.Size(55, 21);
            this.comBoxMoving.TabIndex = 1;
            this.comBoxMoving.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.comboBoxSpectrum);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.comboBoxAutoco);
            this.groupBox1.Controls.Add(this.comboBoxStationary);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.comboBoxSeason);
            this.groupBox1.Location = new System.Drawing.Point(12, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(214, 158);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Analyze The Series";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 128);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Power Spectrum Analysis";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Autocorrelation ";
            // 
            // comboBoxSpectrum
            // 
            this.comboBoxSpectrum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSpectrum.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxSpectrum.FormattingEnabled = true;
            this.comboBoxSpectrum.Items.AddRange(new object[] {
            "0",
            "1"});
            this.comboBoxSpectrum.Location = new System.Drawing.Point(150, 128);
            this.comboBoxSpectrum.Name = "comboBoxSpectrum";
            this.comboBoxSpectrum.Size = new System.Drawing.Size(55, 21);
            this.comboBoxSpectrum.TabIndex = 4;
            this.comboBoxSpectrum.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Stationary Test";
            // 
            // comboBoxAutoco
            // 
            this.comboBoxAutoco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAutoco.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxAutoco.FormattingEnabled = true;
            this.comboBoxAutoco.Items.AddRange(new object[] {
            "0",
            "1"});
            this.comboBoxAutoco.Location = new System.Drawing.Point(150, 92);
            this.comboBoxAutoco.Name = "comboBoxAutoco";
            this.comboBoxAutoco.Size = new System.Drawing.Size(55, 21);
            this.comboBoxAutoco.TabIndex = 4;
            this.comboBoxAutoco.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // comboBoxStationary
            // 
            this.comboBoxStationary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStationary.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxStationary.FormattingEnabled = true;
            this.comboBoxStationary.Items.AddRange(new object[] {
            "0",
            "1"});
            this.comboBoxStationary.Location = new System.Drawing.Point(150, 22);
            this.comboBoxStationary.Name = "comboBoxStationary";
            this.comboBoxStationary.Size = new System.Drawing.Size(55, 21);
            this.comboBoxStationary.TabIndex = 2;
            this.comboBoxStationary.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Seasonality";
            // 
            // comboBoxSeason
            // 
            this.comboBoxSeason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSeason.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxSeason.FormattingEnabled = true;
            this.comboBoxSeason.Items.AddRange(new object[] {
            "0",
            "1"});
            this.comboBoxSeason.Location = new System.Drawing.Point(150, 57);
            this.comboBoxSeason.Name = "comboBoxSeason";
            this.comboBoxSeason.Size = new System.Drawing.Size(55, 21);
            this.comboBoxSeason.TabIndex = 3;
            this.comboBoxSeason.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.lblDescription);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.btnRun);
            this.panel2.Controls.Add(this.lblCorrelation);
            this.panel2.Controls.Add(this.comboBoxCorrelation);
            this.panel2.Controls.Add(this.comboBoxStep);
            this.panel2.Controls.Add(this.lblStep);
            this.panel2.Controls.Add(this.dateTimeSelectorStart);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.comboBoxMethods);
            this.panel2.Controls.Add(this.dateTimeSelectorEnd);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(862, 530);
            this.panel2.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.radioBtnPowerPlant);
            this.groupBox4.Controls.Add(this.radioBtnUnit);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.comboBoxUnits);
            this.groupBox4.Controls.Add(this.lblUnitCode);
            this.groupBox4.Controls.Add(this.txtPowerPlantCode);
            this.groupBox4.Controls.Add(this.lblPPName);
            this.groupBox4.Controls.Add(this.btnSelectPowerPlant);
            this.groupBox4.Location = new System.Drawing.Point(23, 85);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(826, 130);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            // 
            // radioBtnPowerPlant
            // 
            this.radioBtnPowerPlant.AutoSize = true;
            this.radioBtnPowerPlant.Location = new System.Drawing.Point(137, 19);
            this.radioBtnPowerPlant.Name = "radioBtnPowerPlant";
            this.radioBtnPowerPlant.Size = new System.Drawing.Size(79, 17);
            this.radioBtnPowerPlant.TabIndex = 26;
            this.radioBtnPowerPlant.Text = "PowerPlant";
            this.radioBtnPowerPlant.UseVisualStyleBackColor = true;
            this.radioBtnPowerPlant.CheckedChanged += new System.EventHandler(this.radioBtnUnit_CheckedChanged);
            // 
            // radioBtnUnit
            // 
            this.radioBtnUnit.AutoSize = true;
            this.radioBtnUnit.Checked = true;
            this.radioBtnUnit.Location = new System.Drawing.Point(16, 18);
            this.radioBtnUnit.Name = "radioBtnUnit";
            this.radioBtnUnit.Size = new System.Drawing.Size(44, 17);
            this.radioBtnUnit.TabIndex = 25;
            this.radioBtnUnit.TabStop = true;
            this.radioBtnUnit.Text = "Unit";
            this.radioBtnUnit.UseVisualStyleBackColor = true;
            this.radioBtnUnit.CheckedChanged += new System.EventHandler(this.radioBtnUnit_CheckedChanged);
            // 
            // comboBoxUnits
            // 
            this.comboBoxUnits.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxUnits.FormattingEnabled = true;
            this.comboBoxUnits.Location = new System.Drawing.Point(136, 92);
            this.comboBoxUnits.Name = "comboBoxUnits";
            this.comboBoxUnits.Size = new System.Drawing.Size(169, 21);
            this.comboBoxUnits.TabIndex = 3;
            // 
            // txtPowerPlantCode
            // 
            this.txtPowerPlantCode.EnterColor = System.Drawing.Color.LightGreen;
            this.txtPowerPlantCode.Location = new System.Drawing.Point(137, 57);
            this.txtPowerPlantCode.Name = "txtPowerPlantCode";
            this.txtPowerPlantCode.OnlyDigit = false;
            this.txtPowerPlantCode.RegexPattern = null;
            this.txtPowerPlantCode.Size = new System.Drawing.Size(169, 20);
            this.txtPowerPlantCode.TabIndex = 1;
            this.txtPowerPlantCode.Leave += new System.EventHandler(this.txtPowerPlantCode_Leave);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioBtnCorrelation);
            this.groupBox3.Controls.Add(this.radioBtnTimeSeries);
            this.groupBox3.Location = new System.Drawing.Point(23, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(362, 43);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            // 
            // radioBtnCorrelation
            // 
            this.radioBtnCorrelation.AutoSize = true;
            this.radioBtnCorrelation.Location = new System.Drawing.Point(137, 19);
            this.radioBtnCorrelation.Name = "radioBtnCorrelation";
            this.radioBtnCorrelation.Size = new System.Drawing.Size(75, 17);
            this.radioBtnCorrelation.TabIndex = 26;
            this.radioBtnCorrelation.Text = "Correlation";
            this.radioBtnCorrelation.UseVisualStyleBackColor = true;
            // 
            // radioBtnTimeSeries
            // 
            this.radioBtnTimeSeries.AutoSize = true;
            this.radioBtnTimeSeries.Checked = true;
            this.radioBtnTimeSeries.Location = new System.Drawing.Point(16, 18);
            this.radioBtnTimeSeries.Name = "radioBtnTimeSeries";
            this.radioBtnTimeSeries.Size = new System.Drawing.Size(80, 17);
            this.radioBtnTimeSeries.TabIndex = 25;
            this.radioBtnTimeSeries.TabStop = true;
            this.radioBtnTimeSeries.Text = "Time Series";
            this.radioBtnTimeSeries.UseVisualStyleBackColor = true;
            this.radioBtnTimeSeries.CheckedChanged += new System.EventHandler(this.radioBtnTimeSeries_CheckedChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblDescription.Location = new System.Drawing.Point(389, 62);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(11, 13);
            this.lblDescription.TabIndex = 27;
            this.lblDescription.Text = "*";
            // 
            // comboBoxCorrelation
            // 
            this.comboBoxCorrelation.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxCorrelation.FormattingEnabled = true;
            this.comboBoxCorrelation.Location = new System.Drawing.Point(158, 334);
            this.comboBoxCorrelation.Name = "comboBoxCorrelation";
            this.comboBoxCorrelation.Size = new System.Drawing.Size(169, 21);
            this.comboBoxCorrelation.TabIndex = 7;
            // 
            // comboBoxStep
            // 
            this.comboBoxStep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStep.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxStep.FormattingEnabled = true;
            this.comboBoxStep.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxStep.Location = new System.Drawing.Point(158, 299);
            this.comboBoxStep.Name = "comboBoxStep";
            this.comboBoxStep.Size = new System.Drawing.Size(168, 21);
            this.comboBoxStep.TabIndex = 0;
            this.comboBoxStep.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // comboBoxMethods
            // 
            this.comboBoxMethods.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMethods.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxMethods.FormattingEnabled = true;
            this.comboBoxMethods.Location = new System.Drawing.Point(158, 58);
            this.comboBoxMethods.Name = "comboBoxMethods";
            this.comboBoxMethods.Size = new System.Drawing.Size(225, 21);
            this.comboBoxMethods.TabIndex = 0;
            this.comboBoxMethods.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // FrmModeling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(886, 554);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.Name = "FrmModeling";
            this.Text = "Modeling";
            this.Load += new System.EventHandler(this.FrmModeling_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUnitCode;
        private Atf.UI.DateTimeSelector dateTimeSelectorStart;
        private Atf.UI.DateTimeSelector dateTimeSelectorEnd;
        private MyControls.MyTextBox txtPowerPlantCode;
        private MyControls.MyComboBox comboBoxMethods;
        private System.Windows.Forms.Label label10;
        private Button btnRun;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Button btnSelectPowerPlant;
        private System.Windows.Forms.Label lblPPName;
        private MyControls.MyComboBox comboBoxUnits;
        private System.Windows.Forms.Label lblStep;
        private MyControls.MyComboBox comboBoxCorrelation;
        private System.Windows.Forms.Label lblCorrelation;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private MyControls.MyComboBox comboBoxDiffer;
        private System.Windows.Forms.Label label5;
        private MyControls.MyComboBox comBoxMoving;
        private MyControls.MyComboBox comboBoxLog;
        private System.Windows.Forms.Panel panel2;
        private RadioButton radioBtnCorrelation;
        private RadioButton radioBtnTimeSeries;
        private Label lblDescription;
        private MyControls.MyComboBox comboBoxStep;
        private GroupBox groupBox2;
        private GroupBox groupBox1;
        private Label label8;
        private Label label6;
        private MyControls.MyComboBox comboBoxAutoco;
        private MyControls.MyComboBox comboBoxStationary;
        private Label label7;
        private MyControls.MyComboBox comboBoxSeason;
        private GroupBox groupBox4;
        private RadioButton radioBtnPowerPlant;
        private RadioButton radioBtnUnit;
        private GroupBox groupBox3;
        private Label label11;
        private MyControls.MyComboBox comboBoxSpectrum;
       

    }
}