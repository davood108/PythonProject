﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PythonProject
{
    public partial class FrmPreProccess : Form
    {
        private string _pythonPath;// = @"C:\Program Files\Anaconda3\python.exe";
        private List<GeneralMethod> _methods;
        public FrmPreProccess()
        {
            InitializeComponent();
           
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _pythonPath = XmlUtility.GetPythonPath();

            Input input;
            try
            {
                 input = XmlUtility.ReadInput();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
                return;
            }

            //txtAttribute1.Text = input.Attribute1;
            //txtAttribute2.Text = input.Attribute2;
            txtPowerPlantCode.Text = input.PowerPlantCode;
            comboBoxUnits.Text = input.UnitCode;
            dateTimeSelectorStart.Text = input.FirstDate;
            dateTimeSelectorEnd.Text = input.LastDate;
            if (input.IsUnit)
                radioBtnUnit.Checked = true;
            else
                radioBtnPowerPlant.Checked = true;

           _methods = XmlUtility.GetPreProcessingMethods();
           foreach (GeneralMethod method in _methods)
            {
                comboBoxMethods.Items.Add(method);

            }
            comboBoxMethods.SelectedIndex = 0;

            List<string> attribs = XmlUtility.GetPreProcessingAttributes();
            foreach (string attr in attribs)
            {
                comboBoxAttribute1.Items.Add(attr);
                comboBoxAttribute2.Items.Add(attr);
            }
            try
            {
                comboBoxAttribute1.Text = input.Attribute1;
                comboBoxAttribute2.Text = input.Attribute2;
            }
            catch (Exception)
            {
                
                throw;
            }



            LoadUnits();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
           
            errorProvider1.Clear();

            PersianDateTime date = new PersianDateTime(dateTimeSelectorStart.Text);
            if (date.Day != 1)
            {
                errorProvider1.SetError(dateTimeSelectorStart, "Invalid Date. The selected date must be the first day of the month");
                return;
            }

            date = new PersianDateTime(dateTimeSelectorEnd.Text);
            if (date.Day != date.MonthDays)
            {
                errorProvider1.SetError(dateTimeSelectorEnd, "Invalid Date. The selected date must be the last day of the month");
                return;
            }

            if (radioBtnUnit.Checked && txtPowerPlantCode.Text.Contains(','))
            {
                errorProvider1.SetError(txtPowerPlantCode, "Please select one powerplant");
                return;
            }
            Input input = new Input
            {
                Attribute1 = comboBoxAttribute1.Text,
                Attribute2 = comboBoxAttribute2.Text,
                PowerPlantCode = txtPowerPlantCode.Text,
                UnitCode = comboBoxUnits.Text,
                FirstDate = dateTimeSelectorStart.Text,
                LastDate = dateTimeSelectorEnd.Text,
                IsUnit = radioBtnUnit.Checked
            };
            try
            {
                XmlUtility.WriteInput(input);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
                return;
            }
           

            GeneralMethod method = _methods.SingleOrDefault(m => m.Name == ((comboBoxMethods.SelectedItem as GeneralMethod).Name));
            RunScript(method.ScriptFile);

            Process process = Process.Start(Utility.GetOutputPath(input.FirstDate, input.LastDate));
        }


        private void RunScript(string scriptName)
        {
            
            string cmd =
                scriptName;
            string args = "";
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = _pythonPath.Trim();
            start.Arguments = string.Format("{0} {1}", cmd, args);
            start.UseShellExecute = false;
            //start.RedirectStandardOutput = true;
            if (!File.Exists(start.FileName))
            {
                MessageBox.Show("The Python application is not installed or the installation path in Config file is incorrect (" + start.FileName + ")", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            using (Process process = Process.Start(start))
            {

                //using (StreamReader reader = process.StandardOutput)
                //{
                //    string result = reader.ReadToEnd();
                //    if (string.IsNullOrWhiteSpace(result))
                //    {
                //        MessageBox.Show("Finish","OK",MessageBoxButtons.OK,MessageBoxIcon.Information);
                //    }
                //    else
                //    {
                //        MessageBox.Show(result, "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    }

                //}
            }
        }

       

        private void comboBoxMethods_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxAttribute2.Visible = comboBoxMethods.Text == "Scatter";
            label9.Visible = comboBoxMethods.Text == "Scatter";
        }
        
        private void btnSelectPowerPlant_Click(object sender, EventArgs e)
        {
            FrmSelectPowerPlant frm = new FrmSelectPowerPlant(radioBtnPowerPlant.Checked);
            frm.StartPosition=FormStartPosition.CenterParent;
            if (frm.ShowDialog()==DialogResult.OK)
            {
                txtPowerPlantCode.Text = frm.SelectedPowerPlantCode;
                toolTip1.SetToolTip(txtPowerPlantCode, frm.SelectedPowerPlantCode);
                lblPPName.Text = frm.SelectedPowerPlantName;

            }
            LoadUnits();
        }

        private void txtPowerPlantCode_Leave(object sender, EventArgs e)
        {
            LoadUnits();
        }

        private void LoadUnits()
        {
            comboBoxUnits.Items.Clear();
            DataTable dtUnits;
            try
            {
                dtUnits = DbUtility.GetUnitsCode(txtPowerPlantCode.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
          
            for (int i = 0; i < dtUnits.Rows.Count; i++)
            {
                comboBoxUnits.Items.Add(dtUnits.Rows[i][0].ToString());
            }
            if (comboBoxUnits.Items.Count>0)
            {
                comboBoxUnits.SelectedIndex = 0;
            }
        }

        private void radioBtnPowerPlant_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxUnits.Enabled = radioBtnUnit.Checked;            
        }


    }
}
