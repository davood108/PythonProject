﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace PythonProject
{
    public partial class FrmSetting : Form
    {
          Color _BackColor { get; set; }
          Color _PanelColor { get; set; }
          Color _ButtonColor { get; set; }
        public FrmSetting()
        {
            InitializeComponent();
        }

        private void btnApplySetting_Click(object sender, EventArgs e)
        {
            string settingString = String.Format("{0};{1};{2}", _BackColor.ToArgb(), _ButtonColor.ToArgb(),
                _PanelColor.ToArgb());
            XmlUtility.SaveSettingString(settingString);
           // User.CurrentUser.SaveSettingString(settingString);

            FormUtility.BackColor = _BackColor;
            FormUtility.ButtonColor = _ButtonColor;
            FormUtility.PanelColor = _PanelColor;

            foreach (Form openForm in Application.OpenForms)
            {
                openForm.SetAppearanceSetting();
            }
        }

        private void btnBackColor_Click(object sender, EventArgs e)
        {
            radColorDialog1.SelectedColor = _BackColor;
            if (radColorDialog1.ShowDialog()==DialogResult.OK)
            {
                _BackColor = radColorDialog1.SelectedColor;
                this.BackColor = _BackColor;
                //tableLayoutPanel1.BackColor = _BackColor;
            }
            
        }

        private void btnPanelColor_Click(object sender, EventArgs e)
        {
            radColorDialog1.SelectedColor = _PanelColor;
            if (radColorDialog1.ShowDialog() == DialogResult.OK)
            {
                _PanelColor = radColorDialog1.SelectedColor;
                panel1.BackColor = _PanelColor;
       
            }
        }

        private void btnButtonColor_Click(object sender, EventArgs e)
        {
            radColorDialog1.SelectedColor = _ButtonColor;
            if (radColorDialog1.ShowDialog() == DialogResult.OK)
            {
                _ButtonColor = radColorDialog1.SelectedColor;
                btnBackColor.BackColor = _ButtonColor;
                btnButtonColor.BackColor = _ButtonColor;
                btnPanelColor.BackColor = _ButtonColor;
                btnApplySetting.BackColor = _ButtonColor;
                btnDefaultSetting.BackColor = _ButtonColor;
            }
        }

        private void FrmSetting_Load(object sender, EventArgs e)
        {
            _BackColor = FormUtility.BackColor;
            _PanelColor = FormUtility.PanelColor;
            _ButtonColor = FormUtility.ButtonColor;

            txtPythonPath.Text = XmlUtility.GetPythonPath();
            DbConnection conn = XmlUtility.GetDbConnection();
            txtServer.Text = conn.Server;
            txtLogin.Text = conn.Login;
            txtPass.Text = "********";


        }

        private void btnDefaultSetting_Click(object sender, EventArgs e)
        {
            _BackColor = Color.LightSkyBlue;
            _PanelColor = Color.LightSteelBlue;
            _ButtonColor = Color.Beige;

            this.BackColor = _BackColor;
            //tableLayoutPanel1.BackColor = _BackColor;

            panel1.BackColor = _PanelColor;
    
            btnBackColor.BackColor = _ButtonColor;
            btnButtonColor.BackColor = _ButtonColor;
            btnPanelColor.BackColor = _ButtonColor;
            btnApplySetting.BackColor = _ButtonColor;
            btnDefaultSetting.BackColor = _ButtonColor;
        }
        string EncrptByPython(string text)
        {
            string cmd = "Encrypt.py";
            string args = text;
            System.Diagnostics.ProcessStartInfo start = new System.Diagnostics.ProcessStartInfo();
            start.FileName = XmlUtility.GetPythonPath().Trim();
            start.Arguments = string.Format("{0} {1}", cmd, args);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            if (!System.IO.File.Exists(start.FileName))
            {
                throw new Exception("The Python application is not installed or the installation path in Config file is incorrect (" + start.FileName + ")");
            }

            using (System.Diagnostics.Process process = System.Diagnostics.Process.Start(start))
            {
                using (System.IO.StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    if (string.IsNullOrWhiteSpace(result) || result == "error")
                    {
                        throw new Exception("Unexpected Error");

                    }
                    else
                    {
                        return result;
                    }

                }
            }           
        }
        private void btnServer_Click(object sender, EventArgs e)
        {
            string server = txtServer.Text.Trim();
            if (server == "")
            {
                errorProvider1.SetError(txtServer, "Please enter value");
                return;
            }
            string encrypted1 = "";
            string encrypted2 = "";
            try
            {
                encrypted1 = EncrptByPython(server);
                encrypted2 = Protection.Encrypt(server);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                XmlUtility.SaveServerAddress(encrypted1, encrypted2);                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("Saved successfully");


        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string login = txtLogin.Text.Trim();
            if (login == "")
            {
                errorProvider1.SetError(txtLogin, "Please enter value");
                return;
            }
            string encrypted1 = "";
            string encrypted2 = "";
            try
            {
                encrypted1 = EncrptByPython(login);
                encrypted2 = Protection.Encrypt(login);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                XmlUtility.SaveServerLogin(encrypted1, encrypted2);               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("Saved successfully");
        }

        private void btnPass_Click(object sender, EventArgs e)
        {
            string pass = txtPass.Text;
            if (pass == "")
            {
                errorProvider1.SetError(txtPass, "Please enter value");
                return;
            }
            string encrypted1 = "";
            string encrypted2 = "";
            try
            {
                encrypted1 = EncrptByPython(pass);
                encrypted2 = Protection.Encrypt(pass);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                XmlUtility.SaveServerPassword(encrypted1, encrypted2);                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("Saved successfully");

        }

        private void btnPyhtonPath_Click(object sender, EventArgs e)
        {
            string pythonPath = txtPythonPath.Text.Trim();
            if (pythonPath == "")
            {
                errorProvider1.SetError(txtPythonPath, "Please enter value");
                return;
            }
            
            try
            {
                XmlUtility.SavePythonPath(pythonPath);                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("Saved successfully");
        } 
    }
}
