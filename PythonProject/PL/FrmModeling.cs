﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PythonProject
{
    public partial class FrmModeling : Form
    {
        private string _pythonPath;// = @"C:\Program Files\Anaconda3\python.exe";
        private List<GeneralMethod> _methods;
        public FrmModeling()
        {
            InitializeComponent();
            _methods = XmlUtility.GetModelingMethods();
           
        }

        private void FrmModeling_Load(object sender, EventArgs e)
        {
            _pythonPath = XmlUtility.GetPythonPath();
            Input2 input;
            try
            {
                input = XmlUtility.ReadInput2();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
                return;
            }

            
            txtPowerPlantCode.Text = input.PowerPlantCode;
            comboBoxUnits.Text = input.UnitCode;
            dateTimeSelectorStart.Text = input.FirstDate;
            dateTimeSelectorEnd.Text = input.LastDate;
            comboBoxCorrelation.Text = input.OurUnitCode;
            comboBoxStep.Text = input.Step.ToString();

            comboBoxLog.Text = input.LogFunction.ToString();
            comBoxMoving.Text = input.MovingAverage.ToString();
            comboBoxStationary.Text = input.StationaryTest.ToString();
            comboBoxDiffer.Text = input.Differencing.ToString();
            comboBoxSeason.Text = input.Seasonality.ToString();
            comboBoxAutoco.Text = input.Autocorrelation.ToString();
            comboBoxSpectrum.Text = input.SpectrumAnalysis.ToString();

            if (input.IsUnit)
                radioBtnUnit.Checked = true;
            else
                radioBtnPowerPlant.Checked = true;

           
            LoadMethods();

            LoadUnits();
        }

        private void LoadMethods()
        {
            comboBoxMethods.Items.Clear();
            if (radioBtnTimeSeries.Checked)
            {
                panel1.Visible = true;
                foreach (GeneralMethod method in _methods.Where(m => m.Type == 1))
                {
                    comboBoxMethods.Items.Add(method);
                }
            }
            else
            {
                panel1.Visible = false;
                foreach (GeneralMethod method in _methods.Where(m => m.Type == 2))
                {
                    comboBoxMethods.Items.Add(method);
                }
            }

            comboBoxMethods.SelectedIndex = 0;
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
           
            errorProvider1.Clear();

            PersianDateTime date = new PersianDateTime(dateTimeSelectorStart.Text);
            if (date.Day!=1)
            {
                errorProvider1.SetError(dateTimeSelectorStart, "Invalid Date. The selected date must be the first day of the month");
                return;
            }

            date = new PersianDateTime(dateTimeSelectorEnd.Text);
            if (date.Day != date.MonthDays)
            {
                errorProvider1.SetError(dateTimeSelectorEnd, "Invalid Date. The selected date must be the last day of the month");
                return;
            }

            if (radioBtnUnit.Checked && txtPowerPlantCode.Text.Contains(','))
            {
                errorProvider1.SetError(txtPowerPlantCode, "Please select one powerplant");
                return;
            }
            //if (!txtStep.RegexSuccess)
            //{
            //    errorProvider1.SetError(txtStep, "Invalid Value: Enter integer Value");
            //    return;
            //}


            Input2 input = new Input2
            {

                PowerPlantCode = txtPowerPlantCode.Text,
                UnitCode = comboBoxUnits.Text,
                FirstDate = dateTimeSelectorStart.Text,
                LastDate = dateTimeSelectorEnd.Text,
                OurUnitCode = comboBoxCorrelation.Text,
                Step = int.Parse(comboBoxStep.Text),

                LogFunction = int.Parse(comboBoxLog.Text),
                MovingAverage = int.Parse(comBoxMoving.Text),
                StationaryTest = int.Parse(comboBoxStationary.Text),
                Differencing = int.Parse(comboBoxDiffer.Text),
                Seasonality = int.Parse(comboBoxSeason.Text),
                Autocorrelation = int.Parse(comboBoxAutoco.Text),
                SpectrumAnalysis = int.Parse(comboBoxSpectrum.Text),
                IsUnit = radioBtnUnit.Checked

            };
            
            try
            {
                XmlUtility.WriteInput2(input);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message);
                return;
            }

            GeneralMethod method = _methods.SingleOrDefault(m => m.Name == (comboBoxMethods.SelectedItem as GeneralMethod).Name);
            RunScript(method.ScriptFile);

            Process process = Process.Start(Utility.GetOutputPath(input.FirstDate, input.LastDate));
        }


        private void RunScript(string scriptName)
        {
            
            string cmd =
                scriptName;
            string args = "";
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = _pythonPath.Trim();
            start.Arguments = string.Format("{0} {1}", cmd, args);
            start.UseShellExecute = false;
           // start.RedirectStandardOutput = true;
            if (!File.Exists(start.FileName))
            {
                MessageBox.Show("The Python application is not installed or the installation path in Config file is incorrect (" + start.FileName + ")", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            using (Process process = Process.Start(start))
            {

                //using (StreamReader reader = process.StandardOutput)
                //{
                //    string result = reader.ReadToEnd();
                //    if (string.IsNullOrWhiteSpace(result))
                //    {
                //        MessageBox.Show("Finish", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    }
                //    else
                //    {
                //        MessageBox.Show(result, "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    }
                //}
            }

            
        }



        private void comboBoxMethods_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtAttribute2.Visible = comboBoxMethods.Text == "Box Plot";
            //label9.Visible = comboBoxMethods.Text == "Box Plot";

            lblUnitCode.Visible = true;
            comboBoxUnits.Visible = true;
            lblCorrelation.Visible = false;
            comboBoxCorrelation.Visible = false;
            //panel1.Visible = false;
            lblStep.Visible = false;
            comboBoxStep.Visible = false;

            GeneralMethod method = comboBoxMethods.SelectedItem as GeneralMethod;
            if (method!=null)
            {
                lblDescription.Text = method.Description;

                switch (method.ScriptFile.ToLower())
                {
                    //case "createWep_1A.py":
                    //    panel1.Visible = true;
                    //    break;

                    case "createcross_11.py":
                        lblUnitCode.Visible = false;
                        comboBoxUnits.Visible = false;
                        lblCorrelation.Visible = true;
                        comboBoxCorrelation.Visible = true;
                        break;

                    case "createplant_13a.py":
                        lblUnitCode.Visible = false;
                        comboBoxUnits.Visible = false;
                        break;

                    case "createstep_6a.py":
                        lblStep.Visible = true;
                        comboBoxStep.Visible = true;
                        break;
                    case "createstep_7a.py":
                        lblStep.Visible = true;
                        comboBoxStep.Visible = true;
                        break;
                }
                /*
                if (method.ScriptFile == "createWep_1A.py")
                {
                    panel1.Visible = true;
                }
                if (method.ScriptFile == "createcross_11.py")
                {
                    lblUnitCode.Visible = false;
                    comboBoxUnits.Visible = false;
                    lblCorrelation.Visible = true;
                    comboBoxCorrelation.Visible = true;

                }
                else if (method.ScriptFile == "createPlant_13A.py")
                {
                    lblUnitCode.Visible = false;
                    comboBoxUnits.Visible = false;
                }
              
                //createStep_6A
                 */
            }
            

        }


        private void btnSelectPowerPlant_Click(object sender, EventArgs e)
        {

            FrmSelectPowerPlant frm = new FrmSelectPowerPlant(radioBtnPowerPlant.Checked);
            frm.StartPosition=FormStartPosition.CenterParent;
            if (frm.ShowDialog()==DialogResult.OK)
            {
                txtPowerPlantCode.Text = frm.SelectedPowerPlantCode;
                toolTip1.SetToolTip(txtPowerPlantCode, frm.SelectedPowerPlantCode);
                lblPPName.Text = frm.SelectedPowerPlantName;
            }
            LoadUnits();
        }

        private void txtPowerPlantCode_Leave(object sender, EventArgs e)
        {
            LoadUnits();
        }

        private void LoadUnits()
        {
            comboBoxUnits.Items.Clear();
            comboBoxCorrelation.Items.Clear();
            DataTable dtUnits;
            try
            {
                dtUnits = DbUtility.GetUnitsCode(txtPowerPlantCode.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

           
            for (int i = 0; i < dtUnits.Rows.Count; i++)
            {
                comboBoxUnits.Items.Add(dtUnits.Rows[i][0].ToString());
                comboBoxCorrelation.Items.Add(dtUnits.Rows[i][0].ToString());
            }
            if (comboBoxUnits.Items.Count>0)
            {
                comboBoxUnits.SelectedIndex = 0;
                comboBoxCorrelation.SelectedIndex = 0;
            }
        }

        private void radioBtnTimeSeries_CheckedChanged(object sender, EventArgs e)
        {
            LoadMethods();           
        }

        private void radioBtnUnit_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxUnits.Enabled = radioBtnUnit.Checked;
        }

       


    }
}
