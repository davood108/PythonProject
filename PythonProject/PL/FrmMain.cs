﻿using System;
using System.Data;
using System.Windows.Forms;
using UI;


namespace PythonProject
{
    public partial class FrmMain : Form
    {
     
        public FrmMain()
        {
            InitializeComponent();
           
        }
      

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }


        private void FrmMain_Load(object sender, EventArgs e)
        {
            Program.MainFormm = this;
            //linkLblUser.Text = User.CurrentUser.FullName;
            //ManageUserAuthorities();

            FormUtility.LoadApplyUserSetting();                                    
        }

   

        private void FrmMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Keys.Escape.GetHashCode())
            {
                Form activeMdiChild = this.ActiveMdiChild;
                if (activeMdiChild != null)
                    activeMdiChild.Close();

            }
        }

        private void linkLblUser_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //if (!FormUtility.Exist(typeof(FrmUser)))
            //{
            //    (new FrmUser()).ShowFormAsMdiChild();
            //}
           
        }
    

        private void preprocessingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!FormUtility.Exist(typeof(FrmPreProccess)))
            {
                FrmPreProccess frm = new FrmPreProccess();
                frm.ShowFormAsMdiChild();
            }
        }

        private void modelingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!FormUtility.Exist(typeof(FrmModeling)))
            {
                FrmModeling frm = new FrmModeling();
                frm.ShowFormAsMdiChild();
            }
        }

        private void aboutToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
           
        }

        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!FormUtility.Exist(typeof(FrmSetting)))
            {
                FrmSetting frm = new FrmSetting();
                frm.ShowFormAsMdiChild();
            }
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (!FormUtility.Exist(typeof(FrmAbout)))
            {
                FrmAbout frm = new FrmAbout();
                frm.ShowFormAsMdiChild();
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("UserManual.pdf");
            }
            catch (Exception)
            {

                MessageBox.Show("The system cannot find the file 'UserManual.pdf'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

    
    }
}
