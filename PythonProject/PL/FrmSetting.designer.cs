﻿namespace PythonProject
{
    partial class FrmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSetting));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnApplySetting = new System.Windows.Forms.Button();
            this.btnDefaultSetting = new System.Windows.Forms.Button();
            this.btnButtonColor = new System.Windows.Forms.Button();
            this.btnPanelColor = new System.Windows.Forms.Button();
            this.btnBackColor = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.radColorDialog1 = new Telerik.WinControls.RadColorDialog();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnPass = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnPyhtonPath = new System.Windows.Forms.Button();
            this.btnServer = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtPythonPath = new PythonProject.MyControls.MyTextBox();
            this.txtPass = new PythonProject.MyControls.MyTextBox();
            this.txtLogin = new PythonProject.MyControls.MyTextBox();
            this.txtServer = new PythonProject.MyControls.MyTextBox();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.SkyBlue;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(9, 8);
            this.panel1.Margin = new System.Windows.Forms.Padding(9, 8, 9, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(505, 205);
            this.panel1.TabIndex = 0;
            // 
            // btnApplySetting
            // 
            this.btnApplySetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnApplySetting.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnApplySetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApplySetting.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApplySetting.Image = global::PythonProject.Properties.Resources.check;
            this.btnApplySetting.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApplySetting.Location = new System.Drawing.Point(48, 143);
            this.btnApplySetting.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnApplySetting.Name = "btnApplySetting";
            this.btnApplySetting.Size = new System.Drawing.Size(128, 30);
            this.btnApplySetting.TabIndex = 4;
            this.btnApplySetting.Text = "Apply Settings";
            this.btnApplySetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApplySetting.UseVisualStyleBackColor = false;
            this.btnApplySetting.Click += new System.EventHandler(this.btnApplySetting_Click);
            // 
            // btnDefaultSetting
            // 
            this.btnDefaultSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnDefaultSetting.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnDefaultSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDefaultSetting.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDefaultSetting.Location = new System.Drawing.Point(241, 143);
            this.btnDefaultSetting.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDefaultSetting.Name = "btnDefaultSetting";
            this.btnDefaultSetting.Size = new System.Drawing.Size(128, 30);
            this.btnDefaultSetting.TabIndex = 3;
            this.btnDefaultSetting.Text = "Default Settings";
            this.btnDefaultSetting.UseVisualStyleBackColor = false;
            this.btnDefaultSetting.Click += new System.EventHandler(this.btnDefaultSetting_Click);
            // 
            // btnButtonColor
            // 
            this.btnButtonColor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnButtonColor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnButtonColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnButtonColor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnButtonColor.Location = new System.Drawing.Point(165, 96);
            this.btnButtonColor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnButtonColor.Name = "btnButtonColor";
            this.btnButtonColor.Size = new System.Drawing.Size(26, 25);
            this.btnButtonColor.TabIndex = 2;
            this.btnButtonColor.Text = "...";
            this.btnButtonColor.UseVisualStyleBackColor = false;
            this.btnButtonColor.Click += new System.EventHandler(this.btnButtonColor_Click);
            // 
            // btnPanelColor
            // 
            this.btnPanelColor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnPanelColor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnPanelColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPanelColor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPanelColor.Location = new System.Drawing.Point(165, 62);
            this.btnPanelColor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPanelColor.Name = "btnPanelColor";
            this.btnPanelColor.Size = new System.Drawing.Size(26, 25);
            this.btnPanelColor.TabIndex = 1;
            this.btnPanelColor.Text = "...";
            this.btnPanelColor.UseVisualStyleBackColor = false;
            this.btnPanelColor.Click += new System.EventHandler(this.btnPanelColor_Click);
            // 
            // btnBackColor
            // 
            this.btnBackColor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnBackColor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnBackColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackColor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackColor.Location = new System.Drawing.Point(165, 30);
            this.btnBackColor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBackColor.Name = "btnBackColor";
            this.btnBackColor.Size = new System.Drawing.Size(26, 25);
            this.btnBackColor.TabIndex = 0;
            this.btnBackColor.Text = "...";
            this.btnBackColor.UseVisualStyleBackColor = false;
            this.btnBackColor.Click += new System.EventHandler(this.btnBackColor_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(48, 102);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Button Color";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(48, 68);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Panel Back Color";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(48, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Form Back Color";
            // 
            // radColorDialog1
            // 
            this.radColorDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("radColorDialog1.Icon")));
            this.radColorDialog1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radColorDialog1.SelectedColor = System.Drawing.Color.Red;
            this.radColorDialog1.SelectedHslColor = Telerik.WinControls.HslColor.FromAhsl(0D, 1D, 1D);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1047, 221);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(532, 8);
            this.panel2.Margin = new System.Windows.Forms.Padding(9, 8, 9, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(506, 205);
            this.panel2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBackColor);
            this.groupBox1.Controls.Add(this.btnApplySetting);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnDefaultSetting);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnButtonColor);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnPanelColor);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(505, 205);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Form Appearance Settings";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtPythonPath);
            this.groupBox2.Controls.Add(this.txtPass);
            this.groupBox2.Controls.Add(this.txtLogin);
            this.groupBox2.Controls.Add(this.txtServer);
            this.groupBox2.Controls.Add(this.btnPass);
            this.groupBox2.Controls.Add(this.btnLogin);
            this.groupBox2.Controls.Add(this.btnPyhtonPath);
            this.groupBox2.Controls.Add(this.btnServer);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(506, 205);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Configuration";
            // 
            // btnPass
            // 
            this.btnPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnPass.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPass.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPass.Image = global::PythonProject.Properties.Resources.check;
            this.btnPass.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPass.Location = new System.Drawing.Point(308, 91);
            this.btnPass.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPass.Name = "btnPass";
            this.btnPass.Size = new System.Drawing.Size(45, 25);
            this.btnPass.TabIndex = 13;
            this.btnPass.Text = "Save";
            this.btnPass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPass.UseVisualStyleBackColor = false;
            this.btnPass.Click += new System.EventHandler(this.btnPass_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Image = global::PythonProject.Properties.Resources.check;
            this.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogin.Location = new System.Drawing.Point(308, 58);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(45, 25);
            this.btnLogin.TabIndex = 14;
            this.btnLogin.Text = "Save";
            this.btnLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnPyhtonPath
            // 
            this.btnPyhtonPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnPyhtonPath.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnPyhtonPath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPyhtonPath.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPyhtonPath.Image = global::PythonProject.Properties.Resources.check;
            this.btnPyhtonPath.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPyhtonPath.Location = new System.Drawing.Point(410, 145);
            this.btnPyhtonPath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPyhtonPath.Name = "btnPyhtonPath";
            this.btnPyhtonPath.Size = new System.Drawing.Size(45, 25);
            this.btnPyhtonPath.TabIndex = 15;
            this.btnPyhtonPath.Text = "Save";
            this.btnPyhtonPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPyhtonPath.UseVisualStyleBackColor = false;
            this.btnPyhtonPath.Click += new System.EventHandler(this.btnPyhtonPath_Click);
            // 
            // btnServer
            // 
            this.btnServer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnServer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnServer.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServer.Image = global::PythonProject.Properties.Resources.check;
            this.btnServer.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnServer.Location = new System.Drawing.Point(308, 26);
            this.btnServer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnServer.Name = "btnServer";
            this.btnServer.Size = new System.Drawing.Size(45, 25);
            this.btnServer.TabIndex = 16;
            this.btnServer.Text = "Save";
            this.btnServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnServer.UseVisualStyleBackColor = false;
            this.btnServer.Click += new System.EventHandler(this.btnServer_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(42, 152);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Python Path";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(42, 101);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(42, 67);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Login";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(42, 34);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Server Adrress";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // txtPythonPath
            // 
            this.txtPythonPath.EnterColor = System.Drawing.Color.LightGreen;
            this.txtPythonPath.Location = new System.Drawing.Point(133, 149);
            this.txtPythonPath.Name = "txtPythonPath";
            this.txtPythonPath.OnlyDigit = false;
            this.txtPythonPath.RegexPattern = null;
            this.txtPythonPath.Size = new System.Drawing.Size(271, 20);
            this.txtPythonPath.TabIndex = 12;
            // 
            // txtPass
            // 
            this.txtPass.EnterColor = System.Drawing.Color.LightGreen;
            this.txtPass.Location = new System.Drawing.Point(133, 98);
            this.txtPass.Name = "txtPass";
            this.txtPass.OnlyDigit = false;
            this.txtPass.PasswordChar = '*';
            this.txtPass.RegexPattern = null;
            this.txtPass.Size = new System.Drawing.Size(169, 20);
            this.txtPass.TabIndex = 11;
            // 
            // txtLogin
            // 
            this.txtLogin.EnterColor = System.Drawing.Color.LightGreen;
            this.txtLogin.Location = new System.Drawing.Point(133, 64);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.OnlyDigit = false;
            this.txtLogin.RegexPattern = null;
            this.txtLogin.Size = new System.Drawing.Size(169, 20);
            this.txtLogin.TabIndex = 10;
            // 
            // txtServer
            // 
            this.txtServer.EnterColor = System.Drawing.Color.LightGreen;
            this.txtServer.Location = new System.Drawing.Point(133, 33);
            this.txtServer.Name = "txtServer";
            this.txtServer.OnlyDigit = false;
            this.txtServer.RegexPattern = null;
            this.txtServer.Size = new System.Drawing.Size(169, 20);
            this.txtServer.TabIndex = 9;
            // 
            // FrmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 221);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FrmSetting";
            this.Text = "Setting";
            this.Load += new System.EventHandler(this.FrmSetting_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private Telerik.WinControls.RadColorDialog radColorDialog1;
        private System.Windows.Forms.Button btnButtonColor;
        private System.Windows.Forms.Button btnPanelColor;
        private System.Windows.Forms.Button btnBackColor;
        private System.Windows.Forms.Button btnApplySetting;
        private System.Windows.Forms.Button btnDefaultSetting;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private MyControls.MyTextBox txtPythonPath;
        private MyControls.MyTextBox txtPass;
        private MyControls.MyTextBox txtLogin;
        private MyControls.MyTextBox txtServer;
        private System.Windows.Forms.Button btnPass;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnPyhtonPath;
        private System.Windows.Forms.Button btnServer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}