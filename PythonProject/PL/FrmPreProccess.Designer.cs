﻿using System.Windows.Forms;

namespace PythonProject
{
    partial class FrmPreProccess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimeSelectorStart = new Atf.UI.DateTimeSelector();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimeSelectorEnd = new Atf.UI.DateTimeSelector();
            this.lblPPName = new System.Windows.Forms.Label();
            this.btnSelectPowerPlant = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnRun = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioBtnPowerPlant = new System.Windows.Forms.RadioButton();
            this.radioBtnUnit = new System.Windows.Forms.RadioButton();
            this.txtPowerPlantCode = new PythonProject.MyControls.MyTextBox();
            this.comboBoxAttribute2 = new PythonProject.MyControls.MyComboBox();
            this.comboBoxAttribute1 = new PythonProject.MyControls.MyComboBox();
            this.comboBoxMethods = new PythonProject.MyControls.MyComboBox();
            this.comboBoxUnits = new PythonProject.MyControls.MyComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "PowerPlant Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "ToDate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 174);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "FromDate";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Unit Code";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(36, 244);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Attribute1";
            // 
            // dateTimeSelectorStart
            // 
            this.dateTimeSelectorStart.Location = new System.Drawing.Point(158, 174);
            this.dateTimeSelectorStart.Name = "dateTimeSelectorStart";
            this.dateTimeSelectorStart.Size = new System.Drawing.Size(169, 21);
            this.dateTimeSelectorStart.TabIndex = 4;
            this.toolTip1.SetToolTip(this.dateTimeSelectorStart, "The first day of month");
            this.dateTimeSelectorStart.UsePersianFormat = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(36, 279);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Attribute2";
            // 
            // dateTimeSelectorEnd
            // 
            this.dateTimeSelectorEnd.Location = new System.Drawing.Point(158, 209);
            this.dateTimeSelectorEnd.Name = "dateTimeSelectorEnd";
            this.dateTimeSelectorEnd.Size = new System.Drawing.Size(169, 21);
            this.dateTimeSelectorEnd.TabIndex = 5;
            this.toolTip1.SetToolTip(this.dateTimeSelectorEnd, "The last day of month");
            this.dateTimeSelectorEnd.UsePersianFormat = true;
            // 
            // lblPPName
            // 
            this.lblPPName.AutoSize = true;
            this.lblPPName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblPPName.Location = new System.Drawing.Point(344, 52);
            this.lblPPName.Name = "lblPPName";
            this.lblPPName.Size = new System.Drawing.Size(11, 13);
            this.lblPPName.TabIndex = 20;
            this.lblPPName.Text = "*";
            // 
            // btnSelectPowerPlant
            // 
            this.btnSelectPowerPlant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnSelectPowerPlant.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnSelectPowerPlant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectPowerPlant.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectPowerPlant.Location = new System.Drawing.Point(304, 44);
            this.btnSelectPowerPlant.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSelectPowerPlant.Name = "btnSelectPowerPlant";
            this.btnSelectPowerPlant.Size = new System.Drawing.Size(34, 31);
            this.btnSelectPowerPlant.TabIndex = 2;
            this.btnSelectPowerPlant.Text = "...";
            this.toolTip1.SetToolTip(this.btnSelectPowerPlant, "Search and Select Powerplants");
            this.btnSelectPowerPlant.UseVisualStyleBackColor = false;
            this.btnSelectPowerPlant.Click += new System.EventHandler(this.btnSelectPowerPlant_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(36, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Method";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnRun.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRun.Image = global::PythonProject.Properties.Resources.RunIcon1;
            this.btnRun.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRun.Location = new System.Drawing.Point(158, 334);
            this.btnRun.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(170, 37);
            this.btnRun.TabIndex = 8;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = false;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.comboBoxAttribute2);
            this.panel2.Controls.Add(this.comboBoxAttribute1);
            this.panel2.Controls.Add(this.comboBoxMethods);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btnRun);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.dateTimeSelectorStart);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.dateTimeSelectorEnd);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Margin = new System.Windows.Forms.Padding(12, 10, 12, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(612, 422);
            this.panel2.TabIndex = 2;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.radioBtnPowerPlant);
            this.groupBox4.Controls.Add(this.radioBtnUnit);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.txtPowerPlantCode);
            this.groupBox4.Controls.Add(this.comboBoxUnits);
            this.groupBox4.Controls.Add(this.btnSelectPowerPlant);
            this.groupBox4.Controls.Add(this.lblPPName);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(29, 50);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(580, 118);
            this.groupBox4.TabIndex = 29;
            this.groupBox4.TabStop = false;
            // 
            // radioBtnPowerPlant
            // 
            this.radioBtnPowerPlant.AutoSize = true;
            this.radioBtnPowerPlant.Location = new System.Drawing.Point(137, 19);
            this.radioBtnPowerPlant.Name = "radioBtnPowerPlant";
            this.radioBtnPowerPlant.Size = new System.Drawing.Size(79, 17);
            this.radioBtnPowerPlant.TabIndex = 26;
            this.radioBtnPowerPlant.Text = "PowerPlant";
            this.radioBtnPowerPlant.UseVisualStyleBackColor = true;
            this.radioBtnPowerPlant.CheckedChanged += new System.EventHandler(this.radioBtnPowerPlant_CheckedChanged);
            // 
            // radioBtnUnit
            // 
            this.radioBtnUnit.AutoSize = true;
            this.radioBtnUnit.Checked = true;
            this.radioBtnUnit.Location = new System.Drawing.Point(16, 18);
            this.radioBtnUnit.Name = "radioBtnUnit";
            this.radioBtnUnit.Size = new System.Drawing.Size(44, 17);
            this.radioBtnUnit.TabIndex = 25;
            this.radioBtnUnit.TabStop = true;
            this.radioBtnUnit.Text = "Unit";
            this.radioBtnUnit.UseVisualStyleBackColor = true;
            this.radioBtnUnit.CheckedChanged += new System.EventHandler(this.radioBtnPowerPlant_CheckedChanged);
            // 
            // txtPowerPlantCode
            // 
            this.txtPowerPlantCode.EnterColor = System.Drawing.Color.LightGreen;
            this.txtPowerPlantCode.Location = new System.Drawing.Point(130, 48);
            this.txtPowerPlantCode.Name = "txtPowerPlantCode";
            this.txtPowerPlantCode.OnlyDigit = false;
            this.txtPowerPlantCode.RegexPattern = null;
            this.txtPowerPlantCode.Size = new System.Drawing.Size(169, 20);
            this.txtPowerPlantCode.TabIndex = 1;
            this.txtPowerPlantCode.Leave += new System.EventHandler(this.txtPowerPlantCode_Leave);
            // 
            // comboBoxAttribute2
            // 
            this.comboBoxAttribute2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAttribute2.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxAttribute2.FormattingEnabled = true;
            this.comboBoxAttribute2.Location = new System.Drawing.Point(159, 276);
            this.comboBoxAttribute2.Name = "comboBoxAttribute2";
            this.comboBoxAttribute2.Size = new System.Drawing.Size(169, 21);
            this.comboBoxAttribute2.TabIndex = 22;
            // 
            // comboBoxAttribute1
            // 
            this.comboBoxAttribute1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAttribute1.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxAttribute1.FormattingEnabled = true;
            this.comboBoxAttribute1.Location = new System.Drawing.Point(160, 244);
            this.comboBoxAttribute1.Name = "comboBoxAttribute1";
            this.comboBoxAttribute1.Size = new System.Drawing.Size(169, 21);
            this.comboBoxAttribute1.TabIndex = 21;
            // 
            // comboBoxMethods
            // 
            this.comboBoxMethods.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMethods.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxMethods.FormattingEnabled = true;
            this.comboBoxMethods.Location = new System.Drawing.Point(160, 25);
            this.comboBoxMethods.Name = "comboBoxMethods";
            this.comboBoxMethods.Size = new System.Drawing.Size(169, 21);
            this.comboBoxMethods.TabIndex = 0;
            this.comboBoxMethods.SelectedIndexChanged += new System.EventHandler(this.comboBoxMethods_SelectedIndexChanged);
            // 
            // comboBoxUnits
            // 
            this.comboBoxUnits.EnterColor = System.Drawing.Color.LightGreen;
            this.comboBoxUnits.FormattingEnabled = true;
            this.comboBoxUnits.Location = new System.Drawing.Point(129, 87);
            this.comboBoxUnits.Name = "comboBoxUnits";
            this.comboBoxUnits.Size = new System.Drawing.Size(169, 21);
            this.comboBoxUnits.TabIndex = 3;
            // 
            // FrmPreProccess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 445);
            this.Controls.Add(this.panel2);
            this.Name = "FrmPreProccess";
            this.Text = "PreProcessing";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private Atf.UI.DateTimeSelector dateTimeSelectorStart;
        private System.Windows.Forms.Label label9;
        private Atf.UI.DateTimeSelector dateTimeSelectorEnd;
        private MyControls.MyTextBox txtPowerPlantCode;
        private MyControls.MyComboBox comboBoxMethods;
        private System.Windows.Forms.Label label10;
        private Button btnRun;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Button btnSelectPowerPlant;
        private System.Windows.Forms.Label lblPPName;
        private MyControls.MyComboBox comboBoxUnits;
        private System.Windows.Forms.ToolTip toolTip1;
        private Panel panel2;
        private MyControls.MyComboBox comboBoxAttribute2;
        private MyControls.MyComboBox comboBoxAttribute1;
        private GroupBox groupBox4;
        private RadioButton radioBtnPowerPlant;
        private RadioButton radioBtnUnit;

    }
}