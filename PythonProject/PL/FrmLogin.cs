﻿using System;
using System.Windows.Forms;

namespace PythonProject
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
           
            txtUserName.Select(); //~ this.ActiveControl = txtPassword;

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
           
            lblError.Text = "*";
            errorProvider1.Clear();

         
            if (txtUserName.Text.Trim() == "")
            {
                string errorText = "نام کاربری را وارد کنید";
                lblError.Text = errorText;
                errorProvider1.SetError(txtUserName, errorText);
                return;
            }
            if (txtPassword.Text == "")
            {
                string errorText = "رمز عبور را وارد کنید";
                lblError.Text = errorText;
                errorProvider1.SetError(txtPassword, errorText);
                return;
            }

            try
            {
               
                string error;
                bool success = User.Login(txtUserName.Text, txtPassword.Text, out error);
                if (!success)
                {
                    //string errorText = "نام کاربری یا رمز عبور اشتباه است";
                    lblError.Text = error;

                    errorProvider1.SetError(txtPassword, error);
                    errorProvider1.SetError(txtUserName, error);

                    LogUtility.Log(String.Format("'{0}'  Error in Log In: {1}", txtUserName.Text, error));
                    return;

                }
            }
            catch (Exception ex)
            {
                LogUtility.Log(ex);
                MessageBox.Show("Communication with the login service failed ", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            User.CurrentUserName = txtUserName.Text.Trim();
            LogUtility.Log(String.Format("'{0}' logged in", User.CurrentUserName));










            //  User.CurrentUser.EditPermissions(true, true, true, true, true);
            //  var x = User.GetAllLogins();

            //throw new Exception("x");
            // textBox1.Text = string.Format("{0:n0}", double.Parse(textBox1.Text.Replace(",", "")));

            //string[] roles = {"User"}; // { "User", "Admin" };
            //GenericIdentity identity = new GenericIdentity("ali");
            //Thread.CurrentPrincipal = new GenericPrincipal(identity, roles);
            //string name = identity.Name;
            //bool Auth = identity.IsAuthenticated;
            //bool isInRole = Thread.CurrentPrincipal.IsInRole("User");      


            //if (isInRole)
            //{
            //    MessageBox.Show("The test was successful as a user in the user-defined role of User");
            //}
            //else
            //{
            //    MessageBox.Show("The test was not successful!");
            //}
            //try
            //{
            // AdminFunction();
            //}
            //catch (Exception ex)
            //{

            //    MessageBox.Show("Exception: " + ex.Message);
            //}

            //  User.CurrentUser = new User() {CreateBy = txtUserName.Text};

            Program.MainFormm = new FrmMain();
            Program.MainFormm.Show();
            this.Hide();



        }


        private void FrmLogin_Load(object sender, EventArgs e)
        {
          
        }

       
    }
}
