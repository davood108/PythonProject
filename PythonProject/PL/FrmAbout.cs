﻿using System.Diagnostics;
using System.IO;
using System.Windows.Forms;


namespace UI
{
    public partial class FrmAbout : Form
    {

        public FrmAbout()
        {
            InitializeComponent();
        }

        private void FrmAbout_Load(object sender, System.EventArgs e)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            string date = fvi.Comments;
            txtDescrpition.Text = txtDescrpition.Text.Replace("#version", version);
            txtDescrpition.Text = txtDescrpition.Text.Replace("#date", date);
        }

    }
}
