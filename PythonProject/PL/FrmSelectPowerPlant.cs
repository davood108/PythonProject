﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace PythonProject
{
    public partial class FrmSelectPowerPlant : Form
    {
        public string SelectedPowerPlantName { get; set; }
        public string SelectedPowerPlantCode { get; set; }
        bool _multipleSelection = false;
       

        // BindingSource bs = new BindingSource();
        public FrmSelectPowerPlant(bool multipleSelection)
        {
           
            InitializeComponent();
            _multipleSelection = multipleSelection;
            if (!_multipleSelection)
            {
                radGridView1.Columns[0].IsVisible = false;
                btnOk.Visible = false;
                //btnCancel.Visible = false;
                checkBoxSelectAll.Visible = false;
                label1.Visible = false;
                label2.Visible = true;
            }
            DialogResult = DialogResult.Cancel;
            this.radGridView1.TableElement.AlternatingRowColor = Color.LightCyan;
        }

        private void FrmParamsList_Load(object sender, EventArgs e)
        {
          LoadPowerPlantsList();
        }

        private void LoadPowerPlantsList()
        {
            try
            {
                DataTable dt = DbUtility.GetPowerPlants();
                radGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void radTextBox1_TextChanged(object sender, EventArgs e)
        {
            string txtToFind = radTextBox1.Text.ToLower().Trim();
            // bs.Filter = string.Format("Name LIKE '%{0}%' OR CONVERT(ID, 'System.String') LIKE '%{0}%' OR Description LIKE '%{0}%' OR Category LIKE '%{0}%'", txtToFind);
            if (txtToFind != "")
            {
                //PropertyManager currencyManager1 = (PropertyManager)BindingContext[radGridView1.DataSource];
              //  currencyManager1.SuspendBinding();

                foreach (GridViewRowInfo row in radGridView1.Rows)
                {
                    row.IsVisible = false;
                    for (int j = 1; j < 6; j++) //radGridView1.Columns.Count
                    {
                        //if(j==2)
                        //    continue; //ignore column enable
                        try
                        {
                            row.Cells[j].Style.BackColor = Color.White;
                            if (row.Cells[j].Value.ToString().ToLower().Trim().Contains(txtToFind))
                            {
                                row.IsVisible = true;
                                row.Cells[j].Style.CustomizeFill = true;
                                row.Cells[j].Style.BackColor =
                                    Color.Aqua;
                            }
                        }
                        catch
                        {
                        }
                    }
                }
               // currencyManager1.ResumeBinding();
            }
            else
            {
                foreach (GridViewRowInfo row in radGridView1.Rows)
                {
                    for (int j = 1; j < 6; j++) //radGridView1.Columns.Count
                    {
                        //if (j == 2)
                        //    continue; //ignore column enable
                        try
                        {
                            row.IsVisible = true;
                            row.Cells[j].Style.CustomizeFill = true;
                            //row.Cells[j].Style.BackColor =
                            //    Color.White;
                            row.Cells[j].Style.Reset();
                        }
                        catch (Exception)
                        {
                            //throw;
                        }
                    }
                }
            }

        }


        private void radGridView1_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (_multipleSelection)
                {
                    
               
                if (e.ColumnIndex == 4 || e.ColumnIndex == 5)//Region
                {
                    string regionCode = radGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();

                    foreach (var row in radGridView1.Rows)
                    {
                        if (row.Cells[4].Value.ToString() == regionCode)
                        {
                            row.Cells[0].Value = true;
                        }

                    }
                }
                else
                {
                    bool b = (bool)(radGridView1.Rows[e.RowIndex].Cells[0].Value ?? false);
                    radGridView1.Rows[e.RowIndex].Cells[0].Value = !b;
                }
                }
                else
                {
                    SelectedPowerPlantName = radGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
                    SelectedPowerPlantCode = radGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                    DialogResult = DialogResult.OK;
                }

             
                
            }

        }

        private void radGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {

            //if (e.KeyChar == (char) Keys.Enter)
            //{
            //    if (radGridView1.SelectedRows.Count == 1)
            //    {
            //        bool b = (bool)(radGridView1.SelectedRows[0].Cells[0].Value ?? false);
            //        radGridView1.SelectedRows[0].Cells[0].Value = !b;
            //        //SelectedPowerPlantName = radGridView1.SelectedRows[0].Cells[2].Value.ToString();
            //        //SelectedPowerPlantCode = radGridView1.SelectedRows[0].Cells[0].Value.ToString();
            //        //DialogResult=DialogResult.OK;
            //    }
            //}
        }

        private void FrmParamsList_KeyDown(object sender, KeyEventArgs e)
            //Set KeyPreview to true on form to catch this event
        {
            if (e.KeyCode == Keys.F && e.Control) //Ctrl+F
                radTextBox1.Focus();
            else if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void checkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSelectAll.CheckState==CheckState.Checked)
            {
                foreach (var row in radGridView1.Rows)
                {
                    row.Cells[0].Value = true;
                }
            }
            else if (checkBoxSelectAll.CheckState == CheckState.Unchecked)
            {
                foreach (var row in radGridView1.Rows)
                {
                    row.Cells[0].Value = false;
                }
            }
            
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

            foreach (var row in radGridView1.Rows)
            {
                bool b = (bool)(row.Cells[0].Value ?? false);
                if (b)
                {
                    SelectedPowerPlantCode += row.Cells[1].Value.ToString() + ',';
                    SelectedPowerPlantName += row.Cells[3].Value.ToString() + ',';
                }
            }
            if (SelectedPowerPlantCode.Trim()!="")
            {
                SelectedPowerPlantCode = SelectedPowerPlantCode.Remove(SelectedPowerPlantCode.Length - 1);
                SelectedPowerPlantName = SelectedPowerPlantName.Remove(SelectedPowerPlantName.Length - 1);
            }

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }



    }
}
