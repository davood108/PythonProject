﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace PythonProject
{
    public static class ExtentionMethods
    {
       
        public static void ShowFormAsMdiChild(this Form frm,FormWindowState windowState)
        {
            frm.MdiParent = Program.MainFormm;
            frm.WindowState=windowState;
            frm.SetAppearanceSetting();//
            frm.Show();
        }
        public static void ShowFormAsMdiChild(this Form frm )
        {
            frm.MdiParent = Program.MainFormm;
            frm.WindowState = FormWindowState.Maximized;
            frm.SetAppearanceSetting();//
            frm.Show();
        }

        /// <summary>
        /// ShowDialog Form in center of screen and apply appearance setting
        /// </summary>
        /// <param name="frm"></param>
        public static DialogResult ShowFormAsDialog(this Form frm)
        {
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.SetAppearanceSetting(); //
            return frm.ShowDialog();
        }

        /// <summary>
        /// Show only one instance of Form with subject of frm. If exist another instance of subject of frm,this method bring it to front
        /// </summary>
        public static void ShowSingleInstance(this Form frm)
        {
            foreach (Form form in Application.OpenForms)
            {
                if (form.GetType()==frm.GetType())
                {
                    form.BringToFront();
                    return;
                }
            }

            frm.MdiParent = Program.MainFormm;
            frm.Show();
        }

        /// <summary>
        /// Show only one instance of Form with Title equls title. If exist another instance with Ttile equls title,this method bring it to front
        /// </summary>
        public static void ShowSingleInstance(this Form frm, string formTitle)
        {
            foreach (Form form in Application.OpenForms)
            {
                if (form.Text == formTitle)
                {
                    form.BringToFront();
                    return;
                }
            }

            frm.MdiParent = Program.MainFormm;
            frm.Show();
        }

        public static void SetAppearanceSetting(this Form frm)
        {
           SetBackColors(frm);
        }

        private static void SetBackColors(Control control)
        {
            if (control is Panel)
            {
                if (control is TableLayoutPanel)
                    (control as Panel).BackColor = FormUtility.BackColor; //SystemColors.ActiveCaption;
                else
                    (control as Panel).BackColor = FormUtility.PanelColor;
            }
            else if (control is Button)
                (control as Button).BackColor = FormUtility.ButtonColor;
            else if (control is Form)
            {
                (control as Form).BackColor = FormUtility.BackColor; //SystemColors.ActiveCaption;
               // (control as Form).Font = new Font(new FontFamily("Arial"),10);
            }
            else if (control is RadioButton)
                (control as RadioButton).BackColor = Color.Transparent;
            foreach (Control child in control.Controls)
            {
                SetBackColors(child);
            }
        }
    }

    public static class FormUtility
    {
        public static Color BackColor { get; set; }
        public static Color PanelColor { get; set; }
        public static Color ButtonColor { get; set; }

       

        static FormUtility()
        {
            BackColor = Color.LightSkyBlue;
            PanelColor = Color.DeepSkyBlue;
            ButtonColor = Color.LightSteelBlue;

        }

        public static void LoadApplyUserSetting()
        {

            string settingString = XmlUtility.GetSettingString();// "-5648924;-3490647;-6037261";//User.CurrentUser.SettingString;
                if (!string.IsNullOrEmpty(settingString))
                {
                    string[] colors = settingString.Split(';');
                    BackColor = Color.FromArgb(int.Parse(colors[0]));
                    ButtonColor = Color.FromArgb(int.Parse(colors[1]));
                    PanelColor = Color.FromArgb(int.Parse(colors[2]));
                }
           
        }

        /// <summary>
        /// If exist a form with subject of formType then bring it to front and return true, else return false
        /// </summary>
        public static bool Exist(Type formType)
        {
            foreach (Form form in Application.OpenForms)
            {
                if (form.GetType() == formType)
                {
                    form.BringToFront();
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// If exist a form with subject of formType then return it, else return null
        /// </summary>
        public static Form GetOpenForm(Type formType)
        {
            return Application.OpenForms.Cast<Form>().FirstOrDefault(form => form.GetType() == formType);
        }

        /// <summary>
        /// If exist a form with subject of formType and title then return it, else return null
        /// </summary>
        public static Form GetOpenForm(Type formType, string title)
        {
            return
                Application.OpenForms.Cast<Form>()
                    .FirstOrDefault(form => form.GetType() == formType && form.Text == title);
        }

        /// <summary>
        /// If exist a form with title of formTitle then bring it to front and return true, else return false
        /// </summary>
        public static bool Exist(string formTitle)
        {
            foreach (Form form in Application.OpenForms)
            {
                if (form.Text == formTitle)
                {
                    form.BringToFront();
                    return true;
                }
            }
            return false;
        }                            

    }
}
