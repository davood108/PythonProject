﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace PythonProject.MyControls
{
    public partial class MyComboBox : ComboBox
    {
        private Color _enterColor = Color.LightGreen;
        private Color _permanentBackColor;

        [Description("the color of textbox when enter"),
         Category("NewProperty"),
        //DefaultValue(Color.LightGreen),
         Browsable(true)]
        public Color EnterColor
        {
            get { return _enterColor; }
            set { _enterColor = value; }
        }

        protected override void OnEnter(EventArgs e)
        {

            this._permanentBackColor = this.BackColor;
            this.BackColor = _enterColor;
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            this.BackColor = this._permanentBackColor;
            base.OnLeave(e);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

    }
}
