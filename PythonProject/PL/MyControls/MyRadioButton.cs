﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace PythonProject.MyControls
{
    public partial class MyRadioButton : RadioButton
    {
        private Color _permanentForeColor;
        private Color _checkedColor = Color.LightGreen;

        [Description("the color of checked radioButton"),
         Category("NewProperty"),
        //DefaultValue(Color.LightGreen),
         Browsable(true)]
        public Color CheckedColor
        {
            get { return _checkedColor; }
            set { _checkedColor = value; }
        }

        public MyRadioButton()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        protected override void OnCheckedChanged(EventArgs e)
        {
            base.OnCheckedChanged(e);
            if (Checked)
            {
                _permanentForeColor = BackColor;
                BackColor = _checkedColor;
                //Font = new Font(Font, FontStyle.Bold);

            }
            else
            {
                BackColor = _permanentForeColor;
                //Font = new Font(Font, FontStyle.Regular);
            }


        }
     
    }
}
