﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace PythonProject.MyControls
{
    public partial class MyTextBox : TextBox
    {
        private bool _mustFill;
        private Color _enterColor = Color.LightGreen;
        private Color _permanentForeColor;
        private Color _permanentBackColor;
        private string _emptyText;
        private bool _LeavedEmpty = false;
        private Button _assigned_btn = new Button();

        //ezafe shodane property be ghesmate design va meghdar dehi default :
        [Description("Set If the user must fill the textbox"),
         Category("NewProperty"),
         DefaultValue(false),
         Browsable(true)]
        public bool MustFill
        {
            get { return _mustFill; }
            set { _mustFill = value; }
        }

        [Description("When the user press Enter key the click event of this button raise"),
         Category("NewProperty"),
         Browsable(true)]
        public Button AssignedBtn
        {
            get { return _assigned_btn; }
            set { _assigned_btn = value; }
        }

        [Description("the color of textbox when enter"),
         Category("NewProperty"),
        //DefaultValue(Color.LightGreen),
         Browsable(true)]
        public Color EnterColor
        {
            get { return _enterColor; }
            set { _enterColor = value; }
        }

        [Description("The text of the textbox when textbox is empty (for must fill texboxes)"),
         Category("NewProperty"),
         DefaultValue("Fill this!"),
         Browsable(true)]
        public string EmptyText
        {
            get { return _emptyText; }
            set { _emptyText = value; }
        }

        [Description("Only Number is allowed"),
         Category("NewProperty"),
         DefaultValue("false"),
         Browsable(true)]
        public bool OnlyDigit { get; set; }

        [Description("Regex Pattern"),
         Category("NewProperty"),
         DefaultValue(""),
         Browsable(true)]
        public string RegexPattern { get; set; }

        [Description("Indicates whether the RegexPatern successfully match Text"),
         Category("NewProperty"),
         DefaultValue("false"),
         Browsable(true)]
        public bool RegexSuccess
        {
            get
            {
                if (!string.IsNullOrEmpty(RegexPattern))
                {
                    Regex r = new Regex("^" + RegexPattern + "$");
                    Match m = r.Match(Text);
                    if (!m.Success)
                        return false;
                }
                return true;
            }
        }

        public MyTextBox()
        {
            EmptyText = "Fill this!";
            InitializeComponent();

        }

        protected override void OnEnter(EventArgs e)
        {
            this.ForeColor = _permanentForeColor;
            this._permanentBackColor = this.BackColor;
            if (!_LeavedEmpty)
            {
                _permanentForeColor = this.ForeColor;
            }
            this.BackColor = _enterColor;
            if (this.Text == _emptyText && _LeavedEmpty)
            {
                this.Text = "";
            }

            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            if (_mustFill && this.Text == "")
            {
                _LeavedEmpty = true;
                this.ForeColor = Color.Red;
                this.BackColor = this._permanentBackColor;
                this.Text = _emptyText;
            }
            else
            {
                this.ForeColor = _permanentForeColor;
                this.BackColor = this._permanentBackColor;
                _LeavedEmpty = false;
            }

            base.OnLeave(e);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        protected override void OnKeyUp(KeyEventArgs e)

        {
            if (e.KeyCode == Keys.Enter && _assigned_btn != null)
            {
                _assigned_btn.PerformClick();
                _assigned_btn.Focus();
            }
            base.OnKeyUp(e);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (OnlyDigit && !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            //if (!char.IsControl(e.KeyChar) && !string.IsNullOrEmpty(RegexPattern))
            //{
            //    Regex r = new Regex(RegexPattern);
            //    Match m = r.Match(Text + e.KeyChar);
            //    if (!m.Success)
            //    {
            //        e.Handled = true;
            //    }
               
            //}

            base.OnKeyPress(e);
        }

        //protected override void OnTextChanged(EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(RegexPattern))
        //    {
        //        Regex r = new Regex(RegexPattern);
        //        Match m = r.Match(Text);
        //        if (!m.Success)
        //        {
        //            Text = "";
        //        }
        //    }

        //    base.OnTextChanged(e);
        //}
    }
}
